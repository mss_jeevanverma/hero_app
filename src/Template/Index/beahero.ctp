<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<section class="slider_back parascroll " id="one">
    <div class="scrollable"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slide_cont"> 

                <form class="form-signin beahero" action="" method="post" id="password_reset">
                <div id="form_msg"></div>
                    <h1 class="form-signin-heading">Be A Hero</h1>

                        <div class="row">
                            <div class="col-lg-6">
                                <label>First Name</label>
                                <input type="text" name="first_name" data-error="Only Characters and Minimum Length 1" pattern="[a-zA-Z]+" maxlength="100" data-minlength="1"  class="form-control" placeholder="First Name" value = "" tabindex="1" required>
                                <div class="help-block with-errors"></div>

                            </div> 
                            <div class="col-lg-6">

                                <label>Last Name</label>
                               <input type="text" name="last_name"  data-error="Only Character and Minimum Length 1"  maxlength="100" data-minlength="1" id="last_name" class="form-control input-lg" value = "" placeholder="Last Name" tabindex="2" required>
                                <div class="help-block with-errors"></div>

                            </div>    
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label>Email ID</label>
                                 <input type="email" attern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" id="email" data-error="Please add a valid Email Address" class="form-control input-lg" placeholder="Email Address" tabindex="3" value = "" required>
                                <div class="help-block with-errors"></div>

                            </div> 
                            <div class="col-lg-6">

                                <label>Phone Number</label>
                                <input type="text" name="phone_no" id="phone_no" class="form-control input-lg" placeholder="Phone Number"  maxlength="14" data-error="Please add valid Phone Number"  tabindex="4" value = "" required>
                                <div class="help-block with-errors"></div>

                            </div>    
                        </div>
                        <div class="row">
                            <!--div class="col-lg-12">
                                <label>Address</label>
                                <input type="text" name="address" data-error="Only Characters and Minimum Length 10" pattern="[^ ].*" maxlength="100" data-minlength="10"  class="form-control" placeholder="Address" value = "" tabindex="1" required>
                                <div class="help-block with-errors"></div>

                            </div-->
                            <div class="col-lg-12">
                                <label>Zip Code</label>
                                <input type="text" required="" pattern="[0-9]{5,}$" oninvalid="this.setCustomValidity('5 Digit Zip code Only')" onchange="this.setCustomValidity('')" data-error="5 Digit Zip code Only" placeholder="Enter Zip Code" class="zip-input" id="zip" maxlength="5" name="a_postal_code" tabindex="5" value="">
                                <div class="help-block with-errors"></div>

                            </div>    
                        </div>
                        <input type="hidden"  value="3" name="role" />

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Apply Now</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <br><br><br><br>
</section>

<script type="text/javascript">
$(document).ready(function()
{ 
    //$(".pull-left li").remove();
    $("#phone_no").mask("(999)999-9999");

    //Application of New Hero
    $(document).on("submit",".beahero",function (event){
        event.preventDefault();

        var data = $(".beahero").serialize();

        $.ajax({
            type:"POST",
            url:"<?php echo $base_url_temp; ?>"+"index/beahero",
            data:data,
            success: function(result){
                var val =  JSON.parse(result);
                if(val.status=="success"){
                    $("#form_msg").html('<div class="alert alert-success" role="alert">'+val.msg+'</div>');
                    $('.beahero')[0].reset();
                }else{
                    $("#form_msg").html('<div class="alert alert-danger" role="alert">'+val.msg+'</div>');
                    $('.beahero')[0].reset();
                }
                setTimeout(function(){ $("#form_msg").children("div").fadeOut(); }, 2000);
            },
            error: function(event){}
        });

    });
});
</script>
