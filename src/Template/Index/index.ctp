<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

?>
<?php 

    $user_info = $data["userinfo"];
    $testimonials = $data["testimonials"];
 ?>
<!--
<section class="slider_back parascroll " id="one">
	    <div class="scrollable"></div>
	    <div class="container">
		 <div class="row">
		   <div class="col-md-7">
		     <div class="slide_cont">   
		       <h2>Ev<img alt="button_hero" src="<?php echo $base_url_temp; ?>img/template_images/o-big.png" style="width: 31px; margin-top: -5px;">lution of the Handyman</h2>
		       <p>They have been around for millennia, but not until now has the handyman evolved to serve the digital age. Hero connects you with local technical experts to identify, diagnose and resolve your every digital need. </p>
		       <a class="btn btn-success btn_pop" href="javascript:void()" data-toggle="modal" data-target="#myModal">I need a Hero</a>
		       <a class="btn btn-success btn_pop btn_o" href="javascript:void()" data-toggle="modal" data-target="#wizards"><img src="<?php echo $base_url_temp; ?>img/template_images/o-icon.png" alt="button_hero"/> </a>
		       <!--h4>I need a Hero</h4>
		     
		       
		     </div>
		   </div>
		</div>
	    </div>
</section>
-->



<section class="slider_back parascroll " id="one">
	     <div class="scrollable"></div>
	       <div class="container">
		 <div class="row">
		   <div class="col-md-7">
		     <div class="slide_cont">  

		       <h2>Ev<!--img alt="button_hero" src="dist/img/o-big.png" style="width: 31px; margin-top: -5px;"-->olution of the Handyman </h2>
		       <p>They have been around for millennia, but not until now has the handyman evolved to serve the digital age. Hero connects you with local technical experts to identify, diagnose and resolve your every digital need. </p>
		       <a class=""  href="javascript:void()" data-toggle="modal" data-target="#wizards"  data-backdrop="static" >
		       		<span class="need-h" >I need a Hero</span>
		       </a>
		       <a class="btn btn-success btn_pop btn_o" href="javascript:void()" data-toggle="modal" data-target="#wizards"  data-backdrop="static" >
			   <img src="<?php echo $base_url_temp; ?>img/template_images//o-icon.png" alt="button_hero" class="logo-orgnl"/> 			   
			   <img src="<?php echo $base_url_temp; ?>img/template_images//green.png" alt="button_hero2" class="logo-green"/> 
			   </a>
		       <!--h4>I need a Hero</h4-->
		     
		       
		     </div>
		   </div>
		</div>
	     </div>
	</section>


<?php include("wizard.modal.php"); ?>



		
	 <section class="content">
	     <div class="container"><div class="circle_mat arrow-wrap" href="#content"><a  href="#content"><i class="fa fa-angle-down"></i></a></div>
		<div class="row">
		    <div class="top_header" id="content"> 
			  <h1>Enter, the <span>Digital</span> Handyman.</h1>
			  <!--p>Hero connects you with local technical experts to identify, diagnose and resolve your every digital need. </p-->		 
			</div>
		    <div class="col-md-4 col-sm-4">
		       <img src="<?php echo $base_url_temp; ?>img/template_images/friendly.jpg" alt="friendly"/>
		       <h2>Friendly Professionals</h2>
		       <p>As well as passing a thorough background check, our heroes are rigorously screened for technical excellence, enthusiasm and humility.</p>
		    </div>
		    <div class="col-md-4 col-sm-4">
		    <img src="<?php echo $base_url_temp; ?>img/template_images/domain.jpg" alt="domain"/>
		       <h2>Master your domain</h2>
		       <p>Own your gadgetry. You've accumulated a lot of expensive stuff. Let's get more value out of them. Remember, they work for you...</p>
		    </div>
		    <div class="col-md-4 col-sm-4">
		    <img src="<?php echo $base_url_temp; ?>img/template_images/delivery.jpg" alt="delivery"/>
		       <h2>With you in minutes</h2>
		       <p>Our talented heroes are dispatched instantly from your mobile phone, straight to your door quicker than take-out pizza. And just as hot.</p>
		    </div>
		</div>
	     </div>
	 </section>	

	 <section class="form_section cta cta-3 ">
		<div class="container">
			<div class="row v-align-children">
				<div class="col-sm-5 col-md-5 text-left left_text col-md-offset-1">
					<h3>Tech-savvy? Star Wars fan?  </h3>
					<p>
					 Take our qualifying exam. Be a Hero.
					</p>
				</div>
						
				<div class="col-md-5 col-sm-7">
					<form class="fv-form fv-form-bootstrap" action="" method="post" id="beahero_form">
             					   <div id="form_msg"></div> 
             					   <div class="form-group"> 
                                <input type="text" name="first_name" data-error="Only Characters " pattern="[a-zA-Z]+" maxlength="100" data-minlength="1"  class="form-control" placeholder="First Name" value = "" tabindex="1" required>
                                <div class="help-block with-errors"></div>
                     				    </div>
                       		     <div class="form-group"> 
                               <input type="text" name="last_name"  data-error="Only Character "  maxlength="100" data-minlength="1" id="last_name" class="form-control input-lg" value = "" placeholder="Last Name" tabindex="2" required>
                                <div class="help-block with-errors"></div>
                           		   </div>
                          		  <div class="form-group"> 
                                 <input type="email" attern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" id="email" data-error="Please add a valid Email Address" class="form-control input-lg" placeholder="Email Address" tabindex="4" value = "" required>
                                <div class="help-block with-errors"></div>
                        			</div>
                        	    <div class="form-group"> 
                                <input type="text" name="phone_no" id="phone_no" class="form-control input-lg" placeholder="Phone Number"  maxlength="14" data-error="Please add valid Phone Number"  tabindex="3" value = "" required>
                                <div class="help-block with-errors"></div>
                       			 </div>
                       		     <div class="form-group"> 
                                <!--input type="text" name="address" data-error="Please add valid address" pattern="[^ ].*" maxlength="100" data-minlength="5"  class="form-control" placeholder="Address" value = "" tabindex="1" required-->
<input type="text" required="" pattern="[0-9]{5,}$" oninvalid="this.setCustomValidity('5 Digit Zip code Only')" onchange="this.setCustomValidity('')" data-error="5 Digit Zip code Only" placeholder="Enter Zip Code" class="zip-input" id="zip" maxlength="5" name="a_postal_code" value="">
                                <div class="help-block with-errors"></div>
                   				   </div>
                            
                        <input type="hidden"  value="3" name="role" />

                    <input  type="submit" value="Submit">

                    </form>
				</div>
			</div>
		</div>
	 </section>



<section class="testimonials parascroll"> 
<div class="scrollable"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			   <h2>Client Testimonials</h2>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				  <ol class="carousel-indicators">
				  	<?php $flag_dot = true; foreach($testimonials as $key => $testimonial){ ?>
				    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key ?>" <?php if($flag_dot) { echo 'class="active"'; } ?> ></li>
				    <?php $flag_dot = false; } ?>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				  <?php $flag = true; foreach($testimonials as $testimonial){ ?>
				    <div class="item <?php if($flag) { echo 'active'; } ?> ">
					<div class="test_cont">
					     <i class="fa fa-quote-left"></i>
					       <p><?php echo $testimonial->testimonial; ?></p>
						<div class="cd-author">
							<ul class="cd-author-info">
								<li><?php echo $testimonial->name; ?></li>
								<li><?php echo $testimonial->designation; ?></li>
							</ul>
						</div>
					 </div>
				    </div>
				    <?php $flag = false; } ?>
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="fa fa-angle-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="fa fa-angle-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="tech_wizard lgo">
	<div class="container">
		<div class="row">
			<div class="top_header">
			<h1>As Seen <img alt="button_hero" src="<?php echo $base_url_temp; ?>img/template_images/o-icon.png" style="display: inline-block; top: -6px; width: 26px;">n</h1> 
			</div>   
			<div class="col-md-12">
				<ul class="media">
				<li><img src="<?php echo $base_url_temp; ?>img/template_images/img_lg1.png" alt="img_lg1"/></li>
				<li><img src="<?php echo $base_url_temp; ?>img/template_images/img_lg2.png" alt="img_lg2"/></li>
				<li><img src="<?php echo $base_url_temp; ?>img/template_images/img_lg3.png" alt="img_lg3"/></li>
				<li><img src="<?php echo $base_url_temp; ?>img/template_images/img_lg4.png" alt="img_lg4"/></li>
				</ul>

			</div>
		</div>
	</div>
</section>	   



    <section class="tech_wizard">
        <div class="container">
	     <div class="row">
	       <div class="top_header"> 
		   <h1>Come ogle our Heroes - they don't mind</h1>
		</div>
	          <div class="col-md-6">
		       <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="img1"><img src="<?php echo $base_url_temp; ?>img/template_images/img_full1.jpg" alt="img1"/></div>
			    <div role="tabpanel" class="tab-pane" id="img2"><img src="<?php echo $base_url_temp; ?>img/template_images/img_full2.jpg" alt="img1"/></div>
			    <div role="tabpanel" class="tab-pane" id="img3"><img src="<?php echo $base_url_temp; ?>img/template_images/img_full3.jpg" alt="img1"/></div>
			    <div role="tabpanel" class="tab-pane" id="img4"><img src="<?php echo $base_url_temp; ?>img/template_images/img_full4.jpg" alt="img1"/></div>
			  </div>
			
                  </div>
		 <div class="col-md-6">
		    <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#img1" aria-controls="home" role="tab" data-toggle="tab"><img src="<?php echo $base_url_temp; ?>img/template_images/img1.jpg" alt="img1"/></a></li>
			    <li role="presentation"><a href="#img2" aria-controls="profile" role="tab" data-toggle="tab"><img src="<?php echo $base_url_temp; ?>img/template_images/img2.jpg" alt="img1"/></a></li>
			    <li role="presentation"><a href="#img3" aria-controls="messages" role="tab" data-toggle="tab"><img src="<?php echo $base_url_temp; ?>img/template_images/img3.jpg" alt="img1"/></a></li>
			    <li role="presentation"><a href="#img4" aria-controls="messages" role="tab" data-toggle="tab"><img src="<?php echo $base_url_temp; ?>img/template_images/img4.jpg" alt="img1"/></a></li>
		    </ul>
		</div>
	     </div>
	</div>
    </section>
    
<script>
		// $.validate({
		//   form : '#form_applicant'
		// });
	$(document).ready(function(){
	$('.modal').on('hidden.bs.modal', function () {
   $('#login-btn')[0].reset();
   $('#register_form')[0].reset();
   
})

		//$(document).on("click","#btn_applicant",function(){
		$(document).on("submit","#form_applicant",function(event) {

			$.ajax({
				url:"<?php echo $base_url_temp; ?>index/applicants",
				data:$("#form_applicant").serialize(),
				type:"POST",
				success:function(result){
					$("#form_applicant").html(result);
				},
				error:function(error){
					console.log(error);
				}
			});
			event.preventDefault();
		});



   
    $("#phone_no").mask("(999)999-9999");
   // $('#beahero_form').validator();
     //Application of New Hero
   $("#beahero_form").validator().on("submit",function (event){
       	
       	if(event. isDefaultPrevented()){
       		return false;
       	}
event.preventDefault();
        var data = $("#beahero_form").serialize();
        $.ajax({
            type:"POST",
            url:"<?php echo $base_url_temp; ?>"+"index/beahero",
            data:data,
            success: function(result){
                var val =  JSON.parse(result);
                if(val.status=="success"){
                $("#form_msg").html('<div class="alert alert-success" role="alert">'+val.msg+'</div>');
                    $('#beahero_form')[0].reset();
                }
                else{
                    $("#form_msg").html('<div class="alert alert-danger" role="alert">'+val.msg+'</div>');
                    $('#beahero_form')[0].reset();
                }
                setTimeout(function(){ $("#form_msg").children("div").fadeOut(); }, 2000);
            },
            error: function(event){}
        });

    });
});
</script>
