<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<section class="slider_back parascroll " id="one">
         <div class="scrollable"></div>
           <div class="container">
         <div class="row">
           <div class="col-md-10">
             <div class="slide_cont">   
                 <form class="form-signin" action="" method="post" id="password_reset">
                     <div id="ci_msg1" class="alert alert-success" style="display:none;">
                                    Your Password changed successfully.
                                </div>
                                <div id="error_pass"></div>
                    <h4 class="form-signin-heading">New Password</h4>
                      <label for="inputPassword" class="sr-only">Password</label>

                    <input type="password" pattern=".{6,}" maxlength="20" name="new_password"  id="confpassword" class="form-control input-lg" placeholder="New Password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')" tabindex="5" required>
                  <h4 class="form-signin-heading">Confirm New Password</h4>
                 <input type="password" pattern=".{6,}"  maxlength="20" data-match="password" id="matchpassword" data-match-error="Whoops, these don't match" class="form-control input-lg" placeholder="Confirm New Password " onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Confirm Password should be minimum of 6 letters')"  tabindex="6" required>

                 <p></p>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
         
        </form>
             
               
             </div>
           </div>
        </div>
         </div>
    </section>
  <?php  $reset_id=@$_GET['code']; ?>
     <script type="text/javascript">
               $(document).ready(function()
                 { 
                    //Password Info Change
                        $(document).on("submit","#password_reset",function (event){
                            var match_pass = $('#matchpassword').val();
                            var conf = $('#confpassword').val();
                            
                            event.preventDefault();
                            var data = $("#password_reset").serialize();
                            if(conf==match_pass)
                            {
                                $.ajax({
                                    type:"POST",
                                    url:"<?php echo $base_url_temp; ?>"+"index/forgot_pass",
                                    data:data+'&code=<?php echo $reset_id; ?>',
                                    success: function(result){
                                    if(result=='Password change successfully')
                                    {
                                         $('#error_pass').html('<div role="alert"  class="alert alert-success" >Your password has changed succefully.</div>');
                                         $('#password_reset')[0].reset();
                                            setTimeout(function(){                                        
                                              $('#error_pass').html('');
                                            window.location.href="<?php echo $base_url_temp; ?>index";

                                            }, 2000);

                                    }
                                    else
                                        {
                                            $("#ci_msg1").css({"display":"block"});
                                            setTimeout(function(){
                                              $("#ci_msg1").css({"display":"none"});
                                            }, 3000);
                                        }
                                    },
                                    error: function(event){}
                                });
                            }
                            else
                            {
                                    $("#error_pass").html('<div role="alert"  class="alert alert-danger" >Password does not match</div>');
                                            setTimeout(function()
                                            {
                                              $('#error_pass').html('');
                                            }, 3000);
                            }
                        });
            });
</script>