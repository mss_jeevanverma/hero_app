<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<section class="slider_back parascroll " id="one">
    <div class="scrollable"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slide_cont"> 

                <form class="form-signin beahero" action="" method="post" id="password_reset">

                    <h1 class="form-signin-heading">Press</h1>

                        <div class="row">
                            <div class="col-lg-12">
                               <h3> Comming Soon ...</h3>

                            </div>    
                        </div>




                    </form>

                </div>
            </div>
        </div>
    </div>
    <br><br><br><br>
</section>

<script type="text/javascript">
$(document).ready(function()
{ 
    $(".pull-left li").remove();
    $("#phone_no").mask("(999)999-9999");

    //Application of New Hero
    $(document).on("submit",".beahero",function (event){
        event.preventDefault();

        var data = $(".beahero").serialize();

        $.ajax({
            type:"POST",
            url:"<?php echo $base_url_temp; ?>"+"index/beahero",
            data:data,
            success: function(result){
                var val =  JSON.parse(result);
                if(val.status=="success"){
                    $("#form_msg").html('<div class="alert alert-success" role="alert">'+val.msg+'</div>');
                    $('.beahero')[0].reset();
                }else{
                    $("#form_msg").html('<div class="alert alert-danger" role="alert">'+val.msg+'</div>');
                    $('.beahero')[0].reset();
                }
                setTimeout(function(){ $("#form_msg").children("div").fadeOut(); }, 2000);
            },
            error: function(event){}
        });

    });
});
</script>