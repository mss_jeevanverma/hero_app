
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" data-easein="whirlIn" class="modal" id="wizards" style="display: none;">

	<div id="msform">

	</div>

</div>
<!--div class="modal fade popup" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-easein="whirlIn"  style="display: none;">


</div-->

<script type="text/javascript">
$(document).ready(function(){

	function index_start(){
		$.ajax({
			url:"<?php echo $base_url_temp; ?>steps/index",
			//data:$("#form_applicant").serialize(),
			type:"POST",
			success:function(result){
				$("#msform").html(result);
				//alert("all good"+result);
			},
			error:function(error){
				console.log(error);
			}
		});

	}

	index_start();

		// for velocity animation changes
		$( ".modal" ).each(function(index) {
			$(this).on('show.bs.modal', function (e) {
				var open = $(this).attr('data-easein');
				$('.modal-dialog').velocity('transition.' + open);
			}); 
		});

	// Runs when Steps Modal Closes (Clear Sessions)
	$('#wizards').on('hidden.bs.modal', function () {

    	$.ajax({
			url:"<?php echo $base_url_temp; ?>steps/clearall",
			type:"POST",
			success:function(result){

				if(result=='login_customer'){
					$('ul.pull-left').html("<li class='login_btn'><a href='http://www.mastersoftwaretechnologies.com/grabahero/login/logout'><i class='fa fa-lock'></i> Logout</a></li><li class='login_btn'><a href='<?php echo $base_url_temp; ?>user_dashboard'><i class='fa fa-user'></i> My Hero</a></li>");
				}
				index_start();

					//location.reload();
			},
			error:function(error){
				console.log(error);
			}
		});
	});

$(document).on('click',"#msform .close",function(event){ 

	var r = confirm("Are you sure?");
	if (r == true) {
	   $('#wizards').modal('hide');
	}
	
	
});

// Code for Month/Year picker on Modal (checkcoverage.ctp)
var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;

$.fn.modal.Constructor.prototype.enforceFocus = function() {};
// Code for Month/Year picker on Modal (checkcoverage.ctp)


});
</script>

<?php   
	//if user loges in from fb or Gamil and Steps data is in sesstion
	if(@$_SESSION['api_login'] == 'yes'){  
?>

	<script>
		$(document).ready(function(){

			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/checkcoverage",
				//data:$("#form_applicant").serialize(),
				type:"POST",
				success:function(result){
					$("#msform").html(result);
					$('#wizards').modal('show');
					//alert("all good"+result);
				},
				error:function(error){
					console.log(error);
				}
			});

	
		});
	</script>

<?php } ?>
<?php   
	//After forgot password
	if(@$_SESSION['login_popup'] == 'yes'){  
?>

	<script>
		$(document).ready(function()
		{
			  $('.bs-example-modal-lg6').modal('show');
			  $.ajax({
			url:"<?php echo $base_url_temp; ?>steps/clearall",
			type:"POST",
			success:function(result){
			},
			error:function(error){
				console.log(error);
			}
		});
		});
	</script>

<?php } ?>