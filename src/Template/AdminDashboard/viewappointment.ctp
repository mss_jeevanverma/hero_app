<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
    $appointment_data = $data["appointment_data"];
    $user_data = $data["user_data"];
    $hero_data = $data["hero_data"];
    $current_hero_data = $data["current_hero_data"];
    // print_r($current_hero_data);
    // die();

if(count($appointment_data)){

?>

<div class="appointment-content">
    <div class="row">
        
        
        <div class="col-sm-12"><h2>Appointment Information</h2></div>
    </div>
    
    
    
    <div class="row">
        <div  class="col-lg-12">
            <div class="manage-heros-table">
                <div class="panel panel-default">
                  <!-- Default panel contents -->
                

                  <!-- Table -->
                  <table class="table order">
                    <thead>
                      <tr>
                        <th  style="width:100px;">Order Id </th>
                        <th> Order Detail </th>
                        <th>Order Status  </th>
                        <th> Time and Date </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                            <?php echo $appointment_data->id; ?>
                        </td>
                        <td>
                            <?php 
                                $order_data = json_decode($appointment_data->order_data,2); 
                                echo $order_data['stepOne'].' - '.$order_data['stepTwo'];
                                echo '<p>'.$order_data['stepThree'].'<p>';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $appointment_data->status;
                                if($status==0){
                                    echo "Upcoming";
                                }elseif($status==1){
                                    echo "Completed";
                                }else{
                                    echo "Canceled";
                                }   
                            ?>
                        </td>
                        <td>
                            <?php 

                            $order_data=$appointment_data->appointment_date;
                            $appoint_date=explode(" ",$order_data);
                            $yrdata= strtotime($appoint_date['0']);
                            echo $app_date=date('M d, Y', $yrdata);
                            $time_data=strtotime($appoint_date['1']);
                            echo ' ';
                            echo $app_time=date('H:i:s A', $time_data);

                            ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
        </div>                      
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="manage-heros-table">
                <div class="panel panel-default">
                  <!-- Default panel contents -->
                <div class="panel-heading"><h3>Customer Information 
                    <?php 
               $user_status=$user_data->status;
               if($user_status==0){
               echo ' (Deactive) ';
                 } 
                    ?></h3>
                   
                 </div>

                  <!-- Table -->
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:100px;">Name</th>
                        <td>
                            <?php echo $user_data->first_name.' '.$user_data->last_name; ?> 
                        </td>
                      </tr>
                      <tr>
                        <th style="width:150px;word-break: break-all;">Email</th>
                        <td style="word-break: break-all;">
                            <?php echo $user_data->email; ?> 
                        </td>
                      </tr>
                      <tr>
                        <th>Phone No.</th>
                        <td><?php echo $user_data->phone_no; ?> </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
        
        <div class="col-sm-6">
            <div class="manage-heros-table">
               
             
         <div class="panel panel-default">
                  <!-- Default panel contents -->
                <div class="panel-heading"><h3>Assign a Hero 
               <?php 
                if(count($current_hero_data)){
                  $hero_status=$current_hero_data->status;
                    if($hero_status==0){
                    echo ' (Deactive) ';
                  } 
                }
                    ?>
                </h3></div>
            
                <div  class="asng-hero">
                   
              <div class="row">
                        <div class="col-lg-12">
                            <div  id="hero_data">
                                 <!-- Table -->
                       
                            <?php if(count($current_hero_data)){ ?>
                            <table class="table">
                            <tbody>
                                <tr>
                                    <th style="width:100px;">Name</th>
                                    <td><?php echo $current_hero_data->first_name; ?> </td>
                                 </tr>
                                  <tr>
                                        <th style="width:150px;word-break: break-all;">Email</th>
                                        <td style="word-break: break-all;"><?php echo $current_hero_data->email; ?>  </td>
                                 </tr>
                                 <tr>
                                    <th>Phone No.</th>
                                     <td><?php echo $current_hero_data->phone_no; ?> </td>
                                 </tr>
                            </tbody>
                          </table>
                            <?php }
                            else{ 
                           if($appointment_data->status==1){                           

                            }
                            else{
                             echo "<h4> No Hero Assigned Yet </h4>";  
                            }
                          } ?>    
                            </div>
                        </div>    
                    </div>
                    
                    
                    
                    
                     <form class="navbar-form navbar-left" role="search">
                       <?php if($appointment_data->status==1){
                            echo '<div class="panel-heading" ><h5>Payment Completed</h5><div>';
                          }
                          else{
                         ?>
                    <div id='assign_msg'></div>
                        <div class="form-group">
                        <div id="change_html">
                            <?php 
                            // if Hero Not Assigned 
                            if(!count($current_hero_data)){  
                            ?>
                                <select id='here_select_id' class="form-control"  >
                                    <option value="0" >Select a Hero</option>
                                    <?php foreach ($hero_data as $hero){ ?>
                                    <option value="<?php echo $hero->id; ?>" >
                                    <?php echo $hero->first_name.' '.$hero->last_name; ?>
                                    </option>
                                <?php } ?>
                                </select>

                            <?php 
                            // if Hero Already Assigned
                            }else{ 

                            ?>
                                <button class="btn btn-primary action-button " id="btn_cancel_assign" type="button">Cancel Assign</button>
                            <?php } ?>
                        </div>    
                        </div>
                        &nbsp;&nbsp;
                        <?php if(!count($current_hero_data)){ // if Hero Not Assigned?>
                        
                         
                        <button class="btn btn-primary action-button hero-creat" id="btn_assign" type="button">Assign</button>
                        <?php }} ?>
                    </form>
            
                </div>
                </div>


              </div>
        </div>
    </div>
         </div>
        </div>
    </div>
    
    <?php if($appointment_data->status==1){ ?>
            
    <div class="row">
        <div class="col-lg-12 paid-amount">
            <h3>Payments </h3>
            <p>Amount Payed:
              <span>
              <?php $total_hour=$appointment_data->add_hour;
                        echo '$ '.$total_pay=$total_hour*79; 
        
                        ?>
                </span></p>
        </div>                      
    </div>
    <?php } ?>
    
</div>

<?php }else{ echo "<h1>Order Data Not Present Or Order Id Invaid</h1>"; }  ?>                
                    
   


<script>
$(document).ready(function() {

    $('#here_select_id').on('change', function() {

      var hero_id = this.value ;
      if(hero_id==0){
        $('#hero_data').html('<h4>Select A Hero To Assign</h4>');
        return false;
      }

      $.ajax({
            type:"POST",
            url:'<?php echo $base_url_temp; ?>'+'admin_dashboard/getcurhero/',
            data:{"hero_id":hero_id},
            success: function(result){
                
                var obj = JSON.parse(result);
                var html = '<table class="table"><tbody><tr><th style="width:100px;">Name</th><td> '+obj.first_name+'</td></tr>'+'<tr><th style="width:150px;">Email</th><td style="word-break: break-all";> '+obj.email+'</td></tr>'+'<tr><th>Phone No.</th><td> '+obj.phone_no+'</td></tr></tr></tbody></table>';
                $('#hero_data').html(html);

            },
            error: function(e){}
        });

    });

    $("#btn_assign").on("click",function(){
        var hero_id = $('#here_select_id').val();

        if(hero_id==0){
            return false;
        }

        $.ajax({
            type:"POST",
            url:'<?php echo $base_url_temp; ?>'+'admin_dashboard/setcurhero/',
            data:{"hero_id":hero_id, "order_id":"<?php echo $appointment_data->id; ?>"},
            success: function(result){
                if(result=='done'){
                    $('#assign_msg').html('<div class="alert alert-success" role="alert">Hero Assigned To This Order</div>');
                    $('#btn_assign').remove(); // remvoe assigne button
                    $('#change_html').html('<button class="btn btn-primary action-button " id="btn_cancel_assign" type="button">Cancel Assign</button>'); // revmove select add cancel button
                }else{
                    $('#assign_msg').html('<div class="alert alert-danger" role="alert">Something Went Wrong Try Again Later</div>');
                }
                setTimeout(function(){ $('#assign_msg').html(''); }, 3000);

            },
            error: function(e){}
        });

    });
    
    // Cancel a Assignee 
    $(document).on('click',"#btn_cancel_assign",function(){
        
        $.ajax({
            type:"POST",
            url:'<?php echo $base_url_temp; ?>'+'admin_dashboard/cancelcurhero/',
            data:{"order_id":"<?php echo $appointment_data->id; ?>"},
            success: function(result){
                location.reload();
            },
            error: function(e){}
        });

    });

    $('#heros').DataTable();
    $('#heroForm').validator()
} );

</script>
