<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
?>

				

						 <div class="row heding1_tab"> 
                            
                            <div class="col-md-6"><h2>Statics</h2></div>
                            
                        </div>
						
						
						<div class="row">
							<div  class="col-lg-12">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									

									  <!-- Table -->
									  <table class="table">
										
										<tbody>
										  <tr>
											<td>Total Number of Heros : </td> 
											<th><?php echo $data['total_hero_result'];?></th>
										  </tr>
										  
										  <tr>
											<td>Number of Active Heros :  </td>
											<th><?php echo $data['total_hero_act_result'];?></th>
										  </tr>
										  <tr>
											<td>Number of Non Active Heros :</td>
											<th><?php echo $data['total_hero_inact_result'];?></th>
										  </tr>
										  <tr>
											<td>Total Number of Customers : </td>
											<th><?php echo $data['total_user_result'];?></th>
										  </tr>
										  <tr>
											<td>Number of Active Customers :  </td>
											<th><?php echo $data['total_act_result'];?></th>
										  </tr>
										  <tr>
											<td>Number of Non Active Customers :  </td>
											<th><?php echo $data['total_inact_result'];?></th>
										  </tr>
										  <tr>
											<td>Total Number of Appointmets : </td>
											<th><?php echo $data['total_appointment_res'];?></th>
										  </tr>
										  <tr>
											<td>Number of Upcoming Appointmets :</td>
											<th><?php echo $data['total_uppcoming_res'];?></th>
										  </tr>
										  <tr>
											<td>Number of Completed Appointmets : </td>
											<th><?php echo $data['total_complete_res'];?></th>
										  </tr>
										   <tr>
											<td>Number of Cancel Appointmets : </td>
											<th><?php echo $data['total_cancel_res'];?></th>
										  </tr>
										</tbody>
									  </table>
									</div>
								  </div>
							</div>						
						</div>
