<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    
    $user_info = $this->request->session()->read('UserInfo');
    
?>

<div class="row">

      <?php if (@$_GET["val"]=="create") { ?>                                    
                                     <center id="page_message" styel="display:block;"><div role="alert"  class="alert alert-success" >Page has been create successfully </div></center>
                                                                        
                                <?php }
                                    else if(@$_GET["val"]=="error"){?>
                                        <center id="page_message" styel="display:block;"><div role="alert"  class="alert alert-danger" >Title url already exist</div></center>
                                  <?php  }
                                 if(isset($_GET["val"]))
                                    {
                                      ?>
                                       <script type="text/javascript">
                                         $(document).ready( function() {
  
                                            setTimeout(function()
                                            {
                                            $("#page_message").html('');
                                            }, 2000);
                                            });
                                  </script> 

                                       <?php 
                                    }

                                 ?>
                        <h2 class="text-center">Create New Page</h2>
                        
                        <div class="col-lg-12">
                             
        <form role="form" method="post" class="account_fm_rt account_fm fv-form fv-form-bootstrap" action="" id="pagesForm">
                              
                                <div class="form-group">
                               <label>Title</label>
                                <input type="text" name="title" data-error="Only Characters" pattern="[a-zA-Z\s]+" maxlength="100" data-minlength="1" class="form-control" placeholder="Title" tabindex="1" required>
                                <div class="help-block with-errors"></div>
                              </div> 
                               <div class="form-group">
                                <label>Description</label>
                                <textarea  name="description" data-error="Enter page description"  data-minlength="1" id="description" id="editor" class="form-control ckeditor"   placeholder="Description" tabindex="2" required></textarea>
                                <div class="help-block with-errors"></div>
                              </div>           
                                                         
            
            <input type="hidden"  value="1" name="status" />
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-md-6"><input type="submit" value="Create Page" class="btn btn-primary btn-block btn-lg crete_custmr" tabindex="7"></div>
                
            </div>
        </form>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#pagesForm').validator();
    });

</script>
