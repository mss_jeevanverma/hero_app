<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
?>



<div class="row">

     <?php if (@$_GET["val"]=="create") { ?>                                    
                                     <center id="email_message" styel="display:block;"><div role="alert"  class="alert alert-success" >Hero has been Create Successfully </div></center>
                                                                        
                                <?php }
                                    else if(@$_GET["val"]=="error"){?>
                                        <center id="email_message" styel="display:block;"><div role="alert"  class="alert alert-danger" >Email Already Exist</div></center>
                                  <?php  }
                                 if(isset($_GET["val"]))
                                    {
                                      ?>
                                       <script type="text/javascript">
                                         $(document).ready( function() {
  
                                            setTimeout(function()
                                            {
                                            $("#email_message").html('');
                                            }, 2000);
                                            });
                                  </script> 

                                       <?php 
                                    }

                                 ?>
                        <h2 class="text-center">Create New Hero</h2>
                        
                        <div class="col-lg-6 col-lg-offset-3">


        <form role="form" method="post" action="" class="account_fm_rt account_fm fv-form fv-form-bootstrap" id="heroForm">
            
                             <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" data-error="Only Characters " pattern="[a-zA-Z]+" maxlength="100" data-minlength="1" class="form-control" placeholder="First Name" tabindex="1" value="<?php if(isset($_GET["f"]))
                                    { echo $_GET["f"]; } ?>" required>
                                <div class="help-block with-errors"></div>
                              </div> 
                               <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" data-error="Only Characters "  maxlength="100" pattern="[a-zA-Z]+" data-minlength="1" id="last_name" class="form-control" placeholder="Last Name" value="<?php if(isset($_GET["l"])){ echo $_GET["l"]; } ?>" tabindex="2" required>
                                <div class="help-block with-errors"></div>
                              </div>           
                                <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" name="phone_no" id="display_name" class="form-control" placeholder="Phone Number" data-minlength="5"  maxlength="14" data-error="Please add valid Phone Number" value="<?php if(isset($_GET["p"]))
                                    { echo $_GET["p"]; } ?>" tabindex="3" required>
                                  <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                <label>Email ID</label>
                                 <input type="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" id="email" data-error="Please add a valid Email Address"  class="form-control " placeholder="Email Address" tabindex="4"  required>
                                    <div class="help-block with-errors"></div>
                                </div>
           
                                <div class="form-group">
                                       <label>Password</label>
                                 <input type="password" pattern=".{6,}" data-error="Password should be minimum of 6 letters" maxlength="20" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5" required>
                                    <div class="help-block with-errors"></div>
                                </div>               
                      
                                <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password"  data-match="#password"  data-match-error="Password and Confirm Password Mismatched" class="form-control input-lg" placeholder="Confirm Password" tabindex="6" required>
                                <div class="help-block with-errors"></div>
                                       
                        </div>
                        
                        <input type="hidden"  value="3" name="role" />
                        <input type="hidden"  value="1" name="status" />
                       
                        <div class="row">
                            <div class="col-xs-12 col-md-6"><input type="submit" value="Create Hero" class="btn btn-primary btn-block btn-lg crete_hero" tabindex="7">
                           </div>
                
                        </div>
        </form>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#heros').DataTable();    
    $('#heroForm').validator();
    $("#display_name").mask("(999)999-9999");
} );

</script>
