<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
    $pages = @$data["page_data"];
    $paginator = $this->Paginator;
?>


                <div class="row">
                    <div class="col-sm-12 heding_tab"><h2>Manage Pages</h2></div>
                            <div class="col-xs-6 search-section">
                                <form id="custom-search-form" method="post" class="form-search">
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default searching-btn" type="submit">

                                            <i class="fa fa-search"></i></button>
                                      </span>
                                       <input type="text" value="<?php if(isset($_POST['keyword'])) echo $_POST['keyword']; ?>" name="keyword" class="form-control" placeholder="Search by Title" required>

                                    </div><!-- /input-group -->

                                </form>
                                <?php if(isset($_POST['keyword'])) { ?>
                                    <a href="<?php echo $base_url_temp; ?>admin_dashboard/get_pages" class="btn btn-default  btn-xs reset_button">
                                        <i class="fa fa-refresh"></i> Reset Search
                                    </a>
                                <?php } ?>
                            </div>
                            
                            <div class="col-sm-6 text-right create-section"><a href="<?php echo $base_url_temp; ?>admin_dashboard/createpages" class="btn btn-primary action-button hero-creat"><i class="fa fa-plus-circle"></i> Create New Page</a></div>



                </div>


                <div class="row">
                            <div  class="col-lg-13">
                                <div class="manage-heros-table">
                                    <div class="panel panel-default">
                                    <?php if (count($pages)){ ?>
                                        <table id="customers" class="table">
                                        <thead>
                                            <tr>
                                                <th style="width:200px;">Title</th>
                                                <th>Description</th>
                                                <th style="width:100px;">Status</th>
                                                <!--th>Phone No.</th-->
                                                <th style="width:170px;">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($pages as $page) { ?>
                                            <tr>
                                                <td><?php echo $page->title; ?></td>
                                                <td><?php 
                                                    echo substr($page->description, 0, 100);
                                                  ?></td>
                                               
                                                <td><?php if($page->status) {echo "Enable"; }else{echo "Disable";} ?></td>
                                                <!--td><?php echo $customer->phone_no; ?></td-->
                                                <td>
                                                    <a href="<?php echo $base_url_temp; ?>admin_dashboard/editpages/?id=<?php echo $page->id; ?>">
                                                         Edit
                                                    </a>/
                                                    <!--<a class="a_delete" href="<?php echo $base_url_temp; ?>admin_dashboard/editcustomer/" data="id=<?php echo $customer->id; ?>&del=yes" >
                                                         Delete
                                                    </a>/-->
                                                    <a class="a_active" href="<?php echo $base_url_temp; ?>admin_dashboard/editpages/" data = "id=<?php echo $page->id; ?>&status=<?php echo $page->status; ?>" >
                                                        <?php if ($page->status) { ?> 
                                                            Disable
                                                        <?php }else{ ?>
                                                             Enable
                                                        <?php } ?>
                                                    </a>

                                                </td>
                                            </tr>
                                        <?php } ?>    
                                        </tbody>
                                        </table>
                             </div>
                        </div>
            </div>
    </div>
        <center>
        <div class='pagination'>
        <?php 
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){
                echo $paginator->prev("Prev");
            }
            echo $paginator->numbers(array('modulus' => 2));
            if($paginator->hasNext()){
                echo $paginator->next("Next");
            }
            echo $paginator->last("Last");
        ?>
        </div>
    </center>
</div>               
<?php 
    }else{
        echo "<h3><center>No Pages Found.</center></h3>";    
    } ?>              

<script>
$(document).ready(function() {

    $(".a_active").click(function (event){ 

        if(!confirm("Are You Sure ?")){
            // $(this).css({"color": "#337ab7"});
            return false;
        }
        event.preventDefault();

        var url = $(this).attr("href");
        var data = $(this).attr("data");

        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
               window.location.href = window.location.href;
            },
            error: function(e){}
        });

    });
    
    $(".a_delete").click(function (event){
        event.preventDefault();
        var conf=  confirm('Are you sure?');
        if(!conf){ 
             //$(this).css({"color": "#337ab7"});
            return false; }
        var url = $(this).attr("href");
        var data = $(this).attr("data");
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                location.reload();
            },
            error: function(e){}
        });
        
    });

});
</script>
