<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
    $appointments = @$data["appointments"];

    $paginator = $this->Paginator;
?>


<div class="row">
<div class="col-sm-12 heding_tab"><h2>View Appointments</h2></div>
                       
 <div class="col-xs-6 search-section">

        <form method="post" action="" class="form-search">
            <div class="input-group">
                
                 <span class="input-group-btn">
                    <button class="btn btn-default searching-btn" type="submit">
                         <i class="fa fa-search"></i>
                     </button>
                    </span>
                <input type="text" name="keyword" value="<?php if(isset($_POST['keyword'])) echo $_POST['keyword']; ?>" class="form-control serch" placeholder="Search by Service" required>
            </div>  
        </form>
        <?php if(isset($_POST['keyword'])) { ?>
            <a href="<?php echo $base_url_temp; ?>admin_dashboard/getappointments" class="btn btn-default  btn-xs reset_button">
                <i class="fa fa-refresh"></i> Reset Search
            </a>
        <?php } ?>
    </div>

        <!--div class="col-sm-6 text-right create-section"><a href="<?php echo $base_url_temp; ?>admin_dashboard/createcustomer" class="btn btn-primary action-button hero-creat"><i class="fa fa-plus-circle"></i> Create New Customer</a>
      </div-->

</div>

<div class="row">
          <div  class="col-lg-13">
                   <div class="manage-heros-table">
                           <div class="panel panel-default">
                                <?php if (count($appointments)){ ?>
                                <table id="customers" class="table">
                                <thead>
                                    <tr>
                                        <th  style="width:150px;">Order Id</th>

                                        <th>Service</th>
                                        <th>Status</th>
                                        <!--th>Customer Name</th-->
                                        
                                        <th style="width:150px;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($appointments as $appointment) { ?>
                                    <tr>
                                        <td><?php echo $appointment->id; ?></td>
                                        <td>
                                            <?php 
                                                $order_data = json_decode($appointment->order_data,2);
                                                echo $order_data['stepOne']." - ".$order_data['stepTwo'];
                                                if(strlen($order_data['stepThree'])>50){
                                               echo '<p>'.substr($order_data['stepThree'], 0, 50).'...</p>';
                                            }
                                            else{
                                                 echo '<p>'.$order_data['stepThree'].'</p>';
                                            }
                                            ?>
                                            </td>

                                            <td>
                                                <?php 
                                                    $status = $appointment->status;
                                                    if($status==0){
                                                        echo "Upcomming";
                                                    }elseif($status==1){
                                                        echo "Completed";
                                                    }else{
                                                        echo "Canceled";
                                                    }   
                                                ?>
                                            </td>
                                                           

                                        <td>
                                            <a href="<?php echo $base_url_temp; ?>admin_dashboard/viewappointment/?id=<?php echo $appointment->id; ?>">
                                                <button class="btn btn-primary action-button hero-creat" type="button">View</button>
                                            </a>


                                        </td>
                                    </tr>
                                <?php } ?>    
                             </tbody>
                    </table>
                </div>
            </div>
      </div>
</div>
        <center>
        <div class='pagination'>
        <?php 
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){
                echo $paginator->prev("Prev");
            }
            echo $paginator->numbers(array('modulus' => 2));
            if($paginator->hasNext()){
                echo $paginator->next("Next");
            }
            echo $paginator->last("Last");
        ?>
        </div>
    </center>
</div>               
<?php 
    }else{
        echo "<h3><center>No Customer Found.</center></h3>";    
    } ?>              

<script>
$(document).ready(function() {

    $(".a_active").click(function (event){
        event.preventDefault();

        var url = $(this).attr("href");
        var data = $(this).attr("data");
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                location.reload();
            },
            error: function(e){}
        });

    });
    
    $(".a_delete").click(function (event){
        event.preventDefault();
        var conf=  confirm('Are you sure?');
        if(!conf){ return false; }
        var url = $(this).attr("href");
        var data = $(this).attr("data");
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                location.reload();
            },
            error: function(e){}
        });
        
    });

});
</script>
