<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
    $heros = @$data["heros"];

    $paginator = $this->Paginator;
?>
<div class="row">
    <div class="col-sm-12 heding_tab"><h2 class="">Manage Heros</h2></div>
                            <div class="col-xs-6 search-section">
                                <form id="custom-search-form" method="post" class="form-search">
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default searching-btn" type="submit">

                                            <i class="fa fa-search"></i></button>
                                      </span>
                                       <input type="text" value="<?php if(isset($_POST['keyword'])) echo $_POST['keyword']; ?>" name="keyword" class="form-control serch" placeholder="Search by Email" required>

                                    </div><!-- /input-group -->

                                </form>
                                <?php if(isset($_POST['keyword'])) { ?>
                                    <a href="<?php echo $base_url_temp; ?>admin_dashboard/" class="btn btn-default  btn-xs reset_button">
                                        <i class="fa fa-refresh"></i> Reset Search
                                    </a>
                                <?php } ?>
                            </div>

                            
                            <div class="col-sm-6 text-right create-section"><a href="<?php echo $base_url_temp; ?>admin_dashboard/createhero" class="btn btn-primary action-button hero-creat"><i class="fa fa-plus-circle"></i> Create New Hero</a></div>



                        </div>


                   <div class="row">
                            <div  class="col-lg-13">
                                <div class="manage-heros-table">
                                    <div class="panel panel-default">
                                      <!-- Default panel contents -->
                                    <?php if (count($heros)){ ?>
                                        <table id="heros" class="table">
                                        <thead>
                                            <tr>
                                                <!--th>No.</th-->
                                                <th  style="width:200px;">Name</th>
                                                <th>Email</th>
                                                <th  style="width:100px;">Status</th>
                                                <!--th>Phone No.</th-->
                                                <th  style="width:170px;">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sr_no=1; foreach($heros as  $hero) { ?>
                                            <tr>
                                                <!--td><?php echo $sr_no; ?></td-->
                                                <td><?php echo $hero->first_name." ".$hero->last_name; ?></td>
                                                <td><?php echo $hero->email; ?></td>
                                                <td><?php if($hero->status) {echo "Active"; }else{echo "Not Active";} ?></td>
                                                <!--td><?php echo $hero->phone_no; ?></td-->
                                                <td>
                                                    <a href="<?php echo $base_url_temp; ?>admin_dashboard/edithero/?id=<?php echo $hero->id; ?>">
                                                         Edit
                                                    </a> / 
                                                    <!--<a class="a_delete" href="<?php echo $base_url_temp; ?>admin_dashboard/edithero/" data="id=<?php echo $hero->id; ?>&del=yes" >
                                                        Delete
                                                    </a> / -->
                                                    <a class="a_active" href="<?php echo $base_url_temp; ?>admin_dashboard/edithero/" data ="id=<?php echo $hero->id; ?>&status=<?php echo $hero->status; ?>" >
                                                        <?php if ($hero->status) { ?> 
                                                           Deactivate
                                                        <?php }else{ ?>
                                                             Activate
                                                        <?php } ?>
                                                    </a>

                                                </td>
                                            </tr>
                                        <?php $sr_no++; } ?>    
                                        </tbody>
                                        </table>
                                     </div>
                                 </div>
                            </div>
                 </div>
    <center>
        <div class='pagination'>
        <?php 
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){
                echo $paginator->prev("Prev");
            }
            echo $paginator->numbers(array('modulus' => 2));
            if($paginator->hasNext()){
                echo $paginator->next("Next");
            }
            echo $paginator->last("Last");
        ?>
        </div>
    </center>
</div> 

<?php }else{
    echo "<h3><center>No Hero Found.</center></h3>";    
    } ?>              

<script>
$(document).ready(function() {
    $(".a_active").click(function (event){
        event.preventDefault();

        if(!confirm("Are You Sure ?")){
             //$(this).css({"color": "#337ab7"});
            return false;
        }

        var url = $(this).attr("href");
        var data = $(this).attr("data");
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                window.location.href = window.location.href;
            },
            error: function(e){}
        });

    });
    $(".a_delete").click(function (event){
        event.preventDefault();
        var conf=  confirm('Are you sure?');
        if(!conf){ 
 //$(this).css({"color": "#337ab7"});
            return false; }
        var url = $(this).attr("href");
        var data = $(this).attr("data");
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                location.reload();
            },
            error: function(e){}
        });
        
    });

});
</script>
