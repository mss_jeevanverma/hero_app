<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $hero = $data["hero"][0];

?>

<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
        <h2>Account Information</h2>
        <div class="col-md-6">
            <div class="account_left">
        <form class="account_fm" role="form"  method="post" action="" id="heroinfo">
                        <div id="ci_msg" class="alert alert-success" style="display:none;">
                            Your Account information changed successfully.
                        </div>
                        <h3>Update Account</h3>
                            <label>First Name</label>
                                <input type="text" name="first_name"  id="first_name" pattern="[a-zA-Z]{1,100}"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')" maxlength="20" class="form-control input-lg" placeholder="First Name" tabindex="1" value="<?php echo $hero->first_name; ?>"  required>
                                
                                            <div class="help-block with-errors"></div>
                                
                                <label>Last Name</label>
                                <input type="text" name="last_name" pattern="[a-zA-Z]{1,100}"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"   id="last_name" class="form-control input-lg"  value="<?php echo $hero->last_name; ?>" placeholder="Last Name" tabindex="2" required>
                               
                                            <div class="help-block with-errors"></div>
                               
                                <label>Email ID</label>
                                <input type="email" placeholder="testing@gmail.com"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')"  class="form-control input-lg" tabindex="4" value="<?php echo $hero->email; ?>" disabled/>
                                         <div class="help-block with-errors"></div>
                    <div class="submition">
                                <div class="col-md-4 spacing">
                                    <div class="update_changes">
                                        <button  id="basic_info" class="btn btn-primary btn-block btn-lg">Update Changes </button>
                                    </div>
                                </div>
                        
                                           <div class="col-md-4">
                                                <div class="cancel">
                                                    <!-- <button id="clear_info">Cancel</button> -->
                                                </div>
                                            </div>
                                    <div class="col-md-4 kk">
                                            <div class="update_changes">
                                                <button id="change_pass" class="btn btn-primary btn-block btn-lg">Change Password</button>
                                            </div>
                                    </div> 
                    </div>
        </form>
       
                    
                    
                    
        <form class="account_fm" role="form" method="post" action="" id="heropass" style="display:none;">            
                                <div id="ci_msg1" class="alert alert-success" style="display:none;">
                                    Your Account information changed successfully.
                                </div>
                                <div id="error_pass"></div>
                     <h3>Change Password</h3>
                    <label>Current Password</label>
                                    <input type="password" pattern=".{6,}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity(' Password should be minimum of 6 letters')" name="password" id="password" class="form-control input-lg" placeholder="Password"  tabindex="5" required>
                                            <div class="help-block with-errors"></div>
                                                    <label> New Password </label>
                                            <input type="password" pattern=".{6,}" name="new_password"  id="confpassword" maxlength="20" class="form-control input-lg" placeholder="Password"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity(' Password should be minimum of 6 letters')"  tabindex="5" required>
                                                 <div class="help-block with-errors"></div>
                                                        <label>Confirm New Password</label>
                                            <input type="password" pattern=".{6,}"   data-match="password" id="matchpassword" data-match-error="Whoops, these don't match" class="form-control input-lg" maxlength="20" placeholder="Confirm New Password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity(' Confirm Password should be minimum of 6 letters')"  tabindex="6" required>
                                                    <div class="help-block with-errors"></div>
                                                    <div class="submition">
                                                        <div class="col-md-3 spacing">
                                                            <div class="apply">
                                                                     <button id="password_info" class="btn btn-primary btn-block btn-lg">Apply</button>
                                                          </div>
                                                        </div>
                                                         <div class="col-md-3 kk">
                                                            <div class="cancel">
                                                               <!-- <button>Cancel</button> -->
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                    
        </form>           
                    
                    
                    
    <form class="account_fm" role="form" method="post" action="" id="herophone">           
                     <div id="ci_msg2" class="alert alert-success" style="display:none;">
                            Your Account information changed successfully.
                        </div>
                    <h3>Update Contact</h3>
                    <label>Phone Number</label>
                                 <input type="text" name="phone_no" id="phone_no" pattern=".{10,}" class="form-control input-lg" placeholder="Phone Number" minlength="10" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity(' Enter a valid Phone Number')"   maxlength="13" data-match-error="Please add valid Phone Number" tabindex="3" value="<?php echo $hero->phone_no; ?>" required>
                                <div class="help-block with-errors"></div>
                                                                   
                         
                                    <div class="submition">
                                        <div class="col-md-4 spacing">
                                            <div class="apply_1">
                                                <button  id="phone_info" class="btn btn-primary btn-block btn-lg" >Update Changes</button>
                                             </div>
                                        </div>
                                         <div class="col-md-3 kk">
                                            <div class="cancel">
                                               <!-- <button>Cancel</button> -->
                                            </div>
                                        </div>
                                       
                                    </div>
                    
         </form>            
               
        </div>
        </div>
       
    </div>
    </div>
</div>

 </section> 
                  <style type="text/css">
                  .ui-datepicker-calendar {
                      display: none;
                   }
                  </style>
                   
    <script type="text/javascript">
    $(document).ready(function()
    { 
      //$('.account_left small').css({'display':'block'});
      //$('.help-block',this).hide();
             $("#phone_no").mask("(999)999-9999");
             // $("#a_exp_date").mask("99/99");

                
                      $("#myTab a").click(function(e)
                      {
                        e.preventDefault();
                        $(this).tab('show');
                      });
                      $("#change_pass").click(function(event)
                      {
                     event.preventDefault();
                     $("#heropass").toggle();
                      });
                     

     
                        //Hero Info Change
                        $(document).on("submit","#heroinfo",function (event){
                            event.preventDefault();
                            var data = $("#heroinfo").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"hero_dashboard/edithero",
                                data:data,
                                success: function(result){
                                    $("#ci_msg").css({"display":"block"});
                                    setTimeout(function(){
                                              $("#ci_msg").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                        });

                     //Password Info Change
                        $(document).on("submit","#heropass",function (event){
                            var match_pass = $('#matchpassword').val();
                            var conf = $('#confpassword').val();
                            
                            event.preventDefault();
                            var data = $("#heropass").serialize();
                            if(conf==match_pass)
                            {
                                $.ajax({
                                    type:"POST",
                                    url:"<?php echo $base_url_temp; ?>"+"hero_dashboard/editpassword",
                                    data:data,
                                    success: function(result){
                                    if(result=='Current Password is wrong')
                                    {
                                         $('#error_pass').html('<div role="alert"  class="alert alert-danger" >Current Password is wrong</div>');
                                            setTimeout(function(){
                                              $('#error_pass').html('');
                                            }, 3000);
                                    }
                                    else
                                        {
                                            $("#ci_msg1").css({"display":"block"});
                                            setTimeout(function(){
                                              $("#ci_msg1").css({"display":"none"});
                                            }, 3000);
                                        }
                                    },
                                    error: function(event){}
                                });
                            }
                            else
                            {
                                    $("#error_pass").html('<div role="alert"  class="alert alert-danger" >Password and Confirm Password Mismatched</div>');
                                            setTimeout(function()
                                            {
                                              $('#error_pass').html('');
                                            }, 3000);
                            }
                        });
                         //Phone Info Change
                        $(document).on("submit","#herophone",function (event)
                        {
                            event.preventDefault();
                            var data = $("#herophone").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"hero_dashboard/edithero",
                                data:data,
                                success: function(result){
                                    $("#ci_msg2").css({"display":"block"});
                                     setTimeout(function()
                                            {
                                             $("#ci_msg2").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                       });

     
              


                    $('.help-block').remove();

   });
</script>  

<!-- new jquery add -->

   <script>
    $(function(){
        $('.parascroll').parascroll();
    })
</script>
<!-- write script to toggle class on scroll -->
<script>
$(window).scroll(function() {
    if ($(this).scrollTop() > 20){  
        $('.navbar-fixed-top').addClass("sticky");
    }
    else{
        $('.navbar-fixed-top').removeClass("sticky");
    }
});




//Code stolen from css-tricks for smooth scrolling:
$(function() {
  $('.circle_mat a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>