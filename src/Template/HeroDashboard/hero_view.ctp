<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>
<?php

    $user_info = $this->request->session()->read('UserInfo');
    $appointment_data = @$data["appointment_data"];
    $customer_data = @$data["customer_data"];
    $hero_data = @$data["hero_data"];

     if ( count($appointment_data) && count($hero_data) ){?>
<section class="appointment">
	<div class="container">
    	<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="manage-heros">
										
					<div class="appointment-content">
						<div class="row">
							
							
							<div class="col-sm-12"><h2>Appointment Information</h2></div>
						</div>
						
						
						
						<div class="row">
							<div  class="col-lg-12">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									

									  <!-- Table -->
									  <table class="table">
										<thead>
										  <tr>
											<th>Order Id </th>
											<th> Order Detail </th>
											<th>Order Status  </th>
											<th> Time and Date </th>
										  </tr>
										</thead>
										<tbody>
										  <tr>
											<td><?php echo $appointment_data[0]->id; ?></td>
											<td><?php $order_data=$appointment_data[0]->order_data;
											 $order_content=json_decode($order_data,2);
											 		echo $order_data_all=$order_content['stepOne'].'-'.$order_content['stepTwo'];

											?>
										<p><?php echo  $order_data=$order_content['stepThree']; ?></p></td>
											<td><?php 
												$order_date=$appointment_data[0]->appointment_date;
					                            $appoint_date=explode(" ",$order_date);
					                            $yrdata= strtotime($appoint_date['0']);
					                            $app_date=date('M d, Y', $yrdata);
					                            $time_data=strtotime($appoint_date['1']); 
												$app_time=date('H:i:s A', $time_data);
												//Calculate difference
					                            $diff=$yrdata-time();//time returns current time in seconds
					                            $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)                 
					                           
											 if($days<-1)
					                            {
					                             echo '<h4>Appointment Date Passed</h4>';   
					                            }
											else if ($appointment_data[0]->status==0)
													{
														echo ' Upcomming ';

													} 
													else
													{
														echo 'Completed';
													}
													?>
												</td>
											<td><?php 
					                            echo '<p>'.$app_date.' '.$app_time. '</p>';
                            				?></td>
										  </tr>
										</tbody>
									  </table>
									</div>
								  </div>
							</div>						
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									<div class="panel-heading"><h3>Customer Information
											 <?php 
							               $user_data=$customer_data[0]->status;
							               if($user_data==0){
							               echo ' (Deactive) ';
							                 } 
							                    ?>
									</h3></div>
										<?php if(count($customer_data)){ ?>
												  <!-- Table -->
												  <table class="table">
													<tbody>
													  <tr>
														<th style="width:150px">Name</th>
														<td><?php echo $customer_data[0]->first_name; ?> </td>
													  </tr>
													  <tr>
														<th style="width:150px">Email</th>
														<td style="word-break: break-all;" ><?php echo $customer_data[0]->email; ?> </td>
													  </tr>
													  <tr>
														<th>Phone no.</th>
														<td><?php echo $customer_data[0]->phone_no; ?> </td>
													  </tr>
													  
													</tbody>
												  </table>
									 	<?php }
									 else{
												echo '<h1>No Hero Assigned Yet</h1>';
									 }?>
									</div>
								  </div>
							</div>
							
							<div class="col-sm-6">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									<div class="panel-heading"><h3>Add Hours to Appointment</h3></div>
									<?php 
					               $user_status=$customer_data[0]->status;
					               if($user_status==0){?>
					               <span id="update_hour">

				                        					<?php if($appointment_data[0]->add_hour)
															{ 
																$total_hour=$appointment_data[0]->add_hour;
																$total_pay=$total_hour*79;
															    echo 'Current Hour is : '.$total_hour.' Hours &#10005; $79 = $'.$total_pay;
															}
																else{
																echo '<h5>No Hour Add Yet</h5>';
													       			 }?>
												</span>
												<?php
					                 } 
					       			 else if($user_status==1) { ?>
					         <form class="account_fm" role="form"  method="post" action="" id="add_hour">
                                   <div id="ci_msg">
                                   
                      				</div>
                                            <div class="panel-heading" >
                                            	<span id="update_hour">

				                        					<?php if($appointment_data[0]->add_hour)
															{ 
																$total_hour=$appointment_data[0]->add_hour;
																$total_pay=$total_hour*79;
															    echo 'Current Hour is : '.$total_hour.' Hours &#10005; $79 = $'.$total_pay;
															}
																else{
																echo '<h5>No Hour Add Yet</h5>';
													       			 }?>
												</span>
													        <!--span class="panel-heading" id="update_result"></span-->
													    </div>
									<?php if($appointment_data[0]->status==1){
							       
							       		echo '<div class="panel-heading" ><h5>Payment Completed</h5></div>';
							      
							   		 } 
							   		else{ ?>
                                <input type="text"  pattern="^0*[1-9][0-9]*(\.[0-9]+)?" oninvalid="this.setCustomValidity('Only Numaric Value grater than 0')" onchange="this.setCustomValidity('')" userid="<?php echo $appointment_data[0]->id; ?>" name="add_hour" maxlength="2" minlength="1" class="form-control input-lg" title="Add Only Numeric Value" placeholder="Update Hours" tabindex="1" value=""  required>
                                
											<div  class="asng-hero">
											<button class="btn btn-primary action-button hero-creat" type="submit">Update Hour</button>
										    </div> 
										    <?php } ?>
										  
									</form>	  
								<?php }?>
									</div>
									</div>
								  </div>
							</div>
						</div>
						
						
						<?php if($appointment_data[0]->status==1){ ?>
						<div class="row">
							<div class="col-lg-12 paid-amount">
								<h3>Payments</h3>
								<p>Amount Payed:<span>$ <?php echo $total_pay; ?></span></p>
							</div>						
						</div>
						<?php } ?>

						<div class="row">

							<ul class="nav navbar-nav pull-left nav_log" style ="width:100%;margin-left: 13%;">
								<li class="maked" style="float: left;">
								<a target="_blank" class="videoChat btn btn-primary action-button hero-creat" class="btn btn-default dropdown-toggle" type="button" >Video Conference</a></li>
								<li class="maked" style="float: left;">
								<a target="_blank" class="screenSharing btn btn-primary action-button hero-creat" class="btn btn-default dropdown-toggle" type="button" >Screen Sharing</a></li>
								<li class="maked" style="float: left;">
								<a target="_blank" class="textChat btn btn-primary action-button hero-creat" class="btn btn-default dropdown-toggle" type="button">Text Chat</a></li>
							</ul>
						</div>
						
					</div>
					
					
				</div>
			</div>
		</div>
    </div>
</section>
 <?php }
									 else{
//echo '<h1>Invalid Request</h1>';
$url =  $base_url_temp."hero_dashboard/hero_info";
header("Location: ".$url);
 die;
									 }?>

	<script type="text/javascript">
		$(document).ready(function(){
            
					//Hour  Info Change
                        $(document).on("submit","#add_hour",function (event){
                            event.preventDefault();
                            var add_hour=$("#add_hour").serialize();
                            	var id='<?php echo $appointment_data[0]->id; ?>';
                            	//alert(add_hour);
                              $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"hero_dashboard/add_hour",
                                data:add_hour+'&id='+id,
                                success: function(result){
                                	console.log(result);
                                	var total_hour=$("input").val();
                                	
                                	 //$("#update_hour").css({"display":"none"});
                                	 var pay_info='Current Hour is : '+total_hour+' Hours &#10005; $79 = $'+total_hour*79;
                                	$("#update_hour").html(pay_info);

                                	if(result=="Update Information"){
                                    	$("#ci_msg").html('<div  class="alert alert-success">Your Hour information changed successfully.</div>');
                                	}else if (result=="Hero Not Assigned"){
                                		$("#ci_msg").html('<div  class="alert alert-danger">You Are Not Assigned To This Task</div>');
                             setTimeout(function(){    window.location.href ="<?php echo $base_url_temp; ?>"+"hero_dashboard/hero_info";      }, 2000);
                                	}

                                    $('#add_hour')[0].reset();
                                    setTimeout(function(){        	
                                              	
                                                $("#ci_msg").html("");          
                                              	
                                            }, 2000);
                                },
                                error: function(event){}
                            });
                        });

		});
	</script>
<?php
$base_url_temp_secure="https://" . $_SERVER['SERVER_NAME']."/";

 ?>
	<script type="text/javascript">
	$(document).ready(function (){
		var randUrlText = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
		var fullUrlVideo =  "<?php echo $base_url_temp; ?>share/videoConference" + randUrlText; 
		var fullUrlScreen =  "<?php echo $base_url_temp_secure; ?>share/screenSharing" + randUrlText; 
		var fullUrlText =  "<?php echo $base_url_temp; ?>share/textChat" + randUrlText; 
		$(".videoChat").attr({"href":fullUrlVideo});
		$(".screenSharing").attr({"href":fullUrlScreen});
		$(".textChat").attr({"href":fullUrlText});
	});
</script>
