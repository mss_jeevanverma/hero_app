<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
        <?php if(count($data)) { ?>
        <div class="col-md-10 col-md-offset-1">
        <h1 style="margin-left:0px">
            <?php echo ucwords($data[0]['title']); ?>
        </h1>
            <?php echo $data[0]['description']; ?>
        </div>
        <?php 
            }else{

                echo "<div class='col-md-10 col-md-offset-1'>
                    <h1>Page Not Found</h1>
                </div>";
            } ?>
    </div>
    </div>
</div>

 </section> 
