<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <title>Hero- The Digital Handyman</title>
    <?= $this->Html->meta('icon') ?>

    
    <?= $this->Html->css('bootstrap.min.css') ?>
    <!--?= $this->Html->css('font-awesome.css') ?> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('stylesheet.css') ?>
    <?= $this->Html->css('jquery.parascroll.css') ?>
   <?= $this->Html->css('jquery.datetimepicker.css') ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <?= $this->Html->script('jquery-1.9.1.min.js') ?> 
    <!-- Masked Input Link -https://github.com/digitalBush/jquery.maskedinput -->
    <?= $this->Html->script('jquery.maskedinput.min.js') ?> 
    <!--script src="https://raw.githubusercontent.com/digitalBush/jquery.maskedinput/master/dist/jquery.maskedinput.min.js"></script-->


    <!-- for Credit Card validation Link http://formvalidation.io/validators/creditCard/ -->
    <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>   
    <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>

    <?= $this->Html->script('bootstrap.min.js') ?>    
    <?= $this->Html->script('jquery.parascroll.js') ?>  
    <!--script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script-->
    <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <?= $this->Html->script('jquery.datetimepicker.full.js') ?> 
<!-- Date picker css and js Link : https://tarruda.github.io/bootstrap-datetimepicker/
 <link rel="stylesheet" type="text/css" media="screen"
     href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
<script type="text/javascript"  src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"> 

</script>--> 
<!-- Velocity.js for animation effects -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js'></script>
  
    </head>
    <body>
    <?php 
        @$session = $this->request->session();
        @$user_info = $session->read('UserInfo');
    ?>  
    <!-- header start here -->


<?php
    $user_info = $this->request->session()->read('UserInfo');
    ?>

    <!-- header start here -->
    <div class="navbar navbar-default navbar-fixed-top sticky">
      <div class="container"> 
        <div class="navbar-header">
        <!--button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button-->
        <a href="<?php echo $base_url_temp; ?>" class="navbar-brand"><img src="<?php echo $base_url_temp; ?>img/template_images/logo.png" alt="logo"/></a>
        <ul class="nav navbar-nav pull-left nav_log">
          <!--li class="login_btn"><a href="#"><i class="fa fa-lock"></i> Login</a></li-->
          <?php 
            if(count($user_info))
            { 
          ?>    
                <li class="login_btn"><a href="<?php echo $base_url_temp; ?>login/logout"><i class="fa fa-lock"></i> Logout</a></li>

                <?php 
                $myAccountUrl = '';
                  if ($user_info['role']==2) { $myAccountUrl = 'user_dashboard'; }
                  else if ($user_info['role']==3) { $myAccountUrl = 'hero_dashboard'; }
                  else if ($user_info['role']==1) { $myAccountUrl = 'admin_dashboard'; }

                  ?>
                <li class="login_btn"><a href="<?php echo $base_url_temp.$myAccountUrl; ?>"><i class="fa fa-user"></i> My Hero</a></li>


          <?php }else{ ?>
               
                <li class="login_btn"><a href="#" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg6"><i class="fa fa-lock"></i> Login</a></li>
          <?php  }
          ?> 
        </ul>
        <ul class="nav navbar-nav navbar-right  nav_log">
          <li><i class="fa fa-mobile"></i> (888) 969 4376</li>
        </ul>
   
              
        </div>
      </div>  
    </div>

    <!-- header end here -->

    <?php echo $this->element('head'); ?>
    <?php echo $this->element('login'); ?>
     <?= $this->fetch('content') ?>
      
<?php echo $this->element('foot'); ?>

<!--popup html close here-->
   
   <script>
          $(function(){
            $('.parascroll').parascroll();
          })
</script>
<!-- write script to toggle class on scroll -->
<script>
          $(window).scroll(function() {
              if ($(this).scrollTop() > 20){  
                  $('.navbar-fixed-top').addClass("sticky");
              }
              else{
                  $('.navbar-fixed-top').removeClass("sticky");
              }
          });

                //this is where we apply opacity to the arrow
                $(window).scroll( function(){

                  //get scroll position
                  var topWindow = $(window).scrollTop();
                  //multipl by 1.5 so the arrow will become transparent half-way up the page
                  var topWindow = topWindow * 1.5;
                  
                  //get height of window
                  var windowHeight = $(window).height();
                      
                  //set position as percentage of how far the user has scrolled 
              var position = topWindow / windowHeight;
              //invert the percentage
              position = 1 - position;

              //define arrow opacity as based on how far up the page the user has scrolled
              //no scrolling = 1, half-way up the page = 0
              $('.arrow-wrap').css('opacity', position);

            });




//Code stolen from css-tricks for smooth scrolling:
$(function() {
  $('.circle_mat a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>

<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script>
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
  if(animating) return false;
  animating = true;
  
  current_fs = $(this).parent();
  next_fs = $(this).parent().next();
  
  //activate next step on progressbar using the index of next_fs
  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
  
  //show the next fieldset
  next_fs.show(); 
  //hide the current fieldset with style
  current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale current_fs down to 80%
      scale = 1 - (1 - now) * 0.2;
      //2. bring next_fs from the right(50%)
      left = (now * 50)+"%";
      //3. increase opacity of next_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({'transform': 'scale('+scale+')'});
      next_fs.css({'left': left, 'opacity': opacity});
    }, 
    duration: 800, 
    complete: function(){
      current_fs.hide();
      animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});

$(".previous").click(function(){
  if(animating) return false;
  animating = true;
  
  current_fs = $(this).parent();
  previous_fs = $(this).parent().prev();
  
  //de-activate current step on progressbar
  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
  
  //show the previous fieldset
  previous_fs.show(); 
  //hide the current fieldset with style
  current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale previous_fs from 80% to 100%
      scale = 0.8 + (1 - now) * 0.2;
      //2. take current_fs to the right(50%) - from 0%
      left = ((1-now) * 50)+"%";
      //3. increase opacity of previous_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({'left': left});
      previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
    }, 
    duration: 800, 
    complete: function(){
      current_fs.hide();
      animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});

$(".submit").click(function(){
  return false;
})

</script>


<script type="text/javascript">
  $('.speaker-inner').on('click', function(){
      $('.speaker-inner').removeClass('active');
      $(this).addClass('active');
  });

  $('.installed-proceduer li').on('click', function(){
      $('.installed-proceduer li').removeClass('active');
      $(this).addClass('active');
  }); 
</script>
    </body>

</html>
