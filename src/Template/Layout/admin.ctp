<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

    $user_info = $this->request->session()->read('UserInfo');
    
?>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <title>Admin</title>
    <?= $this->Html->meta('icon') ?>    
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <!--?= $this->Html->css('font-awesome.css') ?> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?= $this->Html->css('stylesheet.css') ?>
    <?= $this->Html->css('jquery.parascroll.css') ?> 
      
      <?= $this->Html->script('ckeditor/ckeditor.js') ?>    
     <?= $this->Html->script('jquery-1.9.1.min.js') ?> 
         <?= $this->Html->script('jquery.maskedinput.min.js') ?>    
    <?= $this->Html->script('bootstrap.min.js') ?>    
    <?= $this->Html->script('jquery.parascroll.js') ?>  
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
  </head>
   <body>
  
    <?php echo $this->element('head'); ?>
    
    <section class="appointment">
  <div class="container">
      <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="manage-heros">
          <?php $control=$this->request->controller;?>
          <?php $action=$this->request->action;?>
          <ul class="appointment-tab">
            <li  <?php if(($control=='AdminDashboard')&&($action=='index' || $action=='createhero' || $action=='edithero') ) { echo 'class="active"';} ?> ><a href="<?php echo $base_url_temp; ?>admin_dashboard"><i class="fa fa-table"></i> <span>Manage Heros </span></a></li>
            <li <?php if(($control=='AdminDashboard')&&($action=='getcustomer' || $action=='editcustomer' || $action=='createcustomer')){echo 'class="active"'; } ?> ><a href="<?php echo $base_url_temp; ?>admin_dashboard/getcustomer"><i class="fa fa-users"></i><span> Manage Customers</span> </a></li>
            <li <?php if($control=='Testimonials' ) { echo 'class="active"'; } ?> ><a href="<?php echo $base_url_temp; ?>testimonials"><i class="fa fa-comments"></i> <span>Manage Testimonials</span>  </a></li>
            <li <?php if($action=='getappointments'|| $action=='viewappointment') { echo 'class="active"';}?> ><a href="<?php echo $base_url_temp; ?>admin_dashboard/getappointments" style="border:none;"><i class="fa fa-coffee"></i> <span>Appointments </span> </a></li>
          </ul>
          <div class="appointment-content">
        
            <!--li ><a href="<?php echo $base_url_temp; ?>admin_dashboard">Manage Heros</a></li>
            <li ><a href="<?php echo $base_url_temp; ?>admin_dashboard/getcustomer">Manage Customers</a></li>
            <li ><a href="<?php echo $base_url_temp; ?>testimonials">Manage Testimonials</a></li>
            <li><a href="<?php echo $base_url_temp; ?>login/logout">Logout</a></li -->
     
                   <?= $this->fetch('content') ?>    
   
   
            </div>
        </div>
      </div>
    </div>
    </div>
</section>

<?php echo $this->element('foot'); ?>
  
    
   <script>
  $(function(){
    $('.parascroll').parascroll();
  })
</script>
<!-- write script to toggle class on scroll -->
<script>
$(window).scroll(function() {
    if ($(this).scrollTop() > 20){  
        $('.navbar-fixed-top').addClass("sticky");
    }
    else{
        $('.navbar-fixed-top').removeClass("sticky");
    }
});

//this is where we apply opacity to the arrow
$(window).scroll( function(){

  //get scroll position
  var topWindow = $(window).scrollTop();
  //multipl by 1.5 so the arrow will become transparent half-way up the page
  var topWindow = topWindow * 1.5;
  
  //get height of window
  var windowHeight = $(window).height();
      
  //set position as percentage of how far the user has scrolled 
  var position = topWindow / windowHeight;
  //invert the percentage
  position = 1 - position;

  //define arrow opacity as based on how far up the page the user has scrolled
  //no scrolling = 1, half-way up the page = 0
  $('.arrow-wrap').css('opacity', position);

});




//Code stolen from css-tricks for smooth scrolling:
$(function() {
  $('.circle_mat a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
// $(document).ready(function(){
//   $('appointment-tab li ').click(function() {
//       $('li').removeClass('active');
//       $(this).addClass('active');
//   });
// });
</script>
    </body>

</html>
