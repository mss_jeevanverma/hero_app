<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php $data = h($msg);?>
<section class="slider_back parascroll " id="one">
    <div class="scrollable"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="slide_cont">   
                <h2>Evolution of the Handyman</h2>
                <p>They have been around for millennia, but not until now has the handyman evolved to serve the digital age. Hero connects you with local technical experts to identify, diagnose and resolve your every digital need. </p>

                <a href="javascript:void()" data-toggle="modal" data-target="#myModal"><img src="<?php echo $base_url_temp; ?>img/template_images/button_hero.png" alt="button_hero"/></a>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container"><div class="circle_mat arrow-wrap" href="#content"><a  href="#content"><i class="fa fa-angle-down"></i></a></div>
        <div class="row">
            <center>
                <form  class="form-signin" action="" method="post">
                    <h2 class="form-signin-heading">Login Form</h2>
                    <?php if(strlen($data)>3){ ?>
                    <div class="alert alert-danger">
                        <strong><?php echo $data; ?></strong>
                    </div>
                    <?php } ?>
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <a href="register" >Create a New User</a><br>
                    <a href="http://mastersoftwaretechnologies.com/grabahero/login_api
" class="btn btn-info" >Login Using Facebook</a><br>
                    <a href="http://mastersoftwaretechnologies.com/grabahero/login_api/gmail" class="btn btn-info" >Login Using Gmail</a><br>

                </form>
            </center>
    </div>
</section>  
