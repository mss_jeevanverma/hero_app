<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $customer = $data["customer"][0];

?>
 <script src="//cdn.webrtc-experiment.com/firebase.js"> </script>
<script src="//cdn.webrtc-experiment.com/RTCPeerConnection-v1.5.js"> </script>
<script src="//cdn.webrtc-experiment.com/video-conferencing/conference.js"> </script>
<!-- script used to stylize video element -->
<script src="//cdn.webrtc-experiment.com/getMediaElement.min.js"> </script>


<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
          <div class="container">

            <div class="row">

              <div class="col-sm-6 col-sm-offset-3">

        
                      <!-- <HTML code for you> ::: just copy this section! -->
                <table class="visible">
                    <tr>
                        <td style="text-align: right;">
                            <input type="text" id="conference-name" value="<?php echo rand(1,3000).rand(1,300).rand(1,3000); ?>" placeholder="Hangout Name..." style="display:none;">
                        </td>
                 
                        <td>

                            <button id="start-conferencing" href="#" <?php 
              if($user_info['role']==2)
              {
                echo  'style="display:none;"' ; } ?> >Start Chat-Hangout</button>
                        </td>
                       
                    </tr>
                </table>

                <table id="rooms-list" class="visible"></table>

<!--                 <table class="visible">
                    <tr>
                        <td style="text-align: center;">
                            <strong>Private chat-hangout</strong> ?? <a href="" target="_blank"
                                                                        title="Open this link in new tab. Then your chat-hangout room will be private!"><code><strong     id="unique-token">#123456789</strong></code></a>
                        </td>
                    </tr>
                </table> -->

                <table id="chat-table" class="center-table hidden">
                    <tr>
                        <td style="text-align: center;">
                            <input type="text" id="chat-message" placeholder="Write Message &amp; Hit Enter" disabled>
                            <button id="post-chat-message" class="btn btn-primary action-button hero-creat">Post Message</button>
                        </td>
                    </tr>
                </table>
                <table id="chat-output" class="hidden"></table>

         <div class="send_link">    <a data-target=".bs-example-modal-lg16" class="btn btn-primary action-button hero-creat" data-toggle="modal" data-dismiss="modal" href="#"><i class="fa fa-share-alt"></i>Share Link</a>
         </div>
            </section>


            <script src="https://cdn.webrtc-experiment.com/firebase.js"> </script>
            <script src="https://cdn.webrtc-experiment.com/RTCPeerConnection-v1.5.js"> </script>
            <script src="https://cdn.webrtc-experiment.com/chat-hangout/hangout.js"> </script>
<script>            


var config = {
    openSocket: function(config) {


        var channel = config.channel || location.href.replace( /\/|:|#|%|\.|\[|\]/g , '');
        var socket = new Firebase('https://webrtc.firebaseIO.com/' + channel);

        socket.channel = channel;
        socket.on("child_added", function(data) {
            config.onmessage && config.onmessage(data.val());
        });

        socket.send = function(data) {
            this.push(data);
        };

        config.onopen && setTimeout(config.onopen, 1);
        socket.onDisconnect().remove();
        return socket;
    },
    onRoomFound: function(room) {
        var alreadyExist = document.getElementById(room.broadcaster);
        if (alreadyExist) return;

        if (typeof roomsList === 'undefined') roomsList = document.body;

        var tr = document.createElement('tr');
        tr.setAttribute('id', room.broadcaster);
        tr.innerHTML = '<td style="display:none;">' + room.roomName + '</td>' +
            '<td style="display:none;" ><button class="join" id="' + room.roomToken + '">Join</button></td>';

 //tr.innerHTML = '<td >' + room.roomName + '</td>' +
            //'<td  ><button class="join" id="' + room.roomToken + '">Join</button></td>';


        roomsList.insertBefore(tr, roomsList.firstChild);

        //tr.onclick = function() {
            //tr = this;
            hangoutUI.joinRoom({
                roomToken: tr.querySelector('.join').id,
                joinUser: tr.id,
                userName: prompt('Enter your name', 'Anonymous')
            });
            hideUnnecessaryStuff();
        //};
    },
    onChannelOpened: function(/* channel */) {
        hideUnnecessaryStuff();
    },
    onChannelMessage: function(data) {
        if (!chatOutput) return;

        var tr = document.createElement('tr');
		tr.className ="user-right-message";
        tr.innerHTML =
            '<td style="width:40%;">' + data.sender + '</td>' +
                '<td>' + data.message + '</td>';

        chatOutput.insertBefore(tr, chatOutput.firstChild);
    }
};

function createButtonClickHandler() {
    hangoutUI.createRoom({
        userName: prompt('Enter your name', 'Anonymous'),
        roomName: (document.getElementById('conference-name') || { }).value || 'Anonymous'
    });
    hideUnnecessaryStuff();
}


/* on page load: get public rooms */
var hangoutUI = hangout(config);

/* UI specific */
var startConferencing = document.getElementById('start-conferencing');
if (startConferencing) startConferencing.onclick = createButtonClickHandler;
var roomsList = document.getElementById('rooms-list');

var chatOutput = document.getElementById('chat-output');

function hideUnnecessaryStuff() {
    var visibleElements = document.getElementsByClassName('visible'),
        length = visibleElements.length;

    for (var i = 0; i < length; i++) {
        visibleElements[i].style.display = 'none';
    }

    var chatTable = document.getElementById('chat-table');
    if (chatTable) chatTable.style.display = 'block';
    if (chatOutput) chatOutput.style.display = 'block';
    if (chatMessage) chatMessage.disabled = false;
    $('#chat-table').removeClass("hidden");
    $('#chat-table').addClass("visible");
    $('#chat-output').removeClass("hidden");
    $('#chat-output').addClass("visible");
	  $('#chat-table').css({"display":""});
}

var chatMessage = document.getElementById('chat-message');
if (chatMessage)
    chatMessage.onchange = function() {
        hangoutUI.send(this.value);
        var tr = document.createElement('tr');
        tr.innerHTML =
            '<td>You:</td>' +
                '<td>' + chatMessage.value + '</td>';

        chatOutput.insertBefore(tr, chatOutput.firstChild);
        chatMessage.value = '';
    };


(function() {
    var uniqueToken = document.getElementById('unique-token');
    if (uniqueToken)
        if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank"></a></h2>';
        else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
})();
</script>
            
            <!-- </HTML code for you> ::: end copy this section! -->
            

              </div>
            

            </div>
<!--div class="row">
         <div class="send_link">    <a data-target=".bs-example-modal-lg16" class="btn btn-primary action-button hero-creat editable-table" data-toggle="modal" data-dismiss="modal" href="#">Share Link</a>
         </div>
</div-->

          </div>

        </div>
    </div>
 <?php    
if($user_info['role']==3)
              {
                ?>




<!-- Share link popup html -->
      <div class="modal fade bs-example-modal-lg16 rgstrpop" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
        <div class="modal-dialog modal-lg first-register">
        
          <div class="modal-content home-login">
               <div class="modal-header">
                     <h2 class="fs-subtitle text-center">Share Link</h2>
                     <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                </div> 
            <div class="modal-body text-center">
      
              <div class="row text-center">
              <div class="col-sm-8 col-md-offset-2">
                    <form class="first-form" method="post"  id="sharelink_form" action="">
                       <div id="for_succ"></div>
                        <div class="form-group">
                           <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" placeholder="Email address" required autofocus>
                           <input type="hidden" id="full_url" value="" name="share_url" >
                           <input type="hidden" id="link_type" value="text chat" name="link_type" >
                        </div>

        
                         <div class="row f-btn">        
                         <div class="col-sm-12 text-center">
                      <button type="submit" id="share_link" class="btn btn-primary action-button hero-creat text-center">Share</button>
                            </div>
                        </div>

                 </form>
             </div>
          
            </div>
         </div>
  
    </div>
  </div>
</div>
<?php } ?>
<!-- Share popup end -->
 </div>
</div>

 </section>

<script type="text/javascript">
$(document).ready(function(){
$(document).on("submit","#sharelink_form",function (event){
event.preventDefault();

    var full_url="<?php echo Router::url(null, true); ?>"+window.location.hash;
    $('#full_url').val(full_url);

            var share_data=$('#sharelink_form').serialize();
            $.ajax({
            type: "POST",
            data: share_data,
            url: "<?php echo $base_url_temp; ?>share/sharelink",
            success:function(result){
               if(result=='success')
               {
               $("#for_succ").html('<div role="alert"  class="alert alert-success" >Share Link Send to Your Email Address</div>');
                    setTimeout(function(){
                      $('#for_succ').html('');
                    }, 2000);
                
               } 
             
            },
            error: function(event){}
            });
});
});



</script>





<style>
table#chat-table {
    font-size: large;
    width: 100%;
}
input#chat-message {
    border: 1px solid #333b53 ;
    height: 40px;
    width: 50%;
    color: #000.;
    float: left;
}
input#chat-message:focus{ border:1px solid #9acd32; }
button#post-chat-message {

    background: none repeat scroll 0 0 #9acd32;
    border: 0 none;
    border-radius: 41px;
    color: white;
    cursor: pointer;
    font-size: 19px;
    font-weight: normal;
    margin: 0px;
    padding: 8px 5px;
    width: 160px;
    display: inline-block;

}
#chat-output tr {
    background-color: #9acd32;
    float: left;
    width:100%;
    margin-top: 10px;
    padding: 7px 9px;
    color: #fff; border-radius:5px;
}
.send_link{ margin-top:20px; float:left;}
</style>

