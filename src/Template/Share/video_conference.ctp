<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $customer = $data["customer"][0];
    ?>
 <script src="//cdn.webrtc-experiment.com/firebase.js"> </script>
<script src="//cdn.webrtc-experiment.com/RTCPeerConnection-v1.5.js"> </script>
<script src="//cdn.webrtc-experiment.com/video-conferencing/conference.js"> </script>
<!-- script used to stylize video element -->
<script src="//cdn.webrtc-experiment.com/getMediaElement.min.js"> </script>


<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
          <div class="container">

            <div class="row">
              <div class="col-sm-12">
                           <!-- just copy this <section> and next script -->
              
            <section class="experiment">                
                <section>
                    <span>
                        Private ?? <a href="/video-conferencing/" target="_blank" title="Open this link in new tab. Then your conference room will be private!"><code><strong id="unique-token">#123456789</strong></code></a>
                    </span>
                    
                    <input type="text" id="conference-name" style="visibility:hidden;" value="<?php echo rand(1,3000).rand(1,300).rand(1,3000); ?>" >
                 
                    <button id="setup-new-room" class="btn btn-primary btn-lg action-button setup" <?php 
              if($user_info['role']==2)
              {
                echo  'style="display:none;"' ; } ?>  ><i class="fa fa-video-camera"></i>Setup New Conference</button>
                

                </section>
                
                <!-- list of all available conferencing rooms -->
                <table style="width: 100%;" id="rooms-list"></table>
                
                <!-- local/remote videos container -->
                <div id="videos-container"></div>
            </section>
        
                    <script>
                // Muaz Khan     - https://github.com/muaz-khan
                // MIT License   - https://www.webrtc-experiment.com/licence/
                // Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/video-conferencing

                var config = {
                    openSocket: function(config) {
                        var channel = config.channel || location.href.replace( /\/|:|#|%|\.|\[|\]/g , '');
                        var socket = new Firebase('https://webrtc.firebaseIO.com/' + channel);
                        socket.channel = channel;
                        socket.on("child_added", function(data) {
                            config.onmessage && config.onmessage(data.val());
                        });
                        socket.send = function(data) {
                            this.push(data);
                        };
                        config.onopen && setTimeout(config.onopen, 1);
                        socket.onDisconnect().remove();
                        return socket;
                    },
                    onRemoteStream: function(media) {
                        var mediaElement = getMediaElement(media.video, {
                            width: (videosContainer.clientWidth / 2) - 50,
                            buttons: ['mute-audio', 'mute-video', 'full-screen', 'volume-slider']
                        });
                        mediaElement.id = media.streamid;
                        videosContainer.insertBefore(mediaElement, videosContainer.firstChild);
                    },
                    onRemoteStreamEnded: function(stream, video) {
                        if (video.parentNode && video.parentNode.parentNode && video.parentNode.parentNode.parentNode) {
                            video.parentNode.parentNode.parentNode.removeChild(video.parentNode.parentNode);
                        }
                    },
                    onRoomFound: function(room) {
                      document.getElementById('setup-new-room').style.display = "none";
                        var alreadyExist = document.querySelector('button[data-broadcaster="' + room.broadcaster + '"]');
                        if (alreadyExist) return;

                        if (typeof roomsList === 'undefined') roomsList = document.body;

                        var tr = document.createElement('tr');
                        tr.innerHTML = '<td style="display:none;" ><strong>' + room.roomName + '</strong> shared a conferencing room with you!</td>' +
                            '<td style="display:none;" ><button class="join">Join</button></td>';
                        roomsList.insertBefore(tr, roomsList.firstChild);

                        var joinRoomButton = tr.querySelector('.join');
                        joinRoomButton.setAttribute('data-broadcaster', room.broadcaster);
                        joinRoomButton.setAttribute('data-roomToken', room.roomToken);
                        //joinRoomButton.onclick = function() {
                            joinRoomButton.disabled = true;

                            var broadcaster = joinRoomButton.getAttribute('data-broadcaster');
                            var roomToken = joinRoomButton.getAttribute('data-roomToken');
                            captureUserMedia(function() {
                                conferenceUI.joinRoom({
                                    roomToken: roomToken,
                                    joinUser: broadcaster
                                });
                            }, function() {
                                joinRoomButton.disabled = false;
                            });
                       // };
                    },
                    onRoomClosed: function(room) {
                        var joinButton = document.querySelector('button[data-roomToken="' + room.roomToken + '"]');
                        if (joinButton) {
                            // joinButton.parentNode === <li>
                            // joinButton.parentNode.parentNode === <td>
                            // joinButton.parentNode.parentNode.parentNode === <tr>
                            // joinButton.parentNode.parentNode.parentNode.parentNode === <table>
                            joinButton.parentNode.parentNode.parentNode.parentNode.removeChild(joinButton.parentNode.parentNode.parentNode);
                        }
                    }
                };

                function setupNewRoomButtonClickHandler() {
                    btnSetupNewRoom.disabled = true;
                    document.getElementById('conference-name').disabled = true;
                    captureUserMedia(function() {
                        conferenceUI.createRoom({
                            roomName: (document.getElementById('conference-name') || { }).value || 'Anonymous'
                        });
                    }, function() {
                        btnSetupNewRoom.disabled = document.getElementById('conference-name').disabled = false;
                    });
                }

                function captureUserMedia(callback, failure_callback) {
                    var video = document.createElement('video');

                    getUserMedia({
                        video: video,
                        onsuccess: function(stream) {
                            config.attachStream = stream;
                            callback && callback();

                            video.setAttribute('muted', true);
                            
                            var mediaElement = getMediaElement(video, {
                                width: (videosContainer.clientWidth / 2) - 50,
                                buttons: ['mute-audio', 'mute-video', 'full-screen', 'volume-slider']
                            });
                            mediaElement.toggle('mute-audio');
                            videosContainer.insertBefore(mediaElement, videosContainer.firstChild);
                        },
                        onerror: function() {
                            alert('unable to get access to your webcam');
                            callback && callback();
                        }
                    });
                }

                var conferenceUI = conference(config);

                /* UI specific */
                var videosContainer = document.getElementById('videos-container') || document.body;
                var btnSetupNewRoom = document.getElementById('setup-new-room');
                var roomsList = document.getElementById('rooms-list');

                if (btnSetupNewRoom) btnSetupNewRoom.onclick = setupNewRoomButtonClickHandler;

                function rotateVideo(video) {
                    video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
                    setTimeout(function() {
                        video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
                    }, 1000);
                }

                (function() {
                    var uniqueToken = document.getElementById('unique-token');
                    if (uniqueToken)
                        if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank"></a></h2>';
                        else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
                })();

                function scaleVideos() {
                    var videos = document.querySelectorAll('video'),
                        length = videos.length, video;

                    var minus = 130;
                    var windowHeight = 700;
                    var windowWidth = 600;
                    var windowAspectRatio = windowWidth / windowHeight;
                    var videoAspectRatio = 4 / 3;
                    var blockAspectRatio;
                    var tempVideoWidth = 0;
                    var maxVideoWidth = 0;

                    for (var i = length; i > 0; i--) {
                        blockAspectRatio = i * videoAspectRatio / Math.ceil(length / i);
                        if (blockAspectRatio <= windowAspectRatio) {
                            tempVideoWidth = videoAspectRatio * windowHeight / Math.ceil(length / i);
                        } else {
                            tempVideoWidth = windowWidth / i;
                        }
                        if (tempVideoWidth > maxVideoWidth)
                            maxVideoWidth = tempVideoWidth;
                    }
                    for (var i = 0; i < length; i++) {
                        video = videos[i];
                        if (video)
                            video.width = maxVideoWidth - minus;
                    }
                }

                window.onresize = scaleVideos;

            </script>
            

              </div>
            </div>
          </div>

        </div>
    </div>



<?php if($user_info['role']==3)
              {
                ?>

<div class="col-sm-12 text-center">
        
        <a data-target=".bs-example-modal-lg16" class="btn btn-primary action-button hero-creat" data-toggle="modal" data-dismiss="modal" href="#"><i class="fa fa-share-alt"></i>Share Link</a>
        </div>



<!-- Share link popup html -->
      <div class="modal fade bs-example-modal-lg16 rgstrpop" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
        <div class="modal-dialog modal-lg first-register">
        
          <div class="modal-content home-login">
               <div class="modal-header">
                     <h2 class="fs-subtitle text-center">Share Link</h2>
                     <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                </div> 
            <div class="modal-body text-center">
      
              <div class="row text-center">
              <div class="col-sm-8 col-md-offset-2">

                    <form class="first-form" method="post"  id="sharelink_form" action="">
                       <div id="for_succ"></div>
                                                     
                        <div class="form-group">
                           <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" placeholder="Email address" required autofocus>
                           <input type="hidden" id="full_url" value="" name="share_url" >
                           <input type="hidden" id="link_type" value="video confrence" name="link_type" >
                        </div>

        
                         <div class="row f-btn">        
                         <div class="col-sm-12 text-center">
                      <button type="submit" id="share_link" class="btn btn-primary action-button hero-creat text-center">Share</button>
                            </div>
                        </div>

                 </form>
             </div>
          
            </div>
         </div>
  
    </div>
  </div>
</div>
<?php } ?>
<!-- Share popup end -->
 </div>
</div>

 </section>

<script type="text/javascript">
$(document).ready(function(){
$(document).on("submit","#sharelink_form",function (event){
event.preventDefault();

    var full_url="<?php echo Router::url(null, true); ?>"+window.location.hash;
    $('#full_url').val(full_url);

            var share_data=$('#sharelink_form').serialize();
            $.ajax({
            type: "POST",
            data: share_data,
            url: "<?php echo $base_url_temp; ?>share/sharelink",
            success:function(result){
               if(result=='success')
               {
               $("#for_succ").html('<div role="alert"  class="alert alert-success" >Share Link Send to Your Email Address</div>');
                    setTimeout(function(){
                      $('#for_succ').html('');
                    }, 2000);
                
               }
             
            },
            error: function(event){}
            });
});
});



</script>
