<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $customer = $data["customer"][0];

?>

<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/ajhifddimkapgcifgcodmmfdlknahffk">
<!-- scripts used for screen-sharing -->
<script src='//cdn.webrtc-experiment.com/firebase.js'> </script>
<script src="//cdn.webrtc-experiment.com/Pluginfree-Screen-Sharing/conference.js"> </script>


<section class="account_contant share-screen">
<div class="container">
<div class = "row">
            <section class="experiment">
                <h2>Prerequisites</h2>
                <ul class="install-browser">
                    <li>
                        <img src="<?php echo $base_url_temp; ?>img/template_images//chrome.png" alt="button_hero" class="logo-orgnl" height="36px"/>
                        <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank"><button id="install-button" class="btn btn-primary">Click to Install</button> </a>
                    </li>
                    
                    <li>
                        <img src="<?php echo $base_url_temp; ?>img/template_images//firefox.png" alt="button_hero" class="logo-orgnl" height="36px"/>  
                        <a href="https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/"><button class="btn btn-danger">Click to Install</button></a>
                    </li>
                </ul>

                  <div class="col-md-6 pull-right">
                    <section class="row share-link">
                    <div class="col-sm-6">
                        <input type="text" id="room-name" placeholder="Your Name" disabled style="visibility:hidden;" value="<?php echo rand(1,3000).rand(1,300).rand(1,3000); ?>">
                        </div>
                         <div class="col-sm-6">
                        <button id="share-screen" class="btn btn-primary btn-lg action-button setup" <?php 
              if($user_info['role']==2)
              {
                echo  'style="display:none;"' ; } ?>  disabled><i class="fa fa-desktop"></i> Share Your Screen</button>
                       </div>
                    </section>
                    <!-- list of all available broadcasting rooms -->
                    <table style="width: 100%;" id="rooms-list"></table>

                </div>
            </section>

</div>
    <div class="row">
        <div class="account_form s-screen">
          <div class="container">

           <!--  <div class="row">   
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <section class="share-link">
                        <input type="text" id="room-name" placeholder="Your Name" disabled style="visibility:hidden;" value="<?php echo rand(1,3000).rand(1,300).rand(1,3000); ?>">
                         
                        <button id="share-screen" class="btn btn-primary btn-lg action-button setup" <?php 
              if($user_info['role']==2)
              {
                echo  'style="display:none;"' ; } ?>  disabled><i class="fa fa-desktop"></i> Share Your Screen</button>
                       
                    </section>
                    <table style="width: 100%;" id="rooms-list"></table>

                </div>
                <div class="col-md-4"></div>

            </div>
 -->


            <div class="row">        
            <div class="col-md-12">
            <div id="videos-container"></div>
            </div>    
            </div> 
          </div>

        </div>
    </div>
   <?php
if($user_info['role']==3)
              {
                ?>
<div class="col-sm-12 text-center">
        
        <a data-target=".bs-example-modal-lg16" class="btn btn-primary action-button hero-creat" data-toggle="modal" data-dismiss="modal" href="#"><i class="fa fa-share-alt"></i>
 Share Link</a>
        </div>



<!-- Share link popup html -->
      <div class="modal fade bs-example-modal-lg16 rgstrpop" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
        <div class="modal-dialog modal-lg first-register">
        
          <div class="modal-content home-login">
               <div class="modal-header">
                     <h2 class="fs-subtitle text-center">Share Link</h2>
                     <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                </div> 
            <div class="modal-body text-center">
      
              <div class="row text-center">
              <div class="col-sm-8 col-md-offset-2">
                    <form class="first-form" method="post"  id="sharelink_form" action="">
                       <div id="for_succ"></div>
                                                 
                        <div class="form-group">
                           <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" placeholder="Email address" required autofocus>
                           <input type="hidden" id="full_url" value="" name="share_url" >
                           <input type="hidden" id="link_type" value="screen sharing" name="link_type" >
                        </div>

        
                         <div class="row f-btn">        
                         <div class="col-sm-12 text-center">
                      <button type="submit" id="share_link" class="btn btn-primary action-button hero-creat text-center">Share</button>
                            </div>
                        </div>

                 </form>
             </div>
          
            </div>
         </div>
  
    </div>
  </div>
</div>
<?php } ?>
<!-- Share popup end -->
 </div>
</div>

 </section>

<script type="text/javascript">
$(document).ready(function(){
$(document).on("submit","#sharelink_form",function (event){
event.preventDefault();

    var full_url="<?php echo Router::url(null, true); ?>"+window.location.hash;
    $('#full_url').val(full_url);

            var share_data=$('#sharelink_form').serialize();
            $.ajax({
            type: "POST",
            data: share_data,
            url: "<?php echo $base_url_temp; ?>share/sharelink",
            success:function(result){
              if(result=='success')
               {
               $("#for_succ").html('<div role="alert"  class="alert alert-success" >Share Link Send to Your Email Address</div>');
                 setTimeout(function(){
                      $('#for_succ').html('');
                    }, 2000);
                
               } 
             
            },
            error: function(event){}
            });
});
});



</script>
 <script>
    function intallFirefoxScreenCapturingExtension() {
        InstallTrigger.install({
            'Foo': {
                // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                URL: 'https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness',
                toString: function() {
                    return this.URL;
                }
            }
        });
    }

    // Muaz Khan     - https://github.com/muaz-khan
    // MIT License   - https://www.webrtc-experiment.com/licence/
    // Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/Pluginfree-Screen-Sharing
    
    var isWebRTCExperimentsDomain = document.domain.indexOf('webrtc-experiment.com') != -1;

    var config = {
        openSocket: function(config) {
            var channel = config.channel || 'screen-capturing-' + location.href.replace( /\/|:|#|%|\.|\[|\]/g , '');
            var socket = new Firebase('https://webrtc.firebaseIO.com/' + channel);
            socket.channel = channel;
            socket.on("child_added", function(data) {
                config.onmessage && config.onmessage(data.val());
            });
            socket.send = function(data) {
                this.push(data);
            };
            config.onopen && setTimeout(config.onopen, 1);
            socket.onDisconnect().remove();
            return socket;
        },
        onRemoteStream: function(media) {
            var video = media.video;
            video.setAttribute('controls', true);
            videosContainer.insertBefore(video, videosContainer.firstChild);
            video.play();
            rotateVideo(video);
        },
        onRoomFound: function(room) {
            if(location.hash.replace('#', '').length) {
                // private rooms should auto be joined.
                conferenceUI.joinRoom({
                    roomToken: room.roomToken,
                    joinUser: room.broadcaster
                });
                return;
            }
            
            var alreadyExist = document.getElementById(room.broadcaster);
            if (alreadyExist) return;

            if (typeof roomsList === 'undefined') roomsList = document.body;

            var tr = document.createElement('tr');
            tr.setAttribute('id', room.broadcaster);
            tr.innerHTML = '<td>' + room.roomName + '</td>' +
                '<td><button class="join" id="' + room.roomToken + '">Open Screen</button></td>';
            roomsList.insertBefore(tr, roomsList.firstChild);

            var button = tr.querySelector('.join');
            button.onclick = function() {
                var button = this;
                button.disabled = true;
                conferenceUI.joinRoom({
                    roomToken: button.id,
                    joinUser: button.parentNode.parentNode.id
                });
            };
        },
        onNewParticipant: function(numberOfParticipants) {
            document.title = numberOfParticipants + ' users are viewing your screen!';
            var element = document.getElementById('number-of-participants');
            if (element) {
                element.innerHTML = numberOfParticipants + ' users are viewing your screen!';
            }
        },
        oniceconnectionstatechange: function(state) {
            if(state == 'failed') {
                alert('Failed to bypass Firewall rules. It seems that target user did not receive your screen. Please ask him reload the page and try again.');
            }
            
            if(state == 'connected') {
                alert('A user successfully received your screen.');
            }
        }
    };

    function captureUserMedia(callback, extensionAvailable) {
        console.log('captureUserMedia chromeMediaSource', DetectRTC.screen.chromeMediaSource);
        
        var screen_constraints = {
            mandatory: {
                chromeMediaSource: DetectRTC.screen.chromeMediaSource,
                maxWidth: screen.width > 1920 ? screen.width : 1920,
                maxHeight: screen.height > 1080 ? screen.height : 1080
                // minAspectRatio: 1.77
            },
            optional: [{ // non-official Google-only optional constraints
                googTemporalLayeredScreencast: true
            }, {
                googLeakyBucket: true
            }]
        };

        // try to check if extension is installed.
        if(isChrome && isWebRTCExperimentsDomain && typeof extensionAvailable == 'undefined' && DetectRTC.screen.chromeMediaSource != 'desktop') {
            DetectRTC.screen.isChromeExtensionAvailable(function(available) {
                captureUserMedia(callback, available);
            });
            return;
        }
        
        if(isChrome && isWebRTCExperimentsDomain && DetectRTC.screen.chromeMediaSource == 'desktop' && !DetectRTC.screen.sourceId) {
            DetectRTC.screen.getSourceId(function(error) {
                if(error && error == 'PermissionDeniedError') {
                    alert('PermissionDeniedError: User denied to share content of his screen.');
                }
                
                captureUserMedia(callback);
            });
            return;
        }
        
        // for non-www.webrtc-experiment.com domains
        if(isChrome && !isWebRTCExperimentsDomain && !DetectRTC.screen.sourceId) {
            window.addEventListener('message', function (event) {
                if (event.data && event.data.chromeMediaSourceId) {
                    var sourceId = event.data.chromeMediaSourceId;

                    DetectRTC.screen.sourceId = sourceId;
                    DetectRTC.screen.chromeMediaSource = 'desktop';

                    if (sourceId == 'PermissionDeniedError') {
                        return alert('User denied to share content of his screen.');
                    }

                    captureUserMedia(callback, true);
                }

                if (event.data && event.data.chromeExtensionStatus) {
                    warn('Screen capturing extension status is:', event.data.chromeExtensionStatus);
                    DetectRTC.screen.chromeMediaSource = 'screen';
                    captureUserMedia(callback, true);
                }
            });
            screenFrame.postMessage();
            return;
        }
        
        if(isChrome && DetectRTC.screen.chromeMediaSource == 'desktop') {
            screen_constraints.mandatory.chromeMediaSourceId = DetectRTC.screen.sourceId;
        }
        
        var constraints = {
            audio: false,
            video: screen_constraints
        };
        
        if(!!navigator.mozGetUserMedia) {
            console.warn(Firefox_Screen_Capturing_Warning);
            constraints.video = {
                mozMediaSource: 'window',
                mediaSource: 'window',
                maxWidth: 1920,
                maxHeight: 1080,
                minAspectRatio: 1.77
            };
        }
        
        console.log( JSON.stringify( constraints , null, '\t') );
        
        var video = document.createElement('video');
        video.setAttribute('autoplay', true);
        video.setAttribute('controls', true);
        videosContainer.insertBefore(video, videosContainer.firstChild);
        
        getUserMedia({
            video: video,
            constraints: constraints,
            onsuccess: function(stream) {
                config.attachStream = stream;
                callback && callback();

                video.setAttribute('muted', true);
                rotateVideo(video);
            },
            onerror: function() {
                if (isChrome && location.protocol === 'http:') {
                    alert('Please test this WebRTC experiment on HTTPS.');
                } else if(isChrome) {
                    alert('Screen capturing is either denied or not supported. Please install chrome extension for screen capturing or run chrome with command-line flag: --enable-usermedia-screen-capturing');
                }
                else if(!!navigator.mozGetUserMedia) {
                    alert(Firefox_Screen_Capturing_Warning);
                }
            }
        });
    }

    /* on page load: get public rooms */
    var conferenceUI = conference(config);

    /* UI specific */
    var videosContainer = document.getElementById("videos-container") || document.body;
    var roomsList = document.getElementById('rooms-list');

    document.getElementById('share-screen').onclick = function() {
        var roomName = document.getElementById('room-name') || { };
        roomName.disabled = true;
        captureUserMedia(function() {
            conferenceUI.createRoom({
                roomName: (roomName.value || 'Anonymous') + ' shared his screen with you'
            });
        });
        this.disabled = true;
    };

    function rotateVideo(video) {
        video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
        setTimeout(function() {
            video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
        }, 1000);
    }

    (function() {
        var uniqueToken = document.getElementById('unique-token');
        if (uniqueToken)
            if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank">Share this link</a></h2>';
            else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
    })();
    
    var Firefox_Screen_Capturing_Warning = 'Make sure that you are using Firefox Nightly and you enabled: media.getusermedia.screensharing.enabled flag from about:config page. You also need to add your domain in "media.getusermedia.screensharing.allowed_domains" flag.';

</script>

<script>
    var screenFrame, loadedScreenFrame;
    
    function loadScreenFrame(skip) {
        if(loadedScreenFrame) return;
        if(!skip) return loadScreenFrame(true);

        loadedScreenFrame = true;

        var iframe = document.createElement('iframe');
        iframe.onload = function () {
            iframe.isLoaded = true;
            console.log('Screen Capturing frame is loaded.');
            
            document.getElementById('share-screen').disabled = false;
            document.getElementById('room-name').disabled = false;
        };
        iframe.src = 'https://www.webrtc-experiment.com/getSourceId/';
        iframe.style.display = 'none';
        (document.body || document.documentElement).appendChild(iframe);

        screenFrame = {
            postMessage: function () {
                if (!iframe.isLoaded) {
                    setTimeout(screenFrame.postMessage, 100);
                    return;
                }
                console.log('Asking iframe for sourceId.');
                iframe.contentWindow.postMessage({
                    captureSourceId: true
                }, '*');
            }
        };
    };
    
    if(!isWebRTCExperimentsDomain) {
        loadScreenFrame();
    }
    else {
        document.getElementById('share-screen').disabled = false;
        document.getElementById('room-name').disabled = false;
    }
</script>

<script>
    // todo: need to check exact chrome browser because opera also uses chromium framework
    var isChrome = !!navigator.webkitGetUserMedia;
    
    // DetectRTC.js - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/DetectRTC
    // Below code is taken from RTCMultiConnection-v1.8.js (http://www.rtcmulticonnection.org/changes-log/#v1.8)
    var DetectRTC = {};

    (function () {
        
        var screenCallback;
        
        DetectRTC.screen = {
            chromeMediaSource: 'screen',
            getSourceId: function(callback) {
                if(!callback) throw '"callback" parameter is mandatory.';
                screenCallback = callback;
                window.postMessage('get-sourceId', '*');
            },
            isChromeExtensionAvailable: function(callback) {
                if(!callback) return;
                
                if(DetectRTC.screen.chromeMediaSource == 'desktop') return callback(true);
                
                // ask extension if it is available
                window.postMessage('are-you-there', '*');
                
                setTimeout(function() {
                    if(DetectRTC.screen.chromeMediaSource == 'screen') {
                        callback(false);
                    }
                    else callback(true);
                }, 2000);
            },
            onMessageCallback: function(data) {
                if (!(typeof data == 'string' || !!data.sourceId)) return;
                
                console.log('chrome message', data);
                
                // "cancel" button is clicked
                if(data == 'PermissionDeniedError') {
                    DetectRTC.screen.chromeMediaSource = 'PermissionDeniedError';
                    if(screenCallback) return screenCallback('PermissionDeniedError');
                    else throw new Error('PermissionDeniedError');
                }
                
                // extension notified his presence
                if(data == 'rtcmulticonnection-extension-loaded') {
                    if(document.getElementById('install-button')) {
                        document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
                    }
                    DetectRTC.screen.chromeMediaSource = 'desktop';
                }
                
                // extension shared temp sourceId
                if(data.sourceId) {
                    DetectRTC.screen.sourceId = data.sourceId;
                    if(screenCallback) screenCallback( DetectRTC.screen.sourceId );
                }
            },
            getChromeExtensionStatus: function (callback) {
                if (!!navigator.mozGetUserMedia) return callback('not-chrome');
                
                var extensionid = 'ajhifddimkapgcifgcodmmfdlknahffk';

                var image = document.createElement('img');
                image.src = 'chrome-extension://' + extensionid + '/icon.png';
                image.onload = function () {
                    DetectRTC.screen.chromeMediaSource = 'screen';
                    window.postMessage('are-you-there', '*');
                    setTimeout(function () {
                        if (!DetectRTC.screen.notInstalled) {
                            callback('installed-enabled');
                        }
                    }, 2000);
                };
                image.onerror = function () {
                    DetectRTC.screen.notInstalled = true;
                    callback('not-installed');
                };
            }
        };
        
        // check if desktop-capture extension installed.
        if(window.postMessage && isChrome) {
            DetectRTC.screen.isChromeExtensionAvailable();
        }
    })();
    
    DetectRTC.screen.getChromeExtensionStatus(function(status) {
        if(status == 'installed-enabled') {
            if(document.getElementById('install-button')) {
                document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
            }
            DetectRTC.screen.chromeMediaSource = 'desktop';
        }
    });
    
    window.addEventListener('message', function (event) {
        if (event.origin != window.location.origin) {
            return;
        }
        
        DetectRTC.screen.onMessageCallback(event.data);
    });
    
    console.log('current chromeMediaSource', DetectRTC.screen.chromeMediaSource);
</script>
