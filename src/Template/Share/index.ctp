<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $customer = $data["customer"][0];

?>

<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
          <div class="container">
            <div class="jumbotron">
              <h1>Video Conference and Screen Sharing</h1>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <h1>Video Confrencing</h1>
                <a id="video_conf" target="_blank" href="">Click Here For Video Confrencing</a>
                <h1>Screen Sharing</h1>
                <a id="screen_share" target="_blank" href="">Click Here For Screen Sharing</a>
              </div>
            </div>
          </div>

        </div>
    </div>
    </div>
</div>

 </section>


<script type="text/javascript">
  $(document).ready(function (e){
    var urlHash = '#' + ((Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-') );
    var videoUrl = 'https://webrtcscreenvideo.herokuapp.com/video.php'+urlHash;
    var screenUrl = 'https://webrtcscreenvideo.herokuapp.com/screenshareing.php'+urlHash;
    $("#video_conf").attr({"href":videoUrl});
    $("#screen_share").attr({"href":screenUrl});
  });
</script>

                  <style type="text/css">
                  .ui-datepicker-calendar {
                      display: none;
                   }
                  </style>
                   

                <script type="text/javascript">
                $(function() {
                                    $('#a_exp_date').datepicker({
                                        changeMonth: true,
                                        minDate: 0,
                                        constrainInput: false,
                                        changeYear: true,
                                        showButtonPanel: true,
                                        dateFormat: 'MM yy'
                                    }).focus(function() 
                                    {
                                        var thisCalendar = $(this);
                                        $('.ui-datepicker-calendar').detach();
                                        $('.ui-datepicker-close').click(function()
                                         {
                                                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                                        thisCalendar.datepicker('setDate', new Date(year, month, 1));
                                        });
                                    });
                            });
                </script>
    <script type="text/javascript">
    $(document).ready(function()
    { 
      //$('.account_left small').css({'display':'block'});
      //$('.help-block',this).hide();
             $("#phone_no").mask("(999)999-9999");
             // $("#a_exp_date").mask("99/99");

                
                      $("#myTab a").click(function(e)
                      {
                        e.preventDefault();
                        $(this).tab('show');
                      });
                      $("#change_pass").click(function(event)
                      {
                     event.preventDefault();
                     $("#customerpass").toggle();
                      });
                      $("#add_new").click(function(event)
                      {
                      event.preventDefault();
                     //$("#customerPayment").toggle();
                      });

     
                        //customer Info Change
                        $(document).on("submit","#customerinfo",function (event){
                            event.preventDefault();
                            var data = $("#customerinfo").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editcustomer",
                                data:data,
                                success: function(result){
                                    $("#ci_msg").css({"display":"block"});
                                    setTimeout(function(){
                                              $("#ci_msg").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                        });

                     //Password Info Change
                        $(document).on("submit","#customerpass",function (event){
                            var match_pass = $('#matchpassword').val();
                            var conf = $('#confpassword').val();
                            
                            event.preventDefault();
                            var data = $("#customerpass").serialize();
                            if(conf==match_pass)
                            {
                                $.ajax({
                                    type:"POST",
                                    url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editpassword",
                                    data:data,
                                    success: function(result){
                                    if(result=='Current Password is wrong')
                                    {
                                         $('#error_pass').html('<div role="alert"  class="alert alert-danger" >Current Password is wrong</div>');
                                            setTimeout(function(){
                                              $('#error_pass').html('');
                                            }, 3000);
                                    }
                                    else
                                        {
                                            $("#ci_msg1").css({"display":"block"});
                                            setTimeout(function(){
                                              $("#ci_msg1").css({"display":"none"});
                                              $('#customerpass')[0].reset();
                                            }, 3000);
                                        }
                                    },
                                    error: function(event){}
                                });
                            }
                            else
                            {
                                    $("#error_pass").html('<div role="alert"  class="alert alert-danger" >Password and Confirm Password Mismatched</div>');
                                            setTimeout(function()
                                            {
                                              $('#error_pass').html('');
                                            }, 3000);
                            }
                        });
                         //Phone Info Change
                        $(document).on("submit","#customerphone",function (event)
                        {
                            event.preventDefault();
                            var data = $("#customerphone").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editcustomer",
                                data:data,
                                success: function(result){
                                    $("#ci_msg2").css({"display":"block"});
                                     setTimeout(function()
                                            {
                                             $("#ci_msg2").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                       });


    //Credit Card validation
    $('#customerPayment').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                a_card_no: {
                    validators: {
                        creditCard: {
                            message: 'The credit card number is not valid'
                        }
                    }
                },
                a_cvv: {
                    validators: {
                        cvv: {
                            creditCardField: 'a_card_no',
                            message: 'The CVV number is not valid'
                        }
                    }
                }

            }
        })
        .on('success.validator.fv', function(e, data) {
            if (data.field === 'a_card_no' && data.validator === 'creditCard') {
                var $icon = data.element.data('fv.icon');
                // data.result.type can be one of
                // AMERICAN_EXPRESS, DINERS_CLUB, DINERS_CLUB_US, DISCOVER, JCB, LASER,
                // MAESTRO, MASTERCARD, SOLO, UNIONPAY, VISA

                switch (data.result.type) {
                    case 'AMERICAN_EXPRESS':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-amex');
                        break;

                    case 'DISCOVER':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-discover');
                        break;

                    case 'MASTERCARD':
                    case 'DINERS_CLUB_US':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-mastercard');
                        break;

                    case 'VISA':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-visa');
                        break;

                    default:
                        $icon.removeClass().addClass('form-control-feedback fa fa-credit-card');
                        break;
                }
            }
        })
        .on('err.field.fv', function(e, data) {
            if (data.field === 'a_card_no') {
                var $icon = data.element.data('fv.icon');
                $icon.removeClass().addClass('form-control-feedback fa fa-times');
            }
        });

                    //Payment Info Change
                    $("#customerPayment").on("submit",function (event){
                        event.preventDefault();
                        var data = $("#customerPayment").serialize();
                        $.ajax({
                            type:"POST",
                            url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editpayment",
                            data:data,
                            success: function(result){
                                $("#ci_msg4").css({"display":"block"});
                                     setTimeout(function()
                                            {
                                             $("#ci_msg4").css({"display":"none"});
                                            }, 3000);
                            },
                            error: function(e){}
                        });

                    });


$('.help-block').remove();

   });
</script>  

<!-- new jquery add -->

   <script>
    $(function(){
        $('.parascroll').parascroll();
    })
</script>
<!-- write script to toggle class on scroll -->
<script>
$(window).scroll(function() {
    if ($(this).scrollTop() > 20){  
        $('.navbar-fixed-top').addClass("sticky");
    }
    else{
        $('.navbar-fixed-top').removeClass("sticky");
    }
});

//this is where we apply opacity to the arrow
$(window).scroll( function(){

  //get scroll position
  var topWindow = $(window).scrollTop();
  //multipl by 1.5 so the arrow will become transparent half-way up the page
  var topWindow = topWindow * 1.5;
  
  //get height of window
  var windowHeight = $(window).height();
      
  //set position as percentage of how far the user has scrolled 
  var position = topWindow / windowHeight;
  //invert the percentage
  position = 1 - position;

  //define arrow opacity as based on how far up the page the user has scrolled
  //no scrolling = 1, half-way up the page = 0
  $('.arrow-wrap').css('opacity', position);

});




//Code stolen from css-tricks for smooth scrolling:
$(function() {
  $('.circle_mat a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>