

<div class="modal fade popup" id="wizards" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">

	<form id="msform">
		<!--fieldset>
			<div class="modal-header">
			<ol class="breadcrumb">
				<li><a href="#">Setup & Install </a></li>
				<li><a href="#">Smart Home</a></li>
				<li>Smart Home Device</li>
				<li><a href="#">Comments</a></li>
				<li  class="active"><a href="#">Summary</a></li>
				
			</ol>
		 	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		 	</div>
		 
		 
			<h3 class="fs-title">Select from the options below to get started.</h3>
			<h2 class="fs-subtitle">What Can We Help You With?</h2>
			<div class="row inner-grid">
				<div class="col-md-3">
					<div class="speaker-inner">
						<div class="speaker-icons setup-install"><img src="<?php echo $base_url_temp; ?>img/template_images/set-up-img.png" alt="set-up &amp; install"></div>
						<div class="speaker-icons setup-install-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/set-up-hover.png" alt="set-up &amp; install"></div>
						<div class="speaker_heading"><h3>Setup &amp; Install</h3></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="speaker-inner">
						<div class="speaker-icons troubleshooting"><img src="<?php echo $base_url_temp; ?>img/template_images/troubleshooting.png" alt="set-up &amp; install"></div>
						<div class="speaker-icons troubleshooting-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/troubleshooting-hover.png" alt="set-up &amp; install"></div>
						<div class="speaker_heading"><h3>Troubleshooting</h3></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="speaker-inner">
						<div class="speaker-icons product-advice"><img src="<?php echo $base_url_temp; ?>img/template_images/product-advice.png" alt="set-up &amp; install"></div>
						<div class="speaker-icons product-advice-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/product-advice-hover.png" alt="set-up &amp; install"></div>
						<div class="speaker_heading"><h3>Product Advice</h3></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="speaker-inner">
						<div class="speaker-icons training"><img src="<?php echo $base_url_temp; ?>img/template_images/training.png" alt="set-up &amp; install"></div>
						<div class="speaker-icons training-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/training-hover.png" alt="set-up &amp; install"></div>
						<div class="speaker_heading"><h3>Training</h3></div>
					</div>
				</div>
			</div>
			<input type="button" name="next" class="next action-button" value="Next" />
		</fieldset-->	
	</form>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$.ajax({
			url:"<?php echo $base_url_temp; ?>index/wizardStepOne",
			//data:$("#form_applicant").serialize(),
			type:"POST",
			success:function(result){
				$("#msform").html(result);
				//alert("all good"+result);
			},
			error:function(error){
				console.log(error);
			}
		});

	});
</script>