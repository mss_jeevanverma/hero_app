<div class="modal-dialog modal-lg">

    <div class="modal-content">
    <div class="modal-header">
        <button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>

    </div>

    <div class="modal-body text-center">

        <div class="row text-center">
            <div class="col-sm-12">
            
                    <h2 class="subhead-lg green b-margin-med">
                        Thanks! We'll be in touch.
                    </h2>
                    <p class="copy-std b-margin-std">
                        We plan to service your area very soon. We'll email you
                        an update when we are.
                    </p>
            <button value="Sign in" class="check-coverage action-button" type="button" data-dismiss="modal">Continue &gt;&gt;</button>
            </div>

        </div>
    </div>

    </div>
</div>