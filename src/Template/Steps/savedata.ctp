<div class="modal-dialog modal-lg">

    <div class="modal-content">
    <div class="modal-header">
        <button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>

    </div>

    <div class="modal-body text-center">
        <h2 class="fs-subtitle">Payment Method</h2>

        <div class="row text-center">
            <div class="col-sm-12">
                <div class="installed-proceduer">
                    <h3><img src="<?php echo $base_url_temp; ?>img/template_images/thanks.png"></h3>
                    <h2 class="fs-subtitle forgot-heading">Thank YOU!</h2>
                </div>
                <div class="row inner-grid thanks-row ">
                    <div class="installed-proceduer thanks">                        
                    <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br>
                    "There is no one who loves pain itself, who seeks after it and wants to have it</p>     
                    </div>
                </div>
            <button value="Sign in" class="check-coverage action-button" type="button" data-dismiss="modal">Continue &gt;&gt;</button>
            </div>

        </div>
    </div>

    </div>
</div>