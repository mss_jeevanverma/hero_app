<?php 
$val = $cat_one;

?>
<div class="modal-dialog modal-lg">

	<div class="modal-content">
	<div class="modal-header">
		<button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
		<!--ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Library</a></li>
			<li class="active">Data</li>
		</ol-->
	</div>

	<div class="modal-body text-center">
	<!--h3 class="fs-title">Select from the options below to get started.</h3-->
	<h3 class="fs-title" id="fs-title_alert" style="display:none;color:red;">Select from the options below to get started</h3>
	
	<h2 class="fs-subtitle">How can we restore your technical zen?</h2>

		<form class="first-form">
		<ul class="p-form">
			<li>
				<div class="form-group">
					<label for="SetupInstall">
						<i class="fa fa-cogs"></i>
						<input type="radio" value="Setup & Install" name="quality" id="SetupInstall">
						<div class="speaker_heading" name="<?php print_r($val[0]['category_name']); ?>" cat-id= "<?php print_r($val[0]['id']); ?>"><h3><?php print_r($val[0]['category_name']); ?></h3></div>
					</label>
				</div>
			</li>
			<li>
				<div class="form-group">
					<label for="Troubleshooting">
						<i class="fa fa-laptop"></i>
						<input type="radio" value="Troubleshooting" name="quality" id="Troubleshooting">
						<div class="speaker_heading" name="<?php print_r($val[1]['category_name']); ?>" cat-id= "<?php print_r($val[1]['id']); ?>" ><h3><?php print_r($val[1]['category_name']); ?></h3></div>
					</label>
				</div>
			</li>
			<li>
				<div class="form-group">
					<label for="Optimize">
						<i class="fa fa-line-chart"></i>
						<input type="radio" value="Optimize" name="quality" id="Optimize">
						<div class="speaker_heading" name="<?php print_r($val[2]['category_name']); ?>" cat-id= "<?php print_r($val[2]['id']); ?>" ><h3><?php print_r($val[2]['category_name']); ?></h3></div>
					</label>
				</div>
			</li>
		</ul>
		</form>
	</div>


	<div class="modal-footer next-popup">
	<input type="hidden" id="mainVal" />
	<!-- <input type="button" name="next" class="next action-button" value="Next" /> -->
	</div>

	</div>
</div>
<?php /*
<fieldset>
	<div class="modal-header">
	<!--ol class="breadcrumb">
		<li><a href="#">Setup & Install </a></li>
		<li><a href="#">Smart Home</a></li>
		<li>Smart Home Device</li>
		<li><a href="#">Comments</a></li>
		<li  class="active"><a href="#">Summary</a></li>
		
	</ol-->
 	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 		<span aria-hidden="true">&times;</span>
 	</button>
 	</div>
 
 
	<h3 class="fs-title">Select from the options below to get started.</h3>
	<h2 class="fs-subtitle">What Can We Help You With?</h2>
	<div class="row inner-grid">
		<div class="col-md-3">
			<div class="speaker-inner">
				<div class="speaker-icons setup-install"><img src="<?php echo $base_url_temp; ?>img/template_images/set-up-img.png" alt="set-up &amp; install"></div>
				<div class="speaker-icons setup-install-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/set-up-hover.png" alt="set-up &amp; install"></div>
				<div class="speaker_heading" name="<?php print_r($val[0]['category_name']); ?>" cat-id= "<?php print_r($val[0]['id']); ?>"><h3><?php print_r($val[0]['category_name']); ?></h3></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="speaker-inner">
				<div class="speaker-icons troubleshooting"><img src="<?php echo $base_url_temp; ?>img/template_images/troubleshooting.png" alt="set-up &amp; install"></div>
				<div class="speaker-icons troubleshooting-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/troubleshooting-hover.png" alt="set-up &amp; install"></div>
				<div class="speaker_heading" name="<?php print_r($val[1]['category_name']); ?>" cat-id= "<?php print_r($val[1]['id']); ?>" ><h3><?php print_r($val[1]['category_name']); ?></h3></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="speaker-inner">
				<div class="speaker-icons product-advice"><img src="<?php echo $base_url_temp; ?>img/template_images/product-advice.png" alt="set-up &amp; install"></div>
				<div class="speaker-icons product-advice-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/product-advice-hover.png" alt="set-up &amp; install"></div>
				<div class="speaker_heading" name="<?php print_r($val[2]['category_name']); ?>" cat-id= "<?php print_r($val[2]['id']); ?>" ><h3><?php print_r($val[2]['category_name']); ?></h3></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="speaker-inner">
				<div class="speaker-icons training"><img src="<?php echo $base_url_temp; ?>img/template_images/training.png" alt="set-up &amp; install"></div>
				<div class="speaker-icons training-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/training-hover.png" alt="set-up &amp; install"></div>
				<div class="speaker_heading" name="<?php print_r($val[3]['category_name']); ?>" cat-id= "<?php print_r($val[3]['id']); ?>" ><h3><?php print_r($val[3]['category_name']); ?></h3></div>
			</div>
		</div>
	</div>
	<input type="hidden" id="mainVal" />
	<input type="button" name="next" class="next action-button" value="Next" />
</fieldset>	
*/ ?>
<script type="text/javascript">
/*	$(document).ready(function(){

		var catName = '';
		var catId = '';	
		$('.speaker-inner').click(function(){

			catName = $(this).find('.speaker_heading').attr("name") ;
			catId = $(this).find('.speaker_heading').attr("cat-id") ;
			var mainVal = '{"catName":"'+catName+'","catId":"'+catId+'"}';
			$('#mainVal').val(mainVal);

		});

		$('.next').click(function(){

			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/two",
				data:{'vals':$('#mainVal').val()},
				type:"POST",
				success:function(result){
					$("#msform").html(result);
				},
				error:function(error){
					console.log(error);
				}
			});

		});
		
	});*/

$(document).ready(function(){

		var catName = '';
		var catId = '';	
		$('.form-group').click(function(){

			catName = $(this).find('.speaker_heading').attr("name") ;
			catId = $(this).find('.speaker_heading').attr("cat-id") ;
			var mainVal = '{"catName":"'+catName+'","catId":"'+catId+'"}';
			$('#mainVal').val(mainVal);

		});

		$('.next').click(function(){
				
			if($('#mainVal').val().length<3){
				//alert('Select from the options below to get started.');
				$("#fs-title_alert").css({"display":"block"});
                      setTimeout(function(){
                           $("#fs-title_alert").css({"display":"none"});
                            }, 3000);
				return false;
			}
				

			// $.ajax({
			// 	url:"<?php echo $base_url_temp; ?>steps/two",
			// 	data:{'vals':$('#mainVal').val()},
			// 	type:"POST",
			// 	success:function(result){
			// 		$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
			// 	},
			// 	error:function(error){
			// 		console.log(error);
			// 	}
			// });

		});



	$('ul.p-form li').click(function() {

	    $('ul.p-form li.current').removeClass('current');
	    $(this).closest('li').addClass('current');

	    	$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/two",
				data:{'vals':$('#mainVal').val()},
				type:"POST",
				async:false,
				success:function(result){
					$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
				},
				error:function(error){
					console.log(error);
				}
			});


	});
		
	});
</script>