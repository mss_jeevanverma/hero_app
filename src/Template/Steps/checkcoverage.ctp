<?php 
/*
* Code when The Zip Entered is in Coverage Area and user is logged in OR user has loged Within the Steps
*/ 
$session = $this->request->session();
$user_info_data = $session->read('UserInfo');

if((@$coverage_data['zip']=='yes' && $coverage_data['user_login']=='yes') || count($user_info_data)){ 

?>

<style>
	.ui-datepicker-calendar {
    display: none;
 }
</style>
<div class="modal-dialog modal-lg">

	<div class="modal-content">
		<div class="modal-header">
			<button aria-label="Close" class="close" type="button"><span aria-hidden="true">×</span></button>
		</div>

		<div class="modal-body text-center">
			<h2 class="fs-subtitle">Payment Method</h2>
			<div class="row text-center">
				<div class="col-sm-6 col-sm-offset-3">
				<?php if(strlen($_SESSION['UserCardInfo']['a_card_no'])>5){ ?>
				<div class="form-group">	
					<div class="btton_radio"><input type="radio" id="new_card" name="card_data" value="new" checked><h3>Pay With New Card</h3> </div>

					<div class="btton_radio"><input type="radio" id="old_card" name="card_data" value="old"><h3>Pay With Old Card</h3></div>
				</div>
				<?php } ?>
					<!-- First form with new info -->
					<form role="form" method="post" action="" id="customerPayment" class="">
					     <div class="help-block with-errors"></div>

						    
					        <div class="form-group">	
							  <div  class="input-append date">
							  <span class="add-on">
							    <input id="datetimepicker"  readonly='true' data-format="dd-MM-yyyy hh:mm:ss" type="text" name="appointment_date" placeholder="Appointment Date & Time" required></input>

							    </span>
							  </div>
							  </div>

                            <div class="form-group">

                                <input type="text" name="a_card_no" id="display_name" class="form-control input-lg" placeholder="Credit Card No."  maxlength="16"
                                data-minlength="16" data-error="Please add valid Card Number"  value="" required>
                           
                            </div>

                            <div class="form-group">
                                <input type="text" name="a_exp_date"  class="monthYearPicker form-control input-lg" readonly='true' placeholder="Expiry Date Ex: MM / YY "    required>
                               
                            </div>

                            <div class="form-group">
                                <input type="password" name="a_cvv" id="display_name" class="form-control input-lg" placeholder="CVV"  maxlength="4" data-minlength="3"  data-error="Please add valid CVV"  value="" required>
                             
                            </div>

                            <div class="form-group">
                                <input type="text" name="a_postal_code"  class="form-control input-lg" placeholder="Postal Code" pattern="^[0-9]{1,}$" data-minlength="5" maxlength="7" data-match-error="Please add valid Postal Code" tabindex="3" value="" required>
                                <div class="help-block with-errors"></div>
                            </div>

				<input type="hidden" name="form_type" value="new" />

                            <div class="row">
                                <div class="col-xs-12 col-md-6"><input type="submit" value="Update" class="btn btn-primary btn-block btn-lg" ></div>
                            </div>
                        </form>

                        <!-- Second form with Old info -->
                        <form role="form" method="post" action="" id="customerPayment_old" style="display:none;" >
					     <div class="help-block with-errors"></div>

						    
					        <div class="form-group">	
							  <div  class="input-append date">
							  <span class="add-on">
							    <input id="datetimepicker_old"  readonly='true' data-format="dd-MM-yyyy hh:mm:ss" type="text" name="appointment_date" placeholder="Appointment Date & Time" required></input>

							    </span>
							  </div>
							  </div>

                            <div class="form-group">

                            	<label> Credit Card No. : </label>
                            	<?php echo "************".substr($_SESSION['UserCardInfo']['a_card_no'], -4); ?>

                            </div>

                            <div class="form-group">

                            	<label> Expiry Date : </label>
                            	<?php echo $_SESSION['UserCardInfo']['a_exp_date']; ?>

                            </div>

                            <div class="form-group">

                            	<label> CVV  : </label>
                            	<?php 
                            		$cvvLength = strlen($_SESSION['UserCardInfo']['a_cvv']); 
                            		for($i=1;$i<=$cvvLength; $i++){
                            			echo "*";
                            		}
                            	?>

                            </div>

                            <div class="form-group">

                            	<label> Postal Code  : </label>
                            	<?php echo $_SESSION['UserCardInfo']['a_postal_code']; ?>

                            </div>

			<input type="hidden" name="form_type" value="old" />

                            <div class="row">
                                <div class="col-xs-12 col-md-6"><input type="submit" value="Update" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                            </div>
                        </form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
/*
* Code when The Zip Entered is in Coverage Area and user is Not logged in
*/
}else if(@$coverage_data['zip']=='yes' && $coverage_data['user_login']=='no'){ 
  ?>

<div class="modal-dialog modal-lg">

<div class="modal-content">
  <div class="modal-header">
        <button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
      </div>
	  
	  <div class="modal-body text-center">
	  <h2 class="fs-subtitle fs-subtitle1">Sign In</h2>
      
      <div class="row">
      	<div class="col-sm-12">
          <form class="first-form" method="post"  id="forgot_pass" action="" style="display:none;">
            	 <div id="for_succ"></div>
            	  <div id="for_error"></div>
				 		
				<div class="form-group">
					 <input type="email" name="email" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a Valid Email Address')"  pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" placeholder="Email address" required autofocus>
				</div>
				
				<div class="row f-btn">
					
					<div class="col-sm-6 text-right mm">
					
				<button class="check-coverage action-button sign-btn" type="submit">Forgot</button>
					</div>
				

            </form>
        </div>
      </div>
	  <div class="row text-center">
      <div class="col-sm-12">
	  <div class="col-sm-6">

        <form class="first-form" id="login_form">
        <div id="frm_msg" ></div>
			<div class="form-group">
				<input type="email" value="" id="user_email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="username" class="username" placeholder="email" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" required>
			</div>
			
			<div class="form-group">
				<input type="password" value="" pattern=".{6,}"  maxlength="20"  id="user_password" name="password" class="password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')" placeholder="password" required>
			</div>
			
			<div class="row f-btn">
				<div class="col-sm-6 text-left">
					<button class="action-button sign-btn" value="Sign in" type="submit" >Sign In</button>
				</div>
				<div class="col-sm-6 text-right">
				
				<div class="action-button"  id="register_button">Register</div>
				</div>
				<div  id="forgot_pas">Forgot Your Password?</div>
			</div>
        </form>
         <form class="first-form" method="post"  id="register_popup" style="display:none" action="">
            	 <div id="reg_succ"></div>
            	  <div id="reg_error"></div>
				 <div class="form-group">
					<input type="text" name="first_name" maxlength="100" minlength="1" pattern="[a-zA-Z]{1,100}" class="form-control" placeholder="First Name"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"  id="first_name')" required autofocus>
				</div>
				
				<div class="form-group">
					  <input type="text" name="last_name" minlength="1" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"  id="last_name" pattern="[a-zA-Z]{1,100}" class="form-control" placeholder="Last Name" required autofocus>
				</div>
								
				<div class="form-group">
					 <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" placeholder="Email address" required autofocus>
				</div>

				<div class="form-group">
					<input type="password" pattern=".{6,}"  maxlength="20" name="password" id="password" class="form-control" placeholder="Password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')"  required autofocus>
				</div>

				<div class="form-group">
				 <input type="password" id="confirmPassoword" maxlength="20" pattern=".{6,}"   data-match="#password" data-match-error="Whoops, these don't match" class="form-control" placeholder="Confirm New Password"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Confirm Password should be minimum of 6 letters')" required autofocus>
				 </div>
				<div class="row f-btn">
					
					<div class="col-sm-6 text-right">
					
				<button class="action-button sign-btn" type="submit">Register</button>
					</div>
					<div class="col-sm-6 text-center">
				
				<a  class="check-coverage action-button sign-btn" href="#" id="login_btn_popup">Login</a>
				</div>
				</div>

            </form>
           
         
            </div>
            <div class="col-sm-6">
            <div class="col-sm-1 hide_right">
		<img src="<?php echo $base_url_temp; ?>img/template_images/or.png">
		
		</div>
        
        
        	<div class="col-sm hide_right">
		<h3>Use an existing account</h3>
		<ul class="social-login">
			  <li><a href="<?php echo $base_url_temp; ?>login_api"><img src="<?php echo $base_url_temp; ?>img/template_images/fb.jpg"></a></li>
			  <li><a href="<?php echo $base_url_temp; ?>login_api/gmail"><img src="<?php echo $base_url_temp; ?>img/template_images/google.jpg"></a></li>
			  <li><a href="<?php echo $base_url_temp; ?>login_api/twitterlogin"><img src="<?php echo $base_url_temp; ?>img/template_images/twitter.jpg"></a></li> 
			</ul>
		</div>
        
        
            </div>
            </div>
		</div>
		
		
		
	
		
      </div>
	  </div>

</div>
</div>



<?php
/*
* Code when The Zip Entered is NOT in Coverage Area
*/ 
 }else if(@$coverage_data['zip']=='no'){  
?>

<div class="modal-dialog modal-lg">

	<div class="modal-content">
	<div class="modal-header">
		<button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
		<!--ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Library</a></li>
			<li class="active">Data</li>
		</ol-->
	</div>

	<div class="modal-body text-center">
		<h3 class="fs-title">We're not in your area just yet.</h3>
		<h4 >We plan to service your area very soon. Enter your email address and we’ll let you know as soon as we are. </h4>
		<div class="row text-center">
		<div class="col-sm-6 col-sm-offset-3">
			<form class="first-form" id="non_area">
				<div class="form-group">
				<input type = "hidden" value = "<?php echo $coverage_data['zip_code']; ?>" id="zip_code" />
				<input type="email" value="" name="email" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a Valid Email Address')" placeholder='Email Address'  pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="zip_email" class="zip-input" required>
				<button class="check-email action-button" type="submit">Add Email</button>
				</div>
			</form>
		</div>
		</div>
	</div>
	<div class="modal-footer next-popup">
		<!--p>Already have an account? <a href="#" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg6">sign in.</a></p-->
	</div>

	</div>
</div>

<?php 
/*
* Code First Time Load
*/
}else{  

?>

<div class="modal-dialog modal-lg">

	<div class="modal-content">
	<div class="modal-header">
		<button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
		<!--ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Library</a></li>
			<li class="active">Data</li>
		</ol-->
	</div>

	<div class="modal-body text-center">
		<h3 class="fs-title">Great! Thanks very much for the information.</h3>
		<h2 class="fs-subtitle">Let's make sure you are within our service area. </h2>
		<div class="row text-center">
		<div class="col-sm-6 col-sm-offset-3">
				<form class="first-form" id='cov_form' data-toggle="validator">
				<div class="form-group">
				<input type="text" value="" name="zip_area"  maxlength="5" id="zip" class="zip-input" placeholder='Enter Zip Code' data-error = "5 Digit Zip code Only" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('5 Digit Zip code Only')" pattern="[0-9]{5,}$"  required>
					</div>
				<button class="check-coverage action-button" type="submit">Check Coverage</button>
			</form>
		</div>
		</div>
	</div>


	<div class="modal-footer next-popup">
		<!--p>Already have an account? <a href="#" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg6">sign in.</a></p-->
	</div>

	</div>
</div>
 
<?php } ?>
<style type="text/css">
.ui-datepicker-calendar {
    display: none;
 }
 
</style>
 
		 <script type="text/javascript">
					$(document).ready(function()
					{
							$('#datetimepicker').datetimepicker({
									dayOfWeekStart : 1,
									lang:'en',
									formatDate:'d.m.Y',
									constrainInput: false,
									formatTime:'H:i',
									step:5,
									minDate:0,
									startDate:	'2015/10/16'
							});

							$('#datetimepicker_old').datetimepicker({
									dayOfWeekStart : 1,
									lang:'en',
									formatDate:'d.m.Y',
									constrainInput: false,
									formatTime:'H:i',
									step:5,
									minDate:0,
									startDate:	'2015/10/16'
							});
					});
		</script>
                <script type="text/javascript">
                $(document).ready(function()
					{
							$('#a_exp_date1').datetimepicker({
									viewMode: 'y',
                					format: 'M/Y',
                					minDate:0,
                					monthOnly:true,
                					timepicker:false,
							});
					});
                </script>
                <script>
$(document).ready(function(){
  $("#register_button").click(function(){
    $("#register_popup").show();
    $("#login_form").hide();
    $(".hide_right").show();
    $(".fs-subtitle1").text("Register");
  });
   $("#login_btn_popup").click(function(){
    $("#register_popup").hide();
    $("#login_form").show();
    $(".hide_right").show();
    $(".fs-subtitle1").text("Sign In");
  });
   $("#forgot_pas").click(function(){
    $("#register_popup").hide();
    $("#login_form").hide();
    $("#forgot_pass").show();
    $(".hide_right").hide();
    $(".fs-subtitle1").text("Forgot Password");
  });
   
 

$("#register_popup").on("submit",function (event){
	event.preventDefault();
		var pass = $('#password').val();
		var conf = $('#confirmPassoword').val();
		
		if(pass==conf)
		{
			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/register",
				data:$("#register_popup").serialize(),
				type:"POST",
				success:function(result){
				if(result=='User Created Successfully'){
					$("#reg_succ").html('<div role="alert"  class="alert alert-danger" >User Created Successfully</div>');
					setTimeout(function(){
					  $('#reg_succ').html('');
					  $("#register_popup")[0].reset();
					}, 3000);
					$("#register_popup").hide();
					$("#login_form").show();

				}
				else if(result=='Email already Exists'){
					$('#reg_error').html('<div role="alert"  class="alert alert-danger" >Email already Exists</div>');
					setTimeout(function(){
					  $('#reg_error').html('');
					}, 3000);
				}
				},
				error:function(error){
					console.log(error);
				}
			});
		}
				else
				{
				$("#reg_succ").html('<div role="alert"  class="alert alert-danger" >Password Does not match</div>');
					setTimeout(function(){
					  $('#reg_succ').html('');
					}, 3000);
				}
			
		});

});
</script>

<script type="text/javascript">
$(document).ready(function(){

	// $('#datetimepicker').datetimepicker({
	// 	format: 'dd-MM-yyyy hh:mm:ss'
	// });

	// Diable Enter Key on Coverage area form
	$('#zip').on('keyup keypress', function(e) {
	  var code = e.keyCode || e.which;
	  if (code == 13) { 
	    e.preventDefault();
	    return false;
	  }
	});

		// Covrage are zip code check 
	$('#cov_form').on("submit",function(event){
		event.preventDefault();

		$('#cov_form').validator();
		var zip_code = $('#zip').val();

		// if(zip_code.length<5){
		// 	return false;
		// }

		$.ajax({
			url:"<?php echo $base_url_temp; ?>steps/checkcoverage",
			data:{'vals':zip_code},
			type:"POST",
			success:function(result){
				$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
			},
			error:function(error){
				console.log(error);
			}
		});
	});

	// Covrage are zip code check 
//	$('.check-email').click(function(){
	$("#non_area").on("submit",function (event){
        event.preventDefault();


		var zip_code = $('#zip_code').val();
		var zip_email = $('#zip_email').val();
		var data = {"zip_code":zip_code,'zip_email':zip_email};
		$.ajax({
			url:"<?php echo $base_url_temp; ?>steps/zipemail",
			data:{'vals':data},
			type:"POST",
			success:function(result){
				$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
			},
			error:function(error){
				console.log(error);
			}
		});
	});


	// Sign In if not 
	//$('.sign-btn').click(function(){
	$("#login_form").on("submit",function (event){
        event.preventDefault();
		var user_email = $('#user_email').val();
		var user_password = $('#user_password').val();
		var data = {"user_email":user_email,'user_password':user_password};

		$.ajax({
			url:"<?php echo $base_url_temp; ?>steps/login",
			data:{'vals':data},
			type:"POST",
			success:function(result){
				if(result.length>100){
					$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
				}
				else if(result=='Your Account is Not Activated.'){
					$('#frm_msg').html('<div role="alert"  class="alert alert-danger" >Your Account is deactivated, Please contact Admin</div>');
					  $("#login_form").find("#user_password").val('');
					setTimeout(function(){
					  $('#frm_msg').html('');
					}, 2000);
				}
				else{
					$('#frm_msg').html('<div role="alert"  class="alert alert-danger" >'+result+'</div>');
					$("#login_form").find("#user_password").val('');
					setTimeout(function(){
					  $('#frm_msg').html('');
					}, 2000);
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});	


    //$("#a_exp_date").mask("99/99");
    $('#customerPayment').validator();
    //Credit Card validation
    $('#customerPayment').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                a_card_no: {
                    validators: {
                        creditCard: {
                           message: '<span style="color:#f00;">The credit card number is not valid</span>'
                        }
                    }
                },
                a_cvv: {
                    validators: {
                        cvv: {
                            creditCardField: 'a_card_no',
                            message: '<span style="color:#f00;">The CVV number is not valid</span>'
                        }
                    }
                }

            }
        })
        .on('success.validator.fv', function(e, data) {
            if (data.field === 'a_card_no' && data.validator === 'creditCard') {
                var $icon = data.element.data('fv.icon');
                // data.result.type can be one of
                // AMERICAN_EXPRESS, DINERS_CLUB, DINERS_CLUB_US, DISCOVER, JCB, LASER,
                // MAESTRO, MASTERCARD, SOLO, UNIONPAY, VISA

                switch (data.result.type) {
                    case 'AMERICAN_EXPRESS':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-amex');
                        break;

                    case 'DISCOVER':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-discover');
                        break;

                    case 'MASTERCARD':
                    case 'DINERS_CLUB_US':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-mastercard');
                        break;

                    case 'VISA':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-visa');
                        break;

                    default:
                        $icon.removeClass().addClass('form-control-feedback fa fa-credit-card');
                        break;
                }
            }
        })
        .on('err.field.fv', function(e, data) {
            if (data.field === 'a_card_no') {
                var $icon = data.element.data('fv.icon');
                $icon.removeClass().addClass('form-control-feedback fa fa-times');
            }
        });

    //Payment Info Change
    $("#customerPayment").on("submit",function (event){
        event.preventDefault();
        var data = $("#customerPayment").serialize();

        $.ajax({
            type:"POST",
            async: false,
            url:"<?php echo $base_url_temp; ?>"+"steps/savedata",
            data:data,
            success: function(result){
            	$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
		return false;
            },
            error: function(e){}
        });

    });

   //Payment Info Change for old data
    $("#customerPayment_old").validator().on("submit",function (event){
        if (event.isDefaultPrevented()) {
    		return false;
  		}
        var data = $("#customerPayment_old").serialize();

        $.ajax({
            type:"POST",
            async: false,
            url:"<?php echo $base_url_temp; ?>"+"steps/savedata",
            data:data,
            success: function(result){
            	$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
            },
            error: function(e){}
        });

    });

    
    	//Send email at time of forgot password	
    	$("#forgot_pass").on("submit",function (event){	
			event.preventDefault();
			$.ajax({
				url:"<?php echo $base_url_temp; ?>login/forgot_send_mail",
				data:$("#forgot_pass").serialize(),
				type:"POST",
				success:function(result){
				if(result=='Email Successfully Send'){
					$("#for_succ").html('<div role="alert"  class="alert alert-success" >Reset Password Link Send to Your Email Address</div>');
					$("#forgot_pass")[0].reset();
					setTimeout(function(){
					  $('#for_succ').html('');
					}, 2000);
				}
				else if(result=='Email Does Not Exists'){
					$('#for_error').html('<div role="alert"  class="alert alert-danger" >Email Does Not Exists</div>');
					$("#forgot_pass")[0].reset();
					setTimeout(function(){
					  $('#for_error').html('');
					}, 2000);
				}
				},
				error:function(error){
					console.log(error);
				}
			});
			
		});






});

</script>

<script>
// month year picker
$(document).ready(function (){
	// $('.monthYearPicker').datepicker({
	// 		changeMonth: true,
	// 		changeYear: true,
	// 		constrainInput: false,
	// 		minDate:0,
	// 		showButtonPanel: true,
	// 		dateFormat: 'mm/yy'
	// 	}).focus(function() {
	// 		var thisCalendar = $(this);
	// 		$('.ui-datepicker-calendar').detach();
	// 		$('.ui-datepicker-close').click(function() {
	// 			$(this).valid();
	// var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	// var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	// thisCalendar.datepicker('setDate', new Date(year, month, 1));
	// 		});
	// 	});


$('.monthYearPicker').datepicker( {
        changeMonth: true,
        changeYear: true,
         minDate:0,
        showButtonPanel: true,
	     dateFormat: 'mm/yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    });


});
</script>

              
<script type="text/javascript">
	$(document).ready(function(){

		$("#new_card").click(function(e){
			$("#customerPayment_old").hide();
			$("#customerPayment").show();
		});

		$("#old_card").click(function(e){
			$("#customerPayment").hide();
			$("#customerPayment_old").show();
		});

	});
</script>
