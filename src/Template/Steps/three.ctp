<div class="modal-dialog modal-lg">

	<div class="modal-content">
		<div class="modal-header">
			<button aria-label="Close" class="close" type="button"><span aria-hidden="true">×</span></button>
			<!--ol class="breadcrumb">
				<li><a href="#">Home</a></li>
				<li><a href="#">Library</a></li>
				<li class="active">Data</li>
			</ol-->
		</div>

		<div class="modal-body text-center">
			<h3 class="fs-title">Great! Thanks very much for the information.</h3>
			<h2 class="fs-subtitle">Is there anything else we should know?</h2>
			<div class="row text-center">
				<div class="col-sm-6 col-sm-offset-3">
					<form class="first-form">
						<div class="form-group">
							<textarea class="comment-textarea"></textarea>
						</div>
					</form>
				</div>
			</div>
		</div>


		<div class="modal-footer next-popup">
		<input type="button" name="next" class="next action-button" value="Next" />
		</div>

	</div>
</div>

<?php /*
<fieldset>
	<div class="modal-header">
		<!--ol class="breadcrumb">
			<li><a href="#">Setup & Install </a></li>
			<li><a href="#">Smart Home</a></li>
			<li>Smart Home Device</li>
			<li><a href="#">Comments</a></li>
			<li  class="active"><a href="#">Summary</a></li>
			
		</ol-->
	 	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 	</div>
	<h3 class="fs-title">Great! Thanks very much for the information.</h3>
	<h2 class="fs-subtitle">Is there anything else we should<br> know?</h2>
	<div class="row inner-grid">
		<div class="installed-proceduer">
			<textarea class="comment-textarea"></textarea>

		</div>
	</div>
	<input type="button" name="next" class="next action-button" value="Next" />
</fieldset>
*/ ?>

<script type="text/javascript">
	$(document).ready(function(){

		var leftDetials = '';

			$('.next').click(function(){
				leftDetials = $('.comment-textarea').val();

				$.ajax({
					url:"<?php echo $base_url_temp; ?>steps/four",
					data:{'vals':leftDetials},
					type:"POST",
					success:function(result){
						$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
					},
					error:function(error){
						console.log(error);
					}
				});
			});
	});
</script>