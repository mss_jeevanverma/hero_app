<?php 

?>

<div class="modal-dialog modal-lg">
   <div class="modal-content">
      <div class="modal-header">
         <button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
         <!--ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
         </ol-->
      </div>
      <div class="modal-body text-center">
         <h3 class="fs-title">Does everything look right to you?</h3>
         <h2 class="fs-subtitle">Here's your appointment summary</h2>
         <div class="row inner-grid">
            <div class="appoint-ment-grunt">
               <div class="service-hours">
                  <h4>SERVICES ($79 PER HOUR) </h4>
                  <table class="table table-bordered">
                     <caption>Network Product Advice</caption>
                     <tbody id="tb_body" >

                    <?php  foreach($services as $key => $service) {  ?>
                        <tr>
                           <td class="text">
                              <p align="left"> 	<?php echo $service['stepOne'].' - ',$service['stepTwo']; ?> </p>
                              <span align="left"><?php echo $service['stepThree'] ?> </span>
                           </td>
                           <!--td>
                              <p>Approx.</p>
                              <h3>120 min</h3>
                           </td-->
                           <td><a  href="#" class="del_service" serv-id="<?php echo $key; ?>" ><i class="fa fa-trash-o"></i></a></td>
                        </tr>
					<?php } ?>	

                     </tbody>
                  </table>
               </div>
               <div class="row on-discount">
                 <!-- <h3>Discount Codes</h3> -->
                
                  <div class="col-sm-12">
                    <input type="button" name="Another Service" id="add_more" class="action-button Another_Service" value="+ Another Service" />
                       <button type="button" name="next" class="next action-button " value="Next">Next</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer next-popup">
      
      </div>
   </div>
</div>

<?php /*
<fieldset>
<div class="modal-header">
	<ol class="breadcrumb">
		<li><a href="#">Setup & Install </a></li>
		<li><a href="#">Smart Home</a></li>
		<li>Smart Home Device</li>
		<li><a href="#">Comments</a></li>
		<li  class="active"><a href="#">Summary</a></li>
		
	</ol>
 	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 </div>
	<h3 class="fs-title">Does everything look right to you?</h3>
	<h2 class="fs-subtitle">Here's your appointment<br> summary.</h2>
	<div class="row inner-grid">
		<div class="appoint-ment-grunt">
			<div class="service-hours">
				<h4>SERVICES ($79 PER HOUR) </h4>
				<table class="table table-bordered">
				  <caption>Network Product Advice</caption>
				  <tbody id="tb_body">
				  <?php  foreach($services as $key => $service) {  ?>
						<tr>
						  <td>
							  <p align="left"> 
							  	<?php echo $service['stepOne'].' - ',$service['stepTwo']; ?> 
							  </p>
							  <span align="left">
							  	<?php echo $service['stepThree'] ?> 
							  </span>
						  </td>
						  <td><p>Approx.</p><h3>120 min</h3></td>
						  <td><a href="#" class="del_service" serv-id="<?php echo $key; ?>"><i class="fa fa-trash-o"></i></a></td>
						</tr>
					<?php } ?>		
					<!--tr>
					  <td><p align="left"> Network Product Advice - Router </p><span align="left">PC & Mac Support WiFi Troubleshooting TV Mounting</span></td>
					  <td><p>Approx.</p><h3>120 min</h3></td>
					  <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
					</tr-->
				  </tbody>
				</table>

			</div>
			
			<div class="row">
				<div class="col-sm-6 on-discount">
					<h3>Discount Codes</h3>
					<p>Enter your coupon code if you have one.</p>
					<input type="text" value="" name="zip area" class="zip-input">
					<button class="check-coverage action-button" type="button">Apply Coupon</button>
				</div>
				<div class="col-sm-6">
					<input type="button" name="Another Service" id="add_more" class="action-button Another_Service" value="+ Another Service" />

				</div>
			</div>
		</div>
	</div>
		<button type="button" name="next" class="next action-button discount-next" value="Next">next</button>
</fieldset>
*/ ?>

<script type="text/javascript">
	$(document).ready(function(){

		// Adding More Services into the Cart
		$('#add_more').click(function(){

			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/index",
				//data:{'vals':$('#mainVal').val()},
				type:"POST",
				success:function(result){
					$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
				},
				error:function(error){
					console.log(error);
				}
			});

		});

		// Deleting the Service from the Cart
		$(".del_service").click(function (){ 
			var service_id = $(this).attr('serv-id');
			var tr_instance = $(this).closest("tr");
					
		   if(confirm("Do you want to delete?")) {
		   	
		   }
		   else
		   {
		       return false;
		   }       
   
			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/deleteservice",
				data:{'vals':service_id},
				type:"POST",
				success:function(result){
					var no_of_trs = $('.table tr').length;
					if(no_of_trs>1){
						tr_instance.fadeOut();
						tr_instance.remove();
					}else{
						tr_instance.fadeOut();
						tr_instance.remove();
						$('#tb_body').append("<h2>Please Add a Service To Continue</h2>");
						$('.next').fadeOut();
					}
				},
				error:function(error){
					console.log(error);
				}
			});
		});


		$('.next').click(function(){

			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/checkcoverage",
				//data:{'vals':leftDetials},
				type:"POST",
				success:function(result){
					$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
				},
				error:function(error){
					console.log(error);
				}
			});
		});	
		
	});
</script>