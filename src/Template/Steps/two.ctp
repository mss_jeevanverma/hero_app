<?php 
$val = $cat_two;

?>
<div class="modal-dialog modal-lg">

<div class="modal-content">
<div class="modal-header">
<button aria-label="Close"  class="close" type="button"><span aria-hidden="true">×</span></button>
<!--ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Library</a></li>
<li class="active">Data</li>
</ol-->
</div>

<div class="modal-body text-center">
<!--h3 class="fs-title">New Stuff? Exciting!</h3-->
<h3 class="fs-title" id="error_mess" style="display:none;color:red;">New Stuff? Exciting!</h3>
<h2 class="fs-subtitle">
	<?php $stepOneData = end($_SESSION['steps']); ?>
	<?php if($stepOneData['stepOne']=='Setup & Install'){ ?>
		What are we installing?
	<?php }else if($stepOneData['stepOne']=='Optimize'){ ?>
		What are we optimizing?
	<?php }else	{ ?>
		Which of the following is annoying you?
	<?php }?>
	
</h2>

	<form class="first-form">
	<ul class="installing-list">
	<?php foreach($val as $data){  ?>

		<li name="<?php echo $data['category_name']; ?>" cat-id ="<?php echo $data['id']; ?>" >
			<div class="form-group">
				<label for="SetupInstall">
					<?php echo $data['html_data'] ?>
					<input type="radio" value="Setup & Install" name="quality">

					<?php if($stepOneData['stepOne']=='Optimize'){ ?>
						<?php $cat_arr= explode('-', $data['category_name']);  ?>
						<span><?php echo $cat_arr[0]; ?></span>
						<p style="font-weight: normal; font-size: 11px;" ><?php echo $cat_arr[1]; ?></p>

					<?php }else{ ?>
						<span><?php echo $data['category_name'] ?></span>
					<?php } ?>

				</label>
			</div>
		</li>
	<?php } ?>	
	</ul>
	</form>
</div>


<div class="modal-footer next-popup">
	<input type="hidden" id="mainVal" />
	<!-- <input type="button" name="next" class="next action-button" value="Next" /> -->
</div>

</div>
</div>

<?php /*
<fieldset>
			
	<div class="modal-header">
	<!--ol class="breadcrumb">
		<li><a href="#">Setup & Install </a></li>
		<li><a href="#">Smart Home</a></li>
		<li>Smart Home Device</li>
		<li><a href="#">Comments</a></li>
		<li  class="active"><a href="#">Summary</a></li>
		
	</ol-->
 	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 </div>
	<h3 class="fs-title">Let's get started</h3>
	<h2 class="fs-subtitle">What do you need to have set<br> up or installed?</h2>
	<div class="row inner-grid">
		<div class="installed-proceduer">
			<ul>
			<?php foreach($val as $data){  ?>

				<li name="<?php echo $data['category_name']; ?>" cat-id ="<?php echo $data['id']; ?>" >
					<?php echo $data['category_name'] ?>
				</li>
			<?php } ?>	
			</ul>

		</div>
	</div>
	<input type="hidden" id="mainVal" />
	<input type="button" name="previous" class="previous action-button" value="Previous" />
	<input type="button" name="next" class="next action-button" value="Next" />
</fieldset>
*/ ?>

<script type="text/javascript">
	// $(document).ready(function(){

	// var catName = '';
	// var catId = '';	
	// 	$('.installed-proceduer li').click(function(){

	// 		catName = $(this).attr("name");
	// 		catId = $(this).attr("cat-id");
	// 		var mainVal = '{"catName":"'+catName+'","catId":"'+catId+'"}';
	// 		$('#mainVal').val(mainVal);

	// 	});


	// 	$('.next').click(function(){

	// 		$.ajax({
	// 			url:"<?php echo $base_url_temp; ?>steps/three",
	// 			data:{'vals':$('#mainVal').val()},
	// 			type:"POST",
	// 			success:function(result){
	// 				$("#msform").html(result);
	// 			},
	// 			error:function(error){
	// 				console.log(error);
	// 			}
	// 		});
	// 	});
		
	// });
$(document).ready(function(){

	var catName = '';
	var catId = '';	
		$('.installing-list li').click(function(){

			catName = $(this).attr("name");
			catId = $(this).attr("cat-id");
			var mainVal = '{"catName":"'+catName+'","catId":"'+catId+'"}';
			$('#mainVal').val(mainVal);
			
			$('ul.installing-list li.current').removeClass('current');
			$(this).closest('li').addClass('current');

			$.ajax({
				url:"<?php echo $base_url_temp; ?>steps/three",
				data:{'vals':$('#mainVal').val()},
				type:"POST",
				success:function(result){
					$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
				},
				error:function(error){
					console.log(error);
				}
			});
		});


		$('.next').click(function(){
			
			if($('#mainVal').val().length<3){
				$("#error_mess").css({"display":"block"});
                      setTimeout(function(){
                           $("#error_mess").css({"display":"none"});
                            }, 3000);
				return false;
			}

			// $.ajax({
			// 	url:"<?php echo $base_url_temp; ?>steps/three",
			// 	data:{'vals':$('#mainVal').val()},
			// 	type:"POST",
			// 	success:function(result){
			// 		$("#msform").velocity("transition.perspectiveRightIn", { stagger: 250 }).html(result);
			// 	},
			// 	error:function(error){
			// 		console.log(error);
			// 	}
			// });
		});

		
	});
</script>