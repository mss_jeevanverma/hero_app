<?php /* <?= $this->Html->css('base.css') ?>
<?= $this->Html->css('cake.css') ?>
 */ ?>
<!--div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Testimonials'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="row">
    <div class="col-sm-12 heding_tab">
  <h2>Add Testimonial</h2>
          </div--> 
    
   
   <div class="row">
    <br /><br />
  <?php if (@$_GET["val"]=="create") { ?>                                    
                                     <center id="succ_message" styel="display:block;"><div role="alert"  class="alert alert-success" >Testimonial has been Create Successfully </div></center>
                                                                        
                                <?php }
                                    
                                  if(isset($_GET["val"]))
                                    {
                                      ?>
                                       <script type="text/javascript">
                                         $(document).ready( function() {
  
                                            setTimeout(function()
                                            {
                                            $("#succ_message").html('');
                                            }, 2000);
                                            });
                                  </script> 

                                       <?php 
                                    }

                                 ?>
    <h2 class="text-center">Add Testimonial</h2>
    <div class="col-lg-6 col-lg-offset-3">
    <?= $this->Form->create($testimonial, array('class' => 'text-center create-hero-form', 'id'=>'testimonial_form')) ?>
       <div class="form-group">
        <?php echo $this->Form->input('name',['div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Full Name']); ?>
        <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
        <?php echo $this->Form->input('testimonial',['class' => 'form-control input-lg','id'=>'test','div'=>false,'rows'=>false,'label' => false,'type'=>'textarea','required'=>'true','data-error'=>'Text Minimum Length 5','data-minlength'=>'5','maxlength'=>'200','placeholder'=>'Testimonial']); ?>
        <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
        <?php echo $this->Form->input('designation',['class' => 'form-control input-lg','div' => false,'label' => false,'type'=>'text','required'=>'true','data-error'=>'Text Minimum Length 5','data-minlength'=>'5','maxlength'=>'50','placeholder'=>'Designation']); ?>
        <div class="help-block with-errors"></div>
        </div>
        <div class="crt-btn">
     <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary action-button']) ?>
     </div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
<script>
$(document).ready(function() {
        $('#testimonial_form').validator();
    } );

</script>