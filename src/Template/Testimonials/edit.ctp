<?php /*<?= $this->Html->css('base.css') ?>
<?= $this->Html->css('cake.css') ?>
 */ ?>
<!--div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $testimonial->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $testimonial->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Testimonials'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="col-sm-12 heding_tab">
  <h2>Edit Testimonial</h2>
          </div-->              
   
        <div class="row">
           <br /><br />
     <?php if (@$_GET["val"]=="update") { ?>                                    
                                     <center id="email_message" styel="display:block;"><div role="alert"  class="alert alert-success" >Testimonial has been Update Successfully </div></center>
                                                                        
                                <?php }
                                    
                                  if(isset($_GET["val"]))
                                    {
                                      ?>
                                       <script type="text/javascript">
                                         $(document).ready( function() {
  
                                            setTimeout(function()
                                            {
                                            $("#email_message").html('');
                                            }, 2000);
                                            });
                                  </script> 

                                       <?php 
                                    }

                                 ?>
            <h2 class="text-center">Edit Testimonial</h2>
             <div class="col-lg-6 col-lg-offset-3">
        <?= $this->Form->create($testimonial,['class' => 'text-center create-hero-form','id'=>'edit_testimonial']) ?>     

            <div class="form-group">
                <?php  echo $this->Form->input('name',['class' => 'form-control','div'=>false,'label' => false,'required'=>'true','data-error'=>'Please enter only characters.','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','placeholder'=>'Full Name']); ?>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('testimonial',['class' => 'form-control','div'=>false,'rows'=>false,'label' => false,'type'=>'textarea','required'=>'true','id'=>'test','data-error'=>'Testimonial should be minimum 5 characters.','data-minlength'=>'5','placeholder'=>'Testimonial']); ?>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <?php echo $this->Form->input('designation',['class' => 'form-control ','div'=>false,'label' => false,'type'=>'text','required'=>'true','data-error'=>'Designation should be minimum 5 characters.','data-minlength'=>'5','placeholder'=>'Designation']);   ?>
                <div class="help-block with-errors"></div>
            </div>

        <div class="crt-btn">
        <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary action-button']) ?>
       </div>
        <?= $this->Form->end() ?>
    </div>
   </div>



<script>
$(document).ready(function() {
      $('#edit_testimonial').validator(); 

} );

</script>



                      