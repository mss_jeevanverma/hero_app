<?php /* <?= $this->Html->css('base.css') ?>
<?= $this->Html->css('cake.css') ?> */ ?>
<!--div class="row">
    <h3><?= __('Actions') ?></h3>
    <a href="/hero_app/testimonials/add"><button ></button></a>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Testimonial'), ['action' => 'add']) ?></li>
    </ul>
</div-->
<?php 

    $paginator = $this->Paginator;
 ?>

                         <div class="row heding1_tab"> 
                            
                            <div class="col-md-6"><h2>Manage Testimonials</h2></div>
                            <div class="col-sm-6 pull-right text-right"><a href="<?php echo $base_url_temp; ?>testimonials/add"><button type="button" class="btn btn-primary action-button hero-creat"><i class="fa fa-plus-circle"></i> Create New</button></a></div>
                        </div>


                    <div class="row">
                            <div  class="col-lg-12">
                                <div class="manage-heros-table">
                                    <div class="panel panel-default">
                                      <!-- Default panel contents -->
                                                <table class="table">
                                                <thead>
                                                    <tr>
                                                        <!--th><?= $this->Paginator->sort('id') ?></th>
                                                        <th><?= $this->Paginator->sort('Testimonial') ?></th-->
                                                            <th>Name</th>
                                                            <th>Designation</th>
                                                            <th class="actions"><?= __('Actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($testimonials as $testimonial): ?>
                                                    <tr>
                                                        <!--td><?= $this->Number->format($testimonial->id) ?></td-->
                                                        <td><?= h($testimonial->name) ?></td>
                                                        <td><?= h($testimonial->designation) ?></td>
                                                        <td class="actions">
                                                            <!--<?= $this->Html->link(__('View'), ['action' => 'view', $testimonial->id]) ?> -->
                                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $testimonial->id]) ?> /
                                                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $testimonial->id], ['confirm' => __('Are you sure you want to delete # {0}?', $testimonial->id)]) ?>
                                                        </td>
                                                    </tr>

                                                <?php endforeach; ?>
                                                </tbody>
                                                </table>
                                       </div>
                                 </div>
                           </div>
                    </div>
        <!--div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div-->
        <center>
        <div class='pagination'>
        <?php 
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){
                echo $paginator->prev("Prev");
            }
            echo $paginator->numbers(array('modulus' => 2));
            if($paginator->hasNext()){
                echo $paginator->next("Next");
            }
            echo $paginator->last("Last");
        ?>
        </div>
    </center>
    </div>
</div>    
