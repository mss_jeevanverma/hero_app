
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
     <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
     <title>Hero- The Digital Handyman</title>
     
     <style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Roboto:400,300,500);
/* Client-specific Styles */

#outlook a {
  padding: 0;
} /* Force Outlook to provide a "view in browser" menu link. */
body {
  width: 100% !important;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  margin: 0;
  padding: 0;
}
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
.ExternalClass {
  width: 100%;
} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%;
} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
#backgroundTable {
  margin: 0;
  padding: 0;
  width: 100% !important;
  line-height: 100% !important;
}
img {
  outline: none;
  text-decoration: none;
  border: none;
  -ms-interpolation-mode: bicubic;
}
a img {
  border: none;
}
.image_fix {
  display: block;
}
p {
  margin: 0px 0px !important;
}
table td {
  border-collapse: collapse;
}
table {
  border-collapse: collapse;
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}
a {
  color: #33b9ff;
  text-decoration: none;
  text-decoration: none!important;
}
/*STYLES*/
table[class=full] {
  width: 100%;
  clear: both;
}
     
         /*IPAD STYLES*/
      @media only screen and (max-width: 640px) {
a[href^="tel"], a[href^="sms"] {
  text-decoration: none;
  color: #33b9ff; /* or whatever your want */
  pointer-events: none;
  cursor: default;
}
.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
  text-decoration: default;
  color: #33b9ff !important;
  pointer-events: auto;
  cursor: default;
}
table[class=devicewidth] {
  width: 440px!important;
  text-align: center!important;
}
table[class=devicewidthinner] {
  width: 420px!important;
  text-align: center!important;
}
table[class=mainsmall1] {
  float: left!important;
}
table[class=mainsmall2] {
  float: right!important;
}
table[class=banner-gap] {
  display: none!important;
}
img[class="bannerbig"] {
  width: 440px!important;
  height: 371px!important;
}
img[class="spacinglines"] {
  width: 420px!important;
}
img[class="banner"] {
  width: 100%;
}
}
         /*IPHONE STYLES*/
      @media only screen and (max-width: 480px) {
a[href^="tel"], a[href^="sms"] {
  text-decoration: none;
  color: #33b9ff; /* or whatever your want */
  pointer-events: none;
  cursor: default;
}
.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
  text-decoration: default;
  color: #33b9ff !important;
  pointer-events: auto;
  cursor: default;
}
table[class=devicewidth] {
  width: 100%!important;
  text-align: center!important;
}
table[class=devicewidthinner] {
  width: 260px!important;
  text-align: center!important;
}
table[class=mainsmall1] {
  float: left!important;
  width: 120px!important;
  height: 90px!important;
}
table[class=mainsmall2] {
  float: right!important;
  width: 120px!important;
  height: 90px!important;
}
img[class=mainsmall1] {
  width: 120px!important;
  height: 90px!important;
}
img[class=mainsmall2] {
  width: 120px!important;
  height: 90px!important;
}
table[class=banner-gap] {
  display: none!important;
}
img[class="bannerbig"] {
  width: 280px!important;
  height: 236px!important;
}
img[class="spacinglines"] {
  width: 260px!important;
}
img[class="view_mr"] {
  height: inherit !important;
  width: 100% !important;
}
img[class="banner"] {
  width: 100%;
}
p[class="more-services-text"]{
  font-size: 14px !important;
  line-height: 20px;
}


}
</style>
     </head>
      <?php 
               $base_url_temp="http://" . $_SERVER['SERVER_NAME']."/grabahero/";
                             ?>
     <body style="font-family: Helvetica, arial, sans-serif;">

<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
       <tbody>
    <tr>
           <td></td>
         </tr>
  </tbody>
     </table>



<!-- Start of header -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
       <tbody>
    <tr>
           <td><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
               <tr>
                   <td width="100%"><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                       <tbody>
                       <!-- Spacing -->
                        <tr>
                           <td  bgcolor="#292929"><a href="<?php echo $base_url_temp;?>" target="_blank"><img class="banner" src="<?php echo $base_url_temp;?>img/email_images/header_1.png" alt="master banner"/></a></td>
                         </tr>
                       <!-- Spacing --> <!-- Spacing -->
                     </tbody>
                     </table></td>
                 </tr>
             </tbody>
             </table></td>
         </tr>
  </tbody>
     </table>
<!-- End of header --> 

<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
       <tbody>
    <tr>
           <td><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
               <tr>
                   <td width="100%"><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                       <tbody>
                       <!-- Spacing -->
                       <tr>
                           <td  bgcolor="#292929"><a href="<?php echo $base_url_temp;?>" target="_blank"><img class="banner" src="<?php echo $base_url_temp;?>img/email_images/banner_1.png" alt="master banner"/></a></td>
                         </tr>
                       <!-- Spacing --> <!-- Spacing -->
                     </tbody>
                     </table></td>
                 </tr>
             </tbody>
             </table></td>
         </tr>
  </tbody>
     </table>






<!-- Start of text -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
       <tbody>
          <tr>
           <td><table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
               <tr>
                   <td width="100%"><table width="560" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                       <tbody>
                       <!-- Spacing -->
                        <tr>
                          <td height="20"></td>
                       </tr>
                 

                       
                          <td height="10"></td>
                       </tr>
                       
                 
                       
                            <tr>
                          <td style="text-align:left; font-size:17px; line-height:24px;">Hi <?php echo ucfirst($customer_name); ?>,</td>
                       </tr>
                        <tr>
                          <td height="10"></td>
                       </tr>
                       <tr>
                          <td style="text-align:left; font-size:14px; line-height:24px;">Welcome to HERO! Thanks so much for joining us. You’re on your way to super-productivity and beyond! </td>
                       </tr>
                       <tr>
                          <td height="10"></td>
                       </tr>
                               <tr>
                          <td style="text-align:left; font-size:15px; line-height:24px;">A hero has been assigned to you. <br /><br /> Details are as follows :</td>
                       </tr>
                       <tr>
                          <td height="10"></td>
                       </tr>
                        
                        <tr>
                          <td height="10"></td>
                       </tr>
                       
            <tr>
                          <td height="20"></td>
                       </tr>
                       <!-- Spacing --> <!-- Spacing -->
                     </tbody>
                     </table></td>
                 </tr>
             </tbody>
             </table></td>
         </tr>
        </tbody>
</table>
<!-- End of text --> 

<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" id="backgroundTable" st-sortable="full-text">
       <tbody>
          <tr>
           <td><table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0"  align="center" class="devicewidth" >
               <tbody>
               <tr>
                   <td width="100%"><table width="560" cellpadding="0" cellspacing="0"  align="center" class="devicewidth">
                       <tbody>
                       <!-- Spacing -->
                        <tr><td><h2>Order Detail</h2></td></tr>
                        <tr style="">
                                              <th style="border:1px solid #000; height:20px;">Order Id</th>
                                                <th style="border:1px solid #000; height:20px;">Order Detail</th>
                                                 <th style="border:1px solid #000; height:20px;">Order Status</th>
                                                <th style="border:1px solid #000; height:20px;">Time and Date </th>
                                           </tr>
                                            <tr style="">
                                              <td style="text-align:center; border:1px solid #000; height:40px;">
                                                  <?php echo $data_hero['appointment_data']->id;?>  
                                              </td>
                                              <td style="text-align:center; border:1px solid #000; height:40px;">
                                                  <?php $order_data=json_decode($data_hero['appointment_data']->order_data,2);                                      
                                                 echo $order_data['stepOne'].' - '.$order_data['stepTwo'];
                                                 echo '<br/><p>'.$order_data['stepThree'].'<p>';



                                                  ?>  
                                              </td>
                                              <td style="text-align:center; border:1px solid #000; height:40px;">
                                                  <?php 
                                                  $status=$data_hero['appointment_data']->status;
                                                      if($status==2){
                                                      echo 'Canceled ';
                                                      }
                                                      else if($status==1){
                                                        echo ' Completed';
                                                      }
                                                      else{
                                                        echo 'Upcomming';
                                                      }

                                                ?>  
                                              </td>
                                              <td style="text-align:center; border:1px solid #000; height:40px;">
                                                  <?php echo $data_hero['appointment_data']->appointment_date;?>  
                                              </td>
                                            </tr>



                                     

                                      
                                            <tr>
                          <td height="20"></td>
                       </tr>
                       <!-- Spacing --> <!-- Spacing -->
                     </tbody>
                     </table></td>
                 </tr>
             </tbody>
             </table></td>
         </tr>
        </tbody>
</table>





<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" id="backgroundTable" st-sortable="full-text">
       <tbody>
          <tr>
           <td><table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0"  align="center" class="devicewidth" >
               <tbody>
               <tr>
                   <td width="100%"><table width="560" cellpadding="0" cellspacing="0"  align="center" class="devicewidth">
                       <tbody>
                       <!-- Spacing -->
                       <tr><td><h2>Hero Detail</h2></td></tr>
                                      <tr style="">
                                              <th style="border:1px solid #000; height:20px;">Hero Name</th>
                                                <td style="text-align:center; border:1px solid #000; height:40px;">
                                                     <?php echo $data_hero['current_hero_data']->first_name.'-'.$data_hero['current_hero_data']->last_name;?> 
                                                   </td>
                                            </tr>


                                          <tr>
                                                <th style="border:1px solid #000; height:20px;">Hero Email</th>
                                                <td style="text-align:center; border:1px solid #000; height:40px;">
                                                    <?php echo $data_hero['current_hero_data']->email;?> 
                                                </td>
                                            </tr>

                                          <tr>
                                            <th style="border:1px solid #000; height:20px;">Hero Phone No.</th>
                                            <td style="text-align:center; border:1px solid #000; height:40px;"><?php echo $data_hero['current_hero_data']->phone_no;?>  </td>
                                          </tr>
                                                                                
                                            <tr>
                          <td height="20"></td>
                       </tr>
                         <tr><td><p><h2>Thank You</h2></p></td></tr>
                       <!-- Spacing --> <!-- Spacing -->
                     </tbody>
                     </table></td>
                 </tr>
             </tbody>
             </table></td>
         </tr>

        </tbody>
</table>





<!-- more services text start here -->

<!-- more services text close here -->

<!-- text -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" bgcolor="111111" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- content -->
                                         
                                           
                                          <tr>
                                             <td height="20"></td>
                                           </tr>
                                         
                                        
                                          <!-- content -->
                                     
                                               <tr>
                                               <td height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                             </tr>
                                             
                                             <tr>
                                               <td height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                             </tr>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                          <!-- End of content -->
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                          </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of text -->





</body>
</html>












