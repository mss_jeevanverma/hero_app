<!-- Login popup html -->
<div class="modal fade bs-example-modal-lg6" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
  <div class="modal-dialog modal-lg first-signin">
  
    <div class="modal-content  home-login">
      <div class="modal-header">
       <h2 class="fs-subtitle">Sign In</h2>
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
           <ol class="breadcrumb">
  <!-- <li><a href="#">Home</a></li>
  <li><a href="#">Library</a></li>
  <li class="active">Data</li> -->
</ol>
          </div>
		  
		  <div class="modal-body text-center">
		 
		  <div class="row text-center">
		  <div class="col-sm-6">
            <form class="first-form" method="post" id="login-btn" action="<?php echo $base_url_temp; ?>login/index">
            	 <div id="frm_msg"></div>
				 <div class="form-group">
					 <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="inputEmail" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Please Enter a Valid Email')" placeholder="Email address" required autofocus>
				</div>
				
				<div class="form-group">
					 <input type="password" name="password" pattern=".{6,}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')"  maxlength="20" id="inputPassword" class="form-control" placeholder="Password" required>
				</div>
				
				<div class="row f-btn">
					
					<div class="col-sm-6 text-left">
					<button class="check-coverage action-button sign-btn"  value="Sign in" type="submit" >Sign In</button>
					 	</div>
					 	<div class="col-sm-6 text-center">
					<a href="#" class="check-coverage action-button sign-btn" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg8"> Register</a>
					</div>
					<button class="forgot-password" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg10" name="next" type="submit">Forgot Your Password?</button>
				</div>
            </form>
			</div>
			
			<div class="col-sm-1">
			<img src="<?php echo $base_url_temp; ?>img/template_images/or.png">
			
			</div>
			
			<div class="col-sm-5">
			<h3>Use an existing account</h3>
			<ul class="social-login">

				  <li><a href="<?php echo $base_url_temp; ?>login_api
					" ><img src="<?php echo $base_url_temp; ?>img/template_images/fb.jpg"></a>
                  </li>
				  <li><a href="<?php echo $base_url_temp; ?>login_api/gmail"><img src="<?php echo $base_url_temp; ?>img/template_images/google.jpg"></a>
				  </li>
				 <li><a href="<?php echo $base_url_temp; ?>login_api/twitterlogin"><img src="<?php echo $base_url_temp; ?>img/template_images/twitter.jpg"></a></li> 
				</ul>
			</div>
			
          </div>
		  </div>
	
	</div>
  </div>
</div>		
		<!-- Login popup html  end -->

			<!-- Resister popup html -->
			<div class="modal fade bs-example-modal-lg8 rgstrpop" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
			  <div class="modal-dialog modal-lg first-register">
			  
			    <div class="modal-content home-login">
			      <div class="modal-header">
			      <h2 class="fs-subtitle">Register</h2>
			            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
			           <ol class="breadcrumb">
			  <!-- <li><a href="#">Home</a></li>
			  <li><a href="#">Library</a></li>
			  <li class="active">Data</li> -->
			</ol>
          </div>
		  
		  <div class="modal-body text-center">
		  
		  <div class="row text-center">
		  <div class="col-sm-6">
            <form class="first-form" method="post"  id="register_form" action="<?php echo $base_url_temp; ?>login/register">
            	 <div id="reg_succ"></div>
            	  <div id="reg_error"></div>
				 <div class="form-group">
					<input type="text" name="first_name"   pattern="[a-zA-Z]{1,100}"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"  id="first_name" class="form-control" placeholder="First Name" required autofocus>
				</div>
				
				<div class="form-group">
					  <input type="text" name="last_name"  pattern="[a-zA-Z]{1,100}"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')" class="form-control" placeholder="Last Name" required autofocus>
				</div>
								
				<div class="form-group">
					 <input type="email" name="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" class="form-control" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Email Address')" placeholder="Email address" required autofocus>
				</div>

				<div class="form-group">
					<input type="password" name="password" maxlength="20" id="password" class="form-control" placeholder="Password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')"   pattern=".{6,}" required autofocus>
				</div>

				<div class="form-group">
				 <input type="password" id="confirmPassoword" maxlength="20" data-match="#password" data-match-error="Whoops, these don't match"  pattern=".{6,}" class="form-control" placeholder="Confirm New Password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Confirm Password should be minimum of 6 letters')"  required autofocus>
				 </div>
				<div class="row f-btn">
					
					<div class="col-sm-6 text-right">
					
				<button class="check-coverage action-button sign-btn" type="submit">Register</button>
					</div>
					<div class="col-sm-6 text-center">
				
				<a data-target=".bs-example-modal-lg6" class="check-coverage action-button sign-btn" data-toggle="modal" data-dismiss="modal" href="#">Login</a>
				</div>
				</div>

            </form>
			</div>
			
			<div class="col-sm-1">
			<img class="pop-prt" src="<?php echo $base_url_temp; ?>img/template_images/or.png">
			
			</div>
			
			<div class="col-sm-5">
			<h3 class="user-coount-heading">Use an existing account</h3>
			<ul class="social-login">

				  <li><a href="<?php echo $base_url_temp; ?>login_api" ><img src="<?php echo $base_url_temp; ?>img/template_images/fb.jpg"></a>
                  </li>
				  <li><a href="<?php echo $base_url_temp; ?>login_api/gmail"><img src="<?php echo $base_url_temp; ?>img/template_images/google.jpg"></a>
				  </li>
				<li><a href="<?php echo $base_url_temp; ?>login_api/twitterlogin"><img src="<?php echo $base_url_temp; ?>img/template_images/twitter.jpg"></a></li> 
				</ul>
			</div>
			
          </div>
		  </div>
	
	</div>
  </div>
</div>
		<!-- Register popup html  end -->



			<!-- Forget password popup html -->
			<div class="modal fade bs-example-modal-lg10" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel1">
			  <div class="modal-dialog modal-lg">
			  
			    <div class="modal-content">
			      <div class="modal-header">
			            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
			           <ol class="breadcrumb">
			  <!-- <li><a href="#">Home</a></li>
			  <li><a href="#">Library</a></li>
			  <li class="active">Data</li> -->
			</ol>
          </div>
		  
		  <div class="modal-body text-center">
		  <h2 class="fs-subtitle">Forgot Password</h2>
		  <div class="row text-center">
		  <div class="col-sm-12">
            <form class="first-form" method="post"  id="forgot_pass" action="">
            	 <div id="for_succ"></div>
            	  <div id="for_error"></div>
				 		
				<div class="form-group">

					 <input type="email" name="email" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Please enter a Valid Email Address')" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email_frget" class="form-control" placeholder="Email address" required autofocus>

				</div>
				
				<div class="row f-btn">
					
					<div class="col-sm-12">
					
				<button class="check-coverage action-button sign-btn" type="submit">Forgot</button>
					</div>
				</div>

            </form>
			</div>
			
			
          </div>
		  </div>
	
	</div>
  </div>
</div>
		<!-- Forget Password popup html  end -->

		<script>
		// $.validate({
		//   form : '#form_applicant'
		// });
	$(document).ready(function(){
	$('.modal').on('hidden.bs.modal', function () {
   $('#login-btn')[0].reset();
   $('#register_form')[0].reset();
   
})

		//$(document).on("click","#btn_applicant",function(){
		$(document).on("submit","#form_applicant",function(event) {

			$.ajax({
				url:"<?php echo $base_url_temp; ?>index/applicants",
				data:$("#form_applicant").serialize(),
				type:"POST",
				success:function(result){
					$("#form_applicant").html(result);
				},
				error:function(error){
					console.log(error);
				}
			});
			event.preventDefault();
		});


			// Sign In if not 
		$(document).on("submit","#login-btn",function(event) {
		event.preventDefault();
		var inputEmail = $('#inputEmail').val();
		var inputPassword = $('#inputPassword').val();
		
		var data = {"email":inputEmail,'password':inputPassword};

		$.ajax({
			url:"<?php echo $base_url_temp; ?>login/index",
			data:data,
			type:"POST",
			success:function(result){
				if(result=='User Logged'){
				
				window.location.href = '<?php echo $base_url_temp; ?>user_dashboard/index';
				
				}
				else if(result=='Admin Logged'){
				
               window.location.href = '<?php echo $base_url_temp; ?>admin_dashboard/statics';
				
				}
				
				else if(result=='Hero Logged'){
				
               window.location.href = '<?php echo $base_url_temp; ?>hero_dashboard/index';
				
				}
				else if(result=='Your Account is Not Activated.'){				
               		$('#frm_msg').html('<div role="alert"  class="alert alert-danger" >Your Account is deactivated, Please contact Admin</div>');
					 $("#login-btn").find("#inputPassword").val('');
					setTimeout(function(){						
					  $('#frm_msg').html('');
					}, 2000);				
				}
				else{
					$('#frm_msg').html('<div role="alert"  class="alert alert-danger" >Your Email and Password does not match</div>');
					 $("#login-btn").find("#inputPassword").val('');
					setTimeout(function(){						
					  $('#frm_msg').html('');
					}, 2000);
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});	

	$(document).on("submit","#register_form",function(event) {
		var pass = $('#password').val();
		var conf = $('#confirmPassoword').val();
		event.preventDefault();
		if(pass==conf)
		{
			$.ajax({
				url:"<?php echo $base_url_temp; ?>login/register",
				data:$("#register_form").serialize(),
				type:"POST",
				success:function(result){
				if(result=='User Created Successfully'){
					$("#reg_succ").html('<div role="alert"  class="alert alert-success" >User Created Successfully</div>');
					setTimeout(function(){
					  $('#reg_succ').html('');
					  $("#register_form")[0].reset();
					}, 3000);
				}
				else if(result=='Email already Exists'){
					$('#reg_error').html('<div role="alert"  class="alert alert-danger" >Email already Exists</div>');
					$("#register_form")[0].reset();
					setTimeout(function(){
					  $('#reg_error').html('');
					  
					}, 2000);
				}
				},
				error:function(error){
					console.log(error);
				}
			});
		}
				else
				{
				$("#reg_succ").html('<div role="alert"  class="alert alert-danger" >Password Does not match</div>');
				$("#register_form")[0].reset();
					setTimeout(function(){
					  $('#reg_succ').html('');					  
					}, 2000);
				}
			
		});

				//Send email at time of forgot password		
			$(document).on("submit","#forgot_pass",function(event) {
				event.preventDefault();
				
			$.ajax({
				url:"<?php echo $base_url_temp; ?>login/forgot_send_mail",
				data:$("#forgot_pass").serialize(),
				type:"POST",
				success:function(result){
				if(result=='Email Successfully Send'){
					$("#for_succ").html('<div role="alert"  class="alert alert-success" >Reset Password Link Send to Your Email Address</div>');
					$("#forgot_pass")[0].reset();
					setTimeout(function(){
					  $('#for_succ').html('');
						}, 2000);
				}
				else if(result=='Email Does Not Exists'){
					$('#for_error').html('<div role="alert"  class="alert alert-danger" >Email Does Not Exists</div>');
					$("#forgot_pass")[0].reset();
					setTimeout(function(){
					  $('#for_error').html('');
					}, 2000);
				}
				},
				error:function(error){
					console.log(error);
				}
			});
			
		});




	});
</script>
