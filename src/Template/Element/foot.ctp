<footer class="footer">
    <div class="container">
       <div class="row">
       <div class="col-md-4 col-sm-6"><div class="copyright">
        <ul>
          <?php      
            foreach($layout_pages_data as $page){ 
              $title = $page['title'];
              $url_title = $page['url_title'];
            ?>
          <li>
            <a href="<?php echo $base_url_temp; ?>page/<?php echo $url_title ?>">
              <?php echo $title; ?>
            </a>
          </li>
          <?php      } ?>

          <li>
            <a href="<?php echo $base_url_temp; ?>index/beahero">
              Be a  Hero- join the Team
            </a>
          </li>        
        </ul>
       </div></div>
       <div class="col-md-4 hidden-xm hidden-md">
        <div class="social-footer">
          <ul> 
            <li><a href="https://www.facebook.com/Grab-A-Hero-863521650431904/?fref=ts"><i class="fa fa-facebook"  target="_blank" ></i></a></li>
            <li><a href="https://twitter.com/grabahero"  target="_blank" ><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.linkedin.com/company/9496539" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
       </div>
       <div class="col-md-4 col-sm-6">
           
       </div>
     </div>
    </div>
  </footer>