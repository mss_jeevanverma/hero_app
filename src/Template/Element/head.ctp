<?php 
	@$session = $this->request->session();
	@$user_info = $session->read('UserInfo');

/*
* ******************** Header Menu for Hero Dashboad  ****************************
*/	 

	if($user_info['role']==3){  ?>

	<nav class="navbar navbar-inverse navbar-fixed-top" id="account_nav">
		<div class="container">
			<div class="navbar-header appointment-page">
				<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php echo $base_url_temp; ?>" class="navbar-brand"><img alt="logo" src="<?php echo $base_url_temp; ?>img/template_images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo $base_url_temp; ?>hero_dashboard/hero_info">Appointments</a> </li>
					<li><a href="<?php echo $base_url_temp; ?>hero_dashboard/">Account </a> </li>
					<li><a href="#">Hello : <?php echo $user_info["first_name"]; ?> </a> </li>
					<?php if(count($user_info)) { ?>     
					<li><a href="<?php echo $base_url_temp; ?>login/logout">Logout</a></li>
					<?php } ?>

				</ul>

				<ul class="nav navbar-nav navbar-right phone-n phone-n-new">
				 <li style="background:none" >
					<a href="#"><p><i class="fa fa-phone"></i>1(888) 9-MY-HERO</p>
					<span>969 4376</span></a>
					</li>
					<!-- <li>
					   <a href="#"> <i class="fa fa-phone"></i>1(888) 9-MY-HERO</a>
					</li>
					<li>
					   <a href="#"> <i class="fa fa-phone"></i>1(888) 969-4376</a>
					</li> -->
				</ul>

			</div><!--/.nav-collapse -->
		</div>
	</nav>	

<?php

/*
* ******************** Header Menu for User Dashboad  ****************************
*/	 

 }elseif($user_info['role']==2){ ?>


	<nav class="navbar navbar-inverse navbar-fixed-top" id="account_nav">
		<div class="container">
			<div class="navbar-header appointment-page">
				<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php echo $base_url_temp; ?>" class="navbar-brand"><img alt="logo" src="<?php echo $base_url_temp; ?>img/template_images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo $base_url_temp; ?>user_dashboard/appointment_info">Appointments</a> </li>
					<li><a href="<?php echo $base_url_temp; ?>user_dashboard/">Account </a> </li>
					<li><a href="#">Hello : <?php echo $user_info["first_name"]; ?> </a> </li>
					<?php if(count($user_info)) { ?>     
					<li><a href="<?php echo $base_url_temp; ?>login/logout">Logout</a></li>
					<?php } ?>
				</ul>


				<ul class="nav navbar-nav navbar-right phone-n phone-n-new">
				 <li style="background:none" >
					<a href="#"><p><i class="fa fa-phone"></i>1(888) 9-MY-HERO</p>
					<span>969 4376</span></a>
					</li>
					<!-- <li>
					   <a href="#"> <i class="fa fa-phone"></i>1(888) 9-MY-HERO</a>
					</li>
					<li>
					   <a href="#"> <i class="fa fa-phone"></i>1(888) 969-4376</a>
					</li> -->
				</ul>


			</div><!--/.nav-collapse -->

		</div>
	</nav>

<?php 

/*
* ******************** Header Menu for Admin Dashboad  ****************************
*/	 

}elseif($user_info['role']==1){ ?>



	<nav class="navbar navbar-inverse navbar-fixed-top" id="account_nav">
		<div class="container">
			<div class="navbar-header appointment-page">
				<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a href="<?php echo $base_url_temp; ?>" class="navbar-brand"><img alt="logo" src="<?php echo $base_url_temp; ?>/img/template_images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">

					<li><a href="<?php echo $base_url_temp; ?>admin_dashboard/getappointments">Appointments</a> </li>
					<li><a href="<?php echo $base_url_temp; ?>admin_dashboard/statics">Statics </a> </li>
					<li><a href="<?php echo $base_url_temp; ?>admin_dashboard/get_pages">Pages </a> </li>
					<li><a href="#">Welcome : <?php echo $user_info["first_name"]; ?> </a> </li>
				</ul>
				<?php if(count($user_info)) { ?>
				<a href="<?php echo $base_url_temp; ?>login/logout">
					<ul class="nav navbar-nav navbar-right">
						<li class="account_button"><button>Logout <i class="fa fa-angle-double-right"></i></button></li>
					</ul>
				</a>
				<?php } ?>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php

/*
* ******************** Header Menu When No One is Logged In  ****************************
*/	 

	}else{

	?>

		<div class="navbar navbar-default navbar-fixed-top">
		  <div class="container"> 
		    <div class="navbar-header">

		    <a href="<?php echo $base_url_temp; ?>" class="navbar-brand"><img src="<?php echo $base_url_temp; ?>img/template_images/logo.png" alt="logo"/></a>
		    <ul class="nav navbar-nav pull-left nav_log">

		      <?php 
		        if(count($user_info))
		        { 
		      ?>    
		            <li class="login_btn"><a href="<?php echo $base_url_temp; ?>login/logout"><i class="fa fa-lock"></i> Logout</a></li>

		            <?php 
		            $myAccountUrl = '';
		              if ($user_info['role']==2) { $myAccountUrl = 'user_dashboard'; }
		              else if ($user_info['role']==3) { $myAccountUrl = 'hero_dashboard'; }
		              else if ($user_info['role']==1) { $myAccountUrl = 'admin_dashboard'; }

		              ?>
		            <li class="login_btn"><a href="<?php echo $base_url_temp.$myAccountUrl; ?>"><i class="fa fa-user"></i> My Hero</a></li>


		      <?php }else{ ?>
		           
		            <li class="login_btn"><a href="#" data-dismiss="modal" data-toggle="modal" data-target=".bs-example-modal-lg6"><i class="fa fa-lock"></i> Login</a></li>
		      <?php  }
		      ?> 
		    </ul>
		    <ul class="nav navbar-nav navbar-right  nav_log phone-n">
		     <li style="background:none" >
					<p><i class="fa fa-phone"></i>1(888) 9-MY-HERO</p>
					<span>969 4376</span>
					</li>
		      <!-- <li style="background:none" ><i class="fa fa-mobile"></i> 1(800) 9-MY-HERO</li>
		      <li style="background:none" ><i class="fa fa-mobile"></i> 1(888) 969 4376</li> -->
		    </ul>        
		    </div>
		  </div>  
		</div>
		<!-- header end here -->

	<?php
	}

?>
