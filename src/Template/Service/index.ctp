<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

?>

<div id="content">
    <div class="row">
        <form class="form-signin" action="" method="post">
            <h2 class="form-signin-heading">Book A Service</h2>
            <select id ="first_parent">
            <option value="">Select An Option</option>
            <?php foreach($parent_categories as $categories){ ?>
                <option value="<?php echo $categories->id; ?>"><?php echo $categories->category_name; ?></option>
            <?php } ?>    
            </select>
            <select id="second_parent" style="display:none;">
            </select>
            <select id="third_parent" style="display:none;">
            </select>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(e){

        // On Change Event on First Select box 
        $(document).on("change","#second_parent",function(e){
            var cat_val = $(this).val();
            $('#second_parent').find('option').remove()
            $('#second_parent').fadeIn();
            $.ajax({
                url:"service/nextcat",
                type:"POST",
                data:{"parent_id":cat_val},
                success:function(result){

                    if(result.length>5){
                        result = JSON.parse(result);

                            $.each(result, function() {
                               $('#second_parent').append(
                                    $("<option></option>").text(this.category_name).val(this.id)
                               );
                            });
                    }else{
                        $('#second_parent').fadeOut();    
                    }        
                },
                error:function(error){
                    console.log(error);
                }
            });
        });


        // On Change Event on First Select box 
        $(document).on("change","#second_parent",function(e){
            var cat_val = $(this).val();
            $('#third_parent').find('option').remove()
            $('#third_parent').fadeIn();
            $.ajax({
                url:"service/nextcat",
                type:"POST",
                data:{"parent_id":cat_val},
                success:function(result){

                    if(result.length>5){
                        result = JSON.parse(result);

                            $.each(result, function() {
                               $('#third_parent').append(
                                    $("<option></option>").text(this.category_name).val(this.id)
                               );
                            });
                    }else{
                        $('#third_parent').fadeOut();    
                    }        
                },
                error:function(error){
                    console.log(error);
                }
            });
        });

    });
</script>