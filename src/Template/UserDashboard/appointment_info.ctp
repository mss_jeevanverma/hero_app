<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $appointment_data = @$data["appointment_data"];

 ?>
<section class="appointment">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
            	<div class="appointment_left">
                <h2>Appointments </h2>
                <div class="table_header">
                    <div class="upcoming">
                        <h2 id="up_button" style="cursor:pointer;">Upcoming</h2>
                    </div>
                    <div class="complete">
                        <h2 id="com_button" style="cursor:pointer;">Complete</h2>
                    </div>
                </div>
     <table class="table table-hover" id="upcoming">
	
    <tbody>
                            <?php  
                            if(count($appointment_data))
                            {
                            foreach($appointment_data as $appointment_content) { 
                                $data_con=$appointment_content->order_data;
                                $order_content=json_decode($data_con,2);
                              ?>
                              <tr id="<?php echo $appointment_content->id; ?>">
                                <td>
                                <!-- <p>Sep 19, 2015  11am</p> -->
        
                        <?php 
                            $order_data=$appointment_content->appointment_date;
                            $appoint_date=explode(" ",$order_data);
                            $yrdata= strtotime($appoint_date['0']);
                            $app_date=date('M d, Y', $yrdata);
                            $time_data=strtotime($appoint_date['1']);
                            //Calculate difference
                            $diff=$yrdata-time();//time returns current time in seconds
                            $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
                            $days=$days+1;
                            if($days==0)
                            {
                              echo '<h4>Today</h4>';  
                            }
                            else if($days<1)
                            {
                             echo '<h4>Appointment Date Passed</h4>';   
                            }
                            else{
                            echo '<h4>In '.$days.' Days</h4>';
                                }
                            $app_time=date('H:i:s A', $time_data);
                            echo '<p>'.$app_date.' '.$app_time. '</p>';
                        ?> 
       
                            <h3><?php echo $order_data=$order_content['stepOne'].'-'.$order_content['stepTwo']; ?>
                            </h3>
                            <p><?php echo  $order_data=$order_content['stepThree']; ?></p>
                            </td>
                            <td>     
                                <h2>Work Estimate</h2>
                                     <h5><?php 
                                        $hour=$appointment_content->add_hour;
                                            if($hour!=0)
                                            {
                                            echo $hour .' hours'; 
                                            }
                                            else{
                                                echo 'No Hour Yet';
                                            }
                                            ?> </h5>
                            </td>
                            <td class="person"><img src="<?php echo $base_url_temp; ?>img/template_images/person.png"></td>
                            
                            <td class="app_cancel">
                            <a href="<?php echo $base_url_temp; ?>user_dashboard/appointment_view?order_id=<?php echo $appointment_content->id; ?>"><button type="button" class="btn btn-primary" id="view_button">View</button></a>
                            </td> 
                    		<!--td class="app_cancel">
                            <button type="button" complete_id="<?php echo $appointment_content->id; ?>" class="btn btn-primary" id="complete_button" data-target=".bs-example-modal-sm<?php echo $appointment_content->id; ?>">Complete</button>
                            </td -->
                            <td class="app_cancel">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm<?php echo $appointment_content->id; ?>">Cancel</button>
                           </td>
                    		
                    		<!-- modal -->
                                <div class="modal fade bs-example-modal-sm<?php echo $appointment_content->id; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                  <div class="modal-dialog modal-sm appointment-modal">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                                       
                                        </div>
                                        <div class="modal-body">
                                          <div class="container-fluid text-center">
                                		  <h3>Cancel Appointmanet</h3>
                                		  <p>Are You sure! you want to cancel this appointment?</p>
                                        
                                        </div>
                                        <div class="modal-footer">
                                		  <div class="col-xs-6 m-f-part"><button class="btn btn-primary yes-btn" data-dismiss="modal" id="appoint_popup<?php echo $appointment_content->id; ?>" order-id="<?php echo $appointment_content->id; ?>" type="button">Yes</button></div>
                                          <div class="col-xs-6 m-f-part" style="border:none"><button data-dismiss="modal" class="btn btn-default black-btn" type="button">Don't cancel</button></div>          
                                        </div>
                                      </div>
                                  </div>
                                </div>
      </tr>
	  <?php
            }
                    }
                        else
                            {
                             echo '<tr><td><p>No Upcoming Appointment Yet</p></td></tr>';
                            } 
            ?> 
	  
    </tbody>
  </table>
   <table class="table table-hover" id="complete" style="display:none;">
    
    <tbody>
                <?php 
                $appointment_data1 = @$data1["appointment_data1"];
                if(count($appointment_data1))
                {
                foreach($appointment_data1 as $appointment_content)
                 { 
                    $data_con=$appointment_content->order_data;
                    $order_content=json_decode($data_con,2);
                ?>
                      <tr id="<?php echo $appointment_content->id; ?>">
                                <td>
                                    <h4></h4>
                                    <!--<h4>In 5 Days</h4>-->
                                
                                       <p> 
                                        <?php 
                                            $order_data=$appointment_content->appointment_date;
                                            $appoint_date=explode(" ",$order_data);
                                            $yrdata= strtotime($appoint_date['0']);
                                            $app_date=date('M d, Y', $yrdata);
                                            $time_data=strtotime($appoint_date['1']);
                                            $app_time=date('H:i:s A', $time_data);
                                            echo $app_date.' '.$app_time 
                                        ?> 
                                        </p>
                                        <h3><?php echo $order_data=$order_content['stepOne'].'-'.$order_content['stepTwo']; ?>
                                        </h3>
                                        <p><?php echo  $order_data=$order_content['stepThree']; ?></p>
                                </td>
                                <td>  
                                    <h2>Work Estimate</h2>
                                     <h5><?php 
                                        $hour=$appointment_content->add_hour;
                                            if($hour!=0)
                                            {
                                            echo $hour .' hours'; 
                                            }
                                            else{
                                                echo 'No Hour Yet';
                                            }
                                            ?> </h5>
                                </td>
                                <td class="app_cancel">
                            <a href="<?php echo $base_url_temp; ?>user_dashboard/appointment_view?order_id=<?php echo $appointment_content->id; ?>"><button type="button" class="btn btn-primary" id="view_button">View</button></a>
                            </td> 
                                <td class="person">    
                                      <img src="<?php echo $base_url_temp; ?>img/template_images/person.png">
                                </td>
                       </tr>
      
              <?php
            }
                    }
                        else
                            {
                             echo '<tr><td><p>No Complete Appointment Yet</p></td></tr>';
                            } 
            ?>
         
    </tbody>
  </table>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="appointment_rt">
                	<!--div class="services">
                    	<a href=""><img src="<?php echo $base_url_temp; ?>img/template_images/home.png"></a>
                        <h2>In-home Service </h2>
                        <p>We arrive at your home on time and ready to get right to work. </p>
                    </div-->
                    <div class="services mobile">
                    	<a href=""><img src="<?php echo $base_url_temp; ?>img/template_images/mobile.png"></a>
                        <h2>Online Service </h2>
                        <p>Connect with one of our friendly experts remotely via Chat, Video Chat and Screen Share.  </p>
                    </div>
                    <div class="services">
                    	<a href=""><img src="<?php echo $base_url_temp; ?>img/template_images/satesfection.png"></a>
                        <h2>100%  Satisfaction</h2>
                        <p>We're commited to making sure you are completely satisfied.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function()
{  
   $(document).on("click",".yes-btn",function (event)
        {
            var order_id = $(this).attr('order-id');
            $.ajax({
                type:"POST",
                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/cancel_appointment",
                data:{'id':order_id},
                success:function(result)
                {
                   $('#'+order_id).remove();
                }
            });                
         });

   $(document).on("click","#complete_button",function (event)
        {
            var complete_id = $(this).attr('complete_id');
            $.ajax({
                type:"POST",
                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/complete_appointment",
                data:{'id':complete_id},
                     success:function(result)
                      {
                        $('#'+complete_id).remove();
                          window.location.reload();                  
                    

                      }
                });                
         });
  
   

    $(document).on("click","#com_button",function (event)
        {
            $("#complete").show();
            $("#upcoming").hide();
            $(".upcoming").css('background', '#333');
            $("#up_button").css('background', '#333');
            $("#com_button").css('background', '#9acd32');
            $(".complete").css('background', '#9acd32');
        });

    $(document).on("click","#up_button",function (event)
       {   

            $("#upcoming").show();
            $("#complete").hide();
            $(".complete").css('background', '#333');
            $(".upcoming").css('background', '#9acd32');
            $("#com_button").css('background', '#333');
            $("#up_button").css('background', '#9acd32');
        });
});
</script>
