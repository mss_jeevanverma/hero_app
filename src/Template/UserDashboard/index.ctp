<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>
<?php
    $user_info = $this->request->session()->read('UserInfo');
    $customer = $data["customer"][0];

?>

<section class="account_contant">
<div class="container">
    <div class="row">
        <div class="account_form">
        <h2>Account Information  </h2>
        <div class="col-md-6">
            <div class="account_left">
        <form class="account_fm" role="form"  method="post" action="" id="customerinfo">
                        <div id="ci_msg" class="alert alert-success" style="display:none;">
                            Your Account information changed successfully.
                        </div>
                        <h3>Update Account   </h3>
                            <label>First Name</label>
                                <input type="text" name="first_name"  pattern="[a-zA-Z]{1,100}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"  id="first_name"maxlength="20"  class="form-control input-lg" placeholder="First Name" tabindex="1" value="<?php echo $customer->first_name; ?>"  required>
                                
                                            <div class="help-block with-errors"></div>
                                
                                <label>Last Name</label>
                                <input type="text" name="last_name" pattern="[a-zA-Z]{1,100}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Only Character and Minimum Length 1 Letter')"  id="last_name" class="form-control input-lg"  value="<?php echo $customer->last_name; ?>" placeholder="Last Name" tabindex="2" required>
                               
                                            <div class="help-block with-errors"></div>
                               
                                <label>Email ID</label>
                                <input type="email" placeholder="testing@gmail.com" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$"  class="form-control input-lg" tabindex="4" value="<?php echo $customer->email; ?>" disabled/>
                                         <div class="help-block with-errors"></div>
                             <div class="submition">
                                <div class="col-md-4 spacing">
                                    <div class="update_changes">
                                        <button  id="basic_info" class="btn btn-primary btn-block btn-lg">Update Changes </button>
                                    </div>
                                </div>
                        
                                           <div class="col-md-4">
                                                <div class="cancel">
                                                    <!-- <button id="clear_info">Cancel</button> -->
                                                </div>
                                            </div>
                                    <div class="col-md-4 kk">
                                            <div class="update_changes">
                                                <button id="change_pass" class="btn btn-primary btn-block btn-lg">Change Password</button>
                                            </div>
                                    </div> 
                    </div>
        </form>
       
                    
                    
                    
        <form class="account_fm" role="form" method="post" action="" id="customerpass" style="display:none;">            
                                <div id="ci_msg1" class="alert alert-success" style="display:none;">
                                    Your Account information changed successfully.
                                </div>
                                <div id="error_pass"></div>
                                         <h3>Change Password</h3>
                                        <label>Current Password</label>
                                            <input type="password" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')" pattern=".{6,}" name="password" id="password" class="form-control input-lg" maxlength="20" placeholder="Password"  tabindex="5" required>
                                            <div class="help-block with-errors"></div>
                                                    <label> New Password </label>
                                            <input type="password"  onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Password should be minimum of 6 letters')" pattern=".{6,}" name="new_password"  id="confpassword" maxlength="20"  class="form-control input-lg" placeholder="New Password" tabindex="5" required>
                                                 <div class="help-block with-errors"></div>
                                                        <label>Confirm New Password</label>
                                            <input type="password" pattern=".{6,}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity(' Confirm Password should be minimum of 6 letters')"   data-match="password" id="matchpassword" maxlength="20" data-match-error="Whoops, these don't match" class="form-control input-lg" placeholder="Confirm New Password"   tabindex="6" required>
                                                    <div class="help-block with-errors"></div>
                                                    <div class="submition">
                                                        <div class="col-md-3 spacing">
                                                            <div class="apply">
                                                                     <button id="password_info" class="btn btn-primary btn-block btn-lg">Apply</button>
                                                          </div>
                                                        </div>
                                                         <div class="col-md-3 kk">
                                                            <div class="cancel">
                                                               <!-- <button>Cancel</button> -->
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                    
        </form>           
                    
                    
                    
    <form class="account_fm" role="form" method="post" action="" id="customerphone">           
                     <div id="ci_msg2" class="alert alert-success" style="display:none;">
                            Your Account information changed successfully.
                        </div>
                    <h3>Update Contact</h3>
                    <label>Phone Number</label>
                                 <input type="text" name="phone_no" id="phone_no" class="form-control input-lg" placeholder="Phone Number"  pattern=".{10,}" maxlength="13" data-match-error="Please add valid Phone Number" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter a valid Phone Number')" tabindex="3" value="<?php echo $customer->phone_no; ?>" required>
                                <div class="help-block with-errors"></div>

                                     <!-- Squared TWO -->
                                    <div class="squaredTwo">
                                           <input type="checkbox" <?php if($customer->phone_text) { echo 'checked'; }?> value="1" id="squaredTwo" name="phone_text" />
                                             <label for="squaredTwo"></label>
                                    </div>
                                              
                          <h2>This is a mobile number. Send Hero arrival status via text.</h2>
                                    <div class="submition">
                                        <div class="col-md-4 spacing">
                                            <div class="apply_1">
                                                <button  id="phone_info" class="btn btn-primary btn-block btn-lg" >Update Changes</button>
                                             </div>
                                        </div>
                                         <div class="col-md-3 kk">
                                            <div class="cancel">
                                               <!-- <button>Cancel</button> -->
                                            </div>
                                        </div>
                                       
                                    </div>
                    
         </form>            
               
        </div>
        </div>
        <div class="col-md-6">
            <div class="account_left">
                     <h3>Payment Method</h3>
                     <div class="submition">
                     <p></p>
                     <div id="ci_msg4" class="alert alert-success" style="display:none;">
                            Your Payment information changed successfully.
                        </div>
                        <!--div class="col-md-3 spacing">
                            <div class="apply">
                                <button id="add_new" style="width:140px;">Edit Credit Info</button>
                            </div>
                        </div-->
                         <div class="col-md-3 kk">
                            <div class="cancel">
                               <!-- <button>Cancel</button> -->
                            </div>
                        </div>
                       
                    </div>
                    <form class="account_fm_rt account_fm" role="form" method="post" action="" id="customerPayment" >
                        <div class="help-block with-errors"></div>
                          <label> Credit Card Number</label>
                            <input type="text" placeholder="Card Number"  name="a_card_no" id="display_name"  pattern="^[0-9]{1,}$" maxlength="16" data-minlength="16" data-match-error="Please add valid Card Number" class="form-control input-lg" tabindex="3" value="<?php echo $customer->a_card_no; ?>" required disabled>

                           <label> Expiry Date </label> 

                            <input placeholder="Expiry Date"  readonly='true' type="text" name="a_exp_date" id="a_exp_date"    class="form-control input-lg"  data-match-error="Please add valid expiry date" tabindex="3" value="<?php echo $customer->a_exp_date; ?>" required disabled>

                              
                           <label> CVV Number </label>
                            <input type="password" name="a_cvv" id="display_name" placeholder="CVV" pattern="^[0-9]{1,}$" maxlength="3" data-minlength="3" class="form-control input-lg"  data-match-error="Please add valid CVV" tabindex="3" value="<?php echo $customer->a_cvv; ?>" required disabled>
                             <label> Postal Code </label>  

                             <input type="text" name="a_postal_code"  class="form-control input-lg"  placeholder="Postal Code" pattern="^[0-9]{1,}$" data-minlength="5" maxlength="7" data-match-error="Please add valid Postal Code" tabindex="3" value="<?php echo $customer->a_postal_code; ?>" required disabled>
                              
                                    <div class="submition">
                                        <div class="col-md-4 spacing">
                                            <!--div class="apply">

                                                <input type="submit" value="Update" class="btn btn-primary btn-block btn-lg" tabindex="7">

                                          </div-->
                                        </div>                 
                                 </div>
                    </form>
                    
            </div>
        </div>
    </div>
    </div>
</div>

 </section> 
                  <style type="text/css">
                  .ui-datepicker-calendar {
                      display: none;
                   }
                  </style>
                   

                <script type="text/javascript">
                $(function() {
                                    $('#a_exp_date').datepicker({
                                        changeMonth: true,
                                        minDate: 0,
                                        constrainInput: false,
                                        changeYear: true,
                                        showButtonPanel: true,
                                        dateFormat: 'MM yy'
                                    }).focus(function() 
                                    {
                                        var thisCalendar = $(this);
                                        $('.ui-datepicker-calendar').detach();
                                        $('.ui-datepicker-close').click(function()
                                         {
                                                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                                        thisCalendar.datepicker('setDate', new Date(year, month, 1));
                                        });
                                    });
                            });
                </script>
    <script type="text/javascript">
    $(document).ready(function()
    { 
      //$('.account_left small').css({'display':'block'});
      //$('.help-block',this).hide();
             $("#phone_no").mask("(999)999-9999");
             // $("#a_exp_date").mask("99/99");

                
                      $("#myTab a").click(function(e)
                      {
                        e.preventDefault();
                        $(this).tab('show');
                      });
                      $("#change_pass").click(function(event)
                      {
                     event.preventDefault();
                     $("#customerpass").toggle();
                      });
                      $("#add_new").click(function(event)
                      {
                      event.preventDefault();
                     //$("#customerPayment").toggle();
                      });

     
                        //customer Info Change
                        $(document).on("submit","#customerinfo",function (event){
                            event.preventDefault();
                            var data = $("#customerinfo").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editcustomer",
                                data:data,
                                success: function(result){
                                    $("#ci_msg").css({"display":"block"});
                                    setTimeout(function(){
                                              $("#ci_msg").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                        });

                     //Password Info Change
                        $(document).on("submit","#customerpass",function (event){
                            var match_pass = $('#matchpassword').val();
                            var conf = $('#confpassword').val();
                            
                            event.preventDefault();
                            var data = $("#customerpass").serialize();
                            if(conf==match_pass)
                            {
                                $.ajax({
                                    type:"POST",
                                    url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editpassword",
                                    data:data,
                                    success: function(result){
                                    if(result=='Current Password is wrong')
                                    {
                                         $('#error_pass').html('<div role="alert"  class="alert alert-danger" >Current Password is wrong</div>');
                                            setTimeout(function(){
                                              $('#error_pass').html('');
                                            }, 3000);
                                    }
                                    else
                                        {
                                            $("#ci_msg1").css({"display":"block"});
                                            setTimeout(function(){
                                              $("#ci_msg1").css({"display":"none"});
                                              $('#customerpass')[0].reset();
                                            }, 3000);
                                        }
                                    },
                                    error: function(event){}
                                });
                            }
                            else
                            {
                                    $("#error_pass").html('<div role="alert"  class="alert alert-danger" >Password and Confirm Password Mismatched</div>');
                                            setTimeout(function()
                                            {
                                              $('#error_pass').html('');
                                            }, 3000);
                            }
                        });
                         //Phone Info Change
                        $(document).on("submit","#customerphone",function (event)
                        {
                            event.preventDefault();
                            var data = $("#customerphone").serialize();

                            $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editcustomer",
                                data:data,
                                success: function(result){
                                    $("#ci_msg2").css({"display":"block"});
                                     setTimeout(function()
                                            {
                                             $("#ci_msg2").css({"display":"none"});
                                            }, 3000);
                                },
                                error: function(event){}
                            });

                       });


    //Credit Card validation
    $('#customerPayment').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                a_card_no: {
                    validators: {
                        creditCard: {
                            message: 'The credit card number is not valid'
                        }
                    }
                },
                a_cvv: {
                    validators: {
                        cvv: {
                            creditCardField: 'a_card_no',
                            message: 'The CVV number is not valid'
                        }
                    }
                }

            }
        })
        .on('success.validator.fv', function(e, data) {
            if (data.field === 'a_card_no' && data.validator === 'creditCard') {
                var $icon = data.element.data('fv.icon');
                // data.result.type can be one of
                // AMERICAN_EXPRESS, DINERS_CLUB, DINERS_CLUB_US, DISCOVER, JCB, LASER,
                // MAESTRO, MASTERCARD, SOLO, UNIONPAY, VISA

                switch (data.result.type) {
                    case 'AMERICAN_EXPRESS':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-amex');
                        break;

                    case 'DISCOVER':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-discover');
                        break;

                    case 'MASTERCARD':
                    case 'DINERS_CLUB_US':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-mastercard');
                        break;

                    case 'VISA':
                        $icon.removeClass().addClass('form-control-feedback fa fa-cc-visa');
                        break;

                    default:
                        $icon.removeClass().addClass('form-control-feedback fa fa-credit-card');
                        break;
                }
            }
        })
        .on('err.field.fv', function(e, data) {
            if (data.field === 'a_card_no') {
                var $icon = data.element.data('fv.icon');
                $icon.removeClass().addClass('form-control-feedback fa fa-times');
            }
        });

                    //Payment Info Change
                    $("#customerPayment").on("submit",function (event){
                        event.preventDefault();
                        var data = $("#customerPayment").serialize();
                        $.ajax({
                            type:"POST",
                            url:"<?php echo $base_url_temp; ?>"+"user_dashboard/editpayment",
                            data:data,
                            success: function(result){
                                $("#ci_msg4").css({"display":"block"});
                                     setTimeout(function()
                                            {
                                             $("#ci_msg4").css({"display":"none"});
                                            }, 3000);
                            },
                            error: function(e){}
                        });

                    });


$('.help-block').remove();

   });
</script>  

<!-- new jquery add -->

   <script>
    $(function(){
        $('.parascroll').parascroll();
    })
</script>
<!-- write script to toggle class on scroll -->
<script>
$(window).scroll(function() {
    if ($(this).scrollTop() > 20){  
        $('.navbar-fixed-top').addClass("sticky");
    }
    else{
        $('.navbar-fixed-top').removeClass("sticky");
    }
});

//this is where we apply opacity to the arrow
$(window).scroll( function(){

  //get scroll position
  var topWindow = $(window).scrollTop();
  //multipl by 1.5 so the arrow will become transparent half-way up the page
  var topWindow = topWindow * 1.5;
  
  //get height of window
  var windowHeight = $(window).height();
      
  //set position as percentage of how far the user has scrolled 
  var position = topWindow / windowHeight;
  //invert the percentage
  position = 1 - position;

  //define arrow opacity as based on how far up the page the user has scrolled
  //no scrolling = 1, half-way up the page = 0
  $('.arrow-wrap').css('opacity', position);

});




//Code stolen from css-tricks for smooth scrolling:
$(function() {
  $('.circle_mat a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>