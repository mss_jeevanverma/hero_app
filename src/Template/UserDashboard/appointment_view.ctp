<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
require_once(ROOT. DS . 'src/Controller/braintree/braintree_php/lib/autoload.php');
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Braintree\TransparentRedirect;
use Braintree\Transaction;

?>
<?php



 $user_info = $this->request->session()->read('UserInfo');
    $appointment_data = @$data["appointment_data"];
    $hero_data = @$data["hero_data"];
     $user_data = @$data["user_data"];
     $tr_data1 = @$data["tr_data1"];
		$tr_data = @$data["tr_data"];
		$status = @$data["status"];
     if (count($appointment_data)){?>

<section class="appointment">
	<div class="container">
    	<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="manage-heros">
										
					<div class="appointment-content">
						<div class="row">
							
							
							<div class="col-sm-12"><h2>Appointment Information</h2></div>
						</div>
						
						
						
						<div class="row">
							<div  class="col-lg-12">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									

									  <!-- Table -->
									  <table class="table">
										<thead>
										  <tr>
											<th>Order Id </th>
											<th> Order Detail </th>
											<th>Order Status  </th>
											<th> Time and Date </th>
										  </tr>
										</thead>
										<tbody>
										  <tr>
											<td><?php echo $appointment_data[0]->id; ?></td>
											<td><?php $order_data=$appointment_data[0]->order_data;
											 $order_content=json_decode($order_data,2);
											 		echo $order_data_all=$order_content['stepOne'].'-'.$order_content['stepTwo'];

											?>
										<p><?php echo  $order_data=$order_content['stepThree']; ?></p></td>
											<td><?php 
												$order_date=$appointment_data[0]->appointment_date;
					                            $appoint_date=explode(" ",$order_date);
					                            $yrdata= strtotime($appoint_date['0']);
					                            $app_date=date('M d, Y', $yrdata);
					                            $time_data=strtotime($appoint_date['1']); 
												$app_time=date('H:i:s A', $time_data);
												//Calculate difference
					                            $diff=$yrdata-time();//time returns current time in seconds
					                            $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)                 
					                           if($appointment_data[0]->status!=1)
					                           {
												if($days<-1)
						                            {
						                             echo '<h4>Appointment Date Passed</h4>';   
						                            }
												else if($appointment_data[0]->status==0)
													{
														echo ' Upcoming ';

													} 
													}
													else
													{
														echo 'Completed';
													}
													?>
												</td>
											<td><?php 
					                            echo '<p>'.$app_date.' '.$app_time. '</p>';
                            				?></td>
										  </tr>
										</tbody>
									  </table>
									</div>
								  </div>
							</div>						
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									<div class="panel-heading"><h3>Hero Information</h3></div>
										<?php if(count($hero_data)){ ?>
												  <!-- Table -->
												  <table class="table hero_info">
													<tbody>
													  <tr>
														<th style="width:90px;">Name</th>
														<td><?php echo $hero_data[0]->first_name; ?> </td>
													  </tr>
													  <tr>
														<th style="width:90px;">Email</th>
														<td><?php echo $hero_data[0]->email; ?> </td>
													  </tr>
													  <tr>
														<th style="width:90px;">Phone no.</th>
														<td><?php echo $hero_data[0]->phone_no; ?> </td>
													  </tr>
													  
													</tbody>
												  </table>
									 	<?php }
									 else{
												echo '<h1>No Hero Assigend Yet</h1>';
									 }?>
									</div>
								  </div>
							</div>
							
							<div class="col-sm-6">
								<div class="manage-heros-table">
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									<div class="panel-heading"><h3>Your Payment Info</h3></div>
									                                  
                                   	<div id="ci_msg" class="alert alert-success" style="display:none;">
                            Your Hour information changed successfully.
                        </div>
                      
                        <div class="panel-heading update" > 
                        	<span id="update_hour">

                        	<?php if($appointment_data[0]->add_hour)
											{ 
												$total_hour=$appointment_data[0]->add_hour;
												$total_pay=$total_hour*79;
											    echo 'Current Hour is :'.$total_hour.' Hours &#10005; $79 = $'.$total_pay;

											}
											else{
													echo '<h5>No Hour Add Yet</h5>';
										        }
										        ?>
							 </span>
									        <!--span class="panel-heading" id="update_result"></span-->
									    </div>
                                <!--input type="text"  pattern="[0-9]*" userid="<?php echo $appointment_data[0]->id; ?>" name="add_hour" maxlength="2" minlength="1" class="form-control input-lg" title="Add Only Numeric Value" placeholder="Add Hours" tabindex="1" value=""  required -->
											<?php if($appointment_data[0]->add_hour)
											{ 											 

												?>
											<div  class="asng-hero">
												<div id="wrap">
						     <?php if ($status):?>
						            <div class="status"><?= $status?></div>
						        <?php endif;?>
						       <form method="post" action="<?=TransparentRedirect::url()?>" autocomplete="off">

									<input type="hidden" name="transaction[customer][first_name]" value="<?php echo @$user_data[0]->first_name; ?>">

						             <input type="hidden" name="transaction[customer][last_name]" value="<?php echo @$user_data[0]->last_name; ?>">
									<input type="hidden" name="transaction[credit_card][cardholder_name]" value="<?php echo @$user_data[0]->first_name.' '.@$user_data[0]->last_name; ?>">
						             <input type="hidden" name="transaction[customer][email]" value="<?php echo @$user_data[0]->email; ?>">
						             <input type="hidden" name="transaction[credit_card][number]" value="<?php echo @$user_data[0]->a_card_no; ?>">
							      	<input type="hidden" name="transaction[credit_card][cvv]" value="<?php echo @$user_data[0]->a_cvv; ?>" >
							        <input type="hidden" name="transaction[credit_card][expiration_date]" value="<?php echo @$user_data[0]->a_exp_date; ?>" >
							       <input type="hidden" name="tr_data" value="<?=$tr_data?>">
							       <?php if($appointment_data[0]->status==1){?>
							       	<div class="status">
							       		Payment Completed
							       </div>
							   		<?php } 
							   		else{?>
							   		<button class="btn btn-primary action-button hero-creat"  id="pay_now" type="submit">Pay Now</button> 

							   	<?php }?>
								</form>
								
									</div> 
								
									</div> 
									<?php }?>
									</div>
								  </div>
							</div>
						</div>
						
						<?php if($appointment_data[0]->status==1){ ?>
						
						<div class="row">
							<div class="col-lg-12 paid-amount">
								<h3>Payments </h3>
								<p>Amount Payed: <span>$ <?php echo $total_pay; ?></span></p>
							</div>						
						</div>
						<?php }?>


					</div>
						
					</div>
					
					
				</div>
			</div>
		</div>
    </div>
</section>
 <?php }
									 else{
echo '<h1>Appointment Data Not Found</h1>';
									 }?>

	<script type="text/javascript">
		$(document).ready(function(){
            
					//Hour  Info Change
                        $(document).on("submit","#add_hour",function (event){
                            event.preventDefault();
                            var add_hour=$("#add_hour").serialize();
                            	var id='<?php echo $appointment_data[0]->id; ?>';
                            	//alert(add_hour);
                              $.ajax({
                                type:"POST",
                                url:"<?php echo $base_url_temp; ?>"+"user_dashboard/add_hour",
                                data:add_hour+'&id='+id,
                                success: function(result){
                                	 //$("#update_hour").css({"display":"none"});
                                	$("#update_hour").html($("input").val());
                                    $("#ci_msg").css({"display":"block"});
                                    $('#add_hour')[0].reset();
                                    setTimeout(function(){        	
                                              	
                                                $("#ci_msg").css({"display":"none"});          
                                              	
                                            }, 2000);
                                },
                                error: function(event){}
                            });
                        });
 					 $("#pay_now").click(function (event){
				       var conf=  confirm('Are you sure you want to pay $<?php echo $total_pay; ?> with credit card number <?php $card_no=@$user_data[0]->a_card_no; 
				       		echo '************'.substr($card_no,-4)
				       	 ?> ?');
				        if(!conf)
				        { 
				        return false; 
				    	}

						});
});
	</script>

