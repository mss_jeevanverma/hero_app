<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdminDashboardController extends AppController
{

    public function initialize()
    {
        $this->layout = 'admin';
        $session = $this->request->session();
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            if($user_info['role']!=1){ $this->redirect(array("controller" => "Index", "action" => "index")); }

        }else{
             $this->redirect(array("controller" => "Index", "action" => "index")); 
        }
               
    }

    # @VIKRANT CODE FOR HERO MANAGEMENT START

    #Get Heros 
    public function index(){
        try{
            $this->loadModel('Users');
            if($this->request->is('post')){
                $post_data = $this->request->data; 
                $keyword = $post_data["keyword"]; 
                $this->paginate = array();
            }else{ $keyword = ""; }

            $this->paginate = array( 
                'conditions' => array('Users.role =' => 3,'Users.del_status =' => 0,'OR' => array('Users.email LIKE' => "%$keyword%")),
                'limit' => 20,
                'order' => array('id' => 'desc'),
            );

            $heros = $this->paginate('Users')->toArray();
            //$heros = $this->Users->find('all')->where(['role =' => 3])->toArray();
            $data = array ("heros"=>$heros);
            $this->set('data', $data);

        } catch (NotFoundException  $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n"; die;
        }

    }

    #Create Heros 
    public function createhero(){
        $this->loadModel('Users');

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data["email"]])->toArray(); 
            if(!count($email))
            {
            $post_data["password"] = md5($post_data["password"]);
            $post_data["status"] = 1;
            $heros = TableRegistry::get('Users');
            $query = $heros->query();
            $query->insert(['first_name','last_name','phone_no','email','password','role','status'])
                  ->values($post_data)
                  ->execute();
            $this->redirect(array("controller" => "admin_dashboard",
            "action"=>"createhero", 
            "val"=>"create")); 
            }
            else{
                $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"createhero",
              "val"=>"error",
              'f'=>$post_data['first_name'],
              'l'=>$post_data['last_name'],
              'p'=>$post_data['phone_no'])); 
            }

        }        
    }


    #Edit Heros 
    public function edithero(){
        $this->loadModel('Users');
        $heros = array();

        if($this->request->is('post')){
            $edit_id = $this->request->query['id'];
            $heros = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();

            $post_data = $this->request->data;
            if($post_data["password"]){ $post_data["password"] = md5($post_data["password"]); }
            $post_data = array_filter($post_data); // used if password is not set
            if(!isset($post_data["status"])) {$post_data["status"]=0;} // array_filter removes status field
            unset($post_data["id"]);
           
           $email = $this->Users->find('all')->where(['email =' => $post_data["email"],['id !=' => $edit_id]])->toArray(); 
            if(count($email))
            {
                $this->redirect(array("controller" => "admin_dashboard", 
                "action" => "edithero",
                "id" => $edit_id,
                "val" => "email_error")); 
            }
            else
            {
            $this->Users->updateAll( $post_data,  array('id' => $edit_id ));
            $this->redirect(array("controller" => "admin_dashboard", 
                "action" => "edithero",
                "id" => $edit_id,
                "val" => "update")); 
            }
           
        }

        if($this->request->is('get')){
            $edit_id = $this->request->query['id'];
            @$del = $this->request->query['del'];
            @$status = $this->request->query['status'];

            
            # Activate/Deactivate Hero
            if($status=="0" || $status=="1"){ 
                if($status) {$status_arr = array("status"=>0);} 
                else {
			$status_arr = array("status"=>1);
			//** Sending Email For New Password When Activated **
			$email = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();
            
			if(count($email)){
			$id=$email[0]->id;
			$name=$email[0]->first_name;
			$rand_url=rand(100,900);
			$rand_url2=rand(100,900);
			$code=$rand_url.$id.$rand_url2;
			$p_r_code=array('p_r_code'=>$code);
			$this->Users->updateAll( $p_r_code,  array('id' => $id)); 

			$code_vars = array('code' => $code,'name'=>$name);
            $this->sendemailglobal(
                $email[0]->email,
                'forgot_pass',
                $code_vars,
                'html',
                'Password Reset');



			    $msg="Email Successfully Send";
			

			}
			//** Sending Email For New Password When Activated **
		     }
                $this->Users->updateAll( $status_arr,  array('id' => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard")); 
            }

            # delete Hero
            if(@$del=="yes"){
                $this->Users->updateAll( array('status ='=>0,'del_status ='=>1),  array('id' => $edit_id )); 
                //$this->Users->deleteAll(array("Users.id" => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard")); 
            }

            $heros = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();
        }

        $data = array ("heros"=>$heros);
        $this->set('data', $data);        
    }

    # @VIKRANT CODE FOR HERO MANAGEMENT END

    # @VIKRANT CODE FOR CUSTOMERS MANAGEMENT START

    #Get Customer 
    public function getcustomer(){
        try{

            $this->loadModel('Users');
            if($this->request->is('post')){
                $post_data = $this->request->data; 
                $keyword = $post_data["keyword"]; 
                $this->paginate = array();
            }else{ $keyword = ""; }

            $this->paginate = array( 
                'conditions' => array('Users.role =' => 2,'Users.del_status =' => 0,'OR' => array('Users.email LIKE' => "%$keyword%")),
                'limit' => 20,
                'order' => array('id' => 'desc'),
            );
            $customers = $this->paginate('Users')->toArray(); 
            //$customers = $this->Users->find('all')->where(['role =' => 2])->toArray();
            $data = array ("customers"=>$customers);
            $this->set('data', $data);

        } catch (NotFoundException  $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n"; die;
        }

    }

    #Create Customer 
    public function createcustomer(){
        $this->loadModel('Users');

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $this->loadModel('Users');
            $email = $this->Users->find('all')->where(['email =' => $post_data["email"]])->toArray(); 
            if(!count($email))
            {
            $post_data["password"] = md5($post_data["password"]);
            $customers = TableRegistry::get('Users');
            $query = $customers->query();
            $query->insert(['first_name','last_name','phone_no','email','password','role','status'])
                  ->values($post_data)
                  ->execute();
            $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"createcustomer",
              "val"=>"create")); 
            }
            else{
                $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"createcustomer",
              "val"=>"error",
              'f'=>$post_data['first_name'],
              'l'=>$post_data['last_name'],
              'p'=>$post_data['phone_no'])); 
            }

        }        
    }


    #Edit Customer 
    public function editcustomer(){
        $this->loadModel('Users');
        $customers = array();

        if($this->request->is('post')){
            $edit_id = $this->request->query['id'];
            $customers = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();

            $post_data = $this->request->data;
            if($post_data["password"]){ $post_data["password"] = md5($post_data["password"]); }
            $post_data = array_filter($post_data); // used if password is not set
            if(!isset($post_data["status"])) {$post_data["status"]=0;} // array_filter removes status field
            unset($post_data["id"]);
            $email = $this->Users->find('all')->where(['email =' => $post_data["email"],['id !=' => $edit_id]])->toArray(); 
                if(count($email))
                {
                    $this->redirect(array("controller" => "admin_dashboard", 
                    "action" => "editcustomer",
                    "id" => $edit_id,
                    "val" => "email_error")); 
                }
                else
                {
                $this->Users->updateAll( $post_data,  array('id' => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard", 
                    "action" => "editcustomer",
                    "id" => $edit_id,
                    "val" => "update")); 
                }
            }

        if($this->request->is('get')){
            $edit_id = $this->request->query['id'];
            @$del = $this->request->query['del'];
            @$status = $this->request->query['status'];

            # Activate/Deactivate Customer
            if($status=="0" || $status=="1"){ 
                if($status) {$status_arr = array("status"=>0);} 
                else {$status_arr = array("status"=>1);}
                $this->Users->updateAll( $status_arr,  array('id' => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard",'action'=>'getcustomer')); 
            }

            # delete Customer
            if(@$del=="yes"){
                $this->Users->updateAll( array('status ='=>0,'del_status ='=>1),  array('id' => $edit_id ));  
                //$this->Users->deleteAll(array("Users.id" => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard",'action'=>'getcustomer')); 
            }

            $customers = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();
        }

        $data = array ("customers"=>$customers);
        $this->set('data', $data);        
    }

    # @VIKRANT CODE FOR CUSTOMERS MANAGEMENT END


    # @VIKRANT CODE FOR Appointment Start

    #Get appointments 
    public function getappointments(){
        try{

            $this->loadModel('Orders');
            $this->loadModel('Users');
            if($this->request->is('post')){
                $post_data = $this->request->data; 
                $keyword = $post_data["keyword"]; 
                $this->paginate = array();
            }else{ $keyword = ""; }

            $this->paginate = array( 
                'conditions' => array('Orders.order_data LIKE' => "%$keyword%"),
                'limit' => 20,
                'order' => array('id' => 'desc'),
            );
            $appointments = $this->paginate('Orders')->toArray(); 
            $data = array ("appointments"=>$appointments);
            $this->set('data', $data);

        } catch (NotFoundException  $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n"; die;
        }
        

    }



    #Edit view 
    public function viewappointment(){
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $appointment_data = array(); 
        $user_data = array(); 
        $hero_data = array(); 
        $current_hero_data = array();
         if($this->request->is('get')){
            $order_id = $this->request->query['id'];
            $appointment_data = $this->Orders->find('all')->where(['id =' => $order_id])->toArray();
            if(count($appointment_data)){
                $user_id = $appointment_data[0]->user_id;
                $hero_id = $appointment_data[0]->hero_id;

                $user_data = $this->Users->find('all')->where(['id =' => $user_id])->toArray();
                $current_hero_data = $this->Users->find('all')->where(['id =' => $hero_id])->toArray();
            }
        }
        $hero_data = $this->Users->find('all')->where(['role =' => 3,'status =' => 1])->toArray();
        
     
        //echo "<pre>"; print_r($current_hero_data); die;
        if($this->request->is('post')){
            $edit_id = $this->request->query['id'];
            $customers = $this->Users->find('all')->where(['id =' => $edit_id])->toArray();

            $post_data = $this->request->data;
            if($post_data["password"]){ $post_data["password"] = md5($post_data["password"]); }
            $post_data = array_filter($post_data); // used if password is not set
            if(!isset($post_data["status"])) {$post_data["status"]=0;} // array_filter removes status field
            unset($post_data["id"]);
            $this->Users->updateAll( $post_data,  array('id' => $edit_id ));
            $this->redirect(array("controller" => "admin_dashboard", 
                "action" => "edithero",
                "id" => $edit_id,
                "val" => "update")); 
           
        }

       

        $data = array (
            'appointment_data'=>$appointment_data[0],
            'user_data'=>$user_data[0],
            'hero_data'=>$hero_data,
            'current_hero_data'=>@$current_hero_data[0]
            );
        $this->set('data', $data);        
    }

    # Get Hero Data using Hero Id "ON Change of Select Box"
    public function getcurhero(){

        $this->loadModel('Users');
        if($this->request->is('post')){
            $post_data = $this->request->data;
            $hero_id = $post_data['hero_id'];
            $hero_data = $this->Users->find('all')->where(['id =' => $hero_id])->toArray();
            
            $hero_data_arr = array(
                'first_name' => $hero_data[0]->first_name,
                'last_name' => $hero_data[0]->last_name,
                'email' => $hero_data[0]->email,
                'phone_no' => $hero_data[0]->phone_no,
                );
            print_r(json_encode($hero_data_arr));
        }
        die();
    }

    # setting the hero to current Order/Appointment
    public function setcurhero(){

        $this->loadModel('Orders');
        $this->loadModel('Users');
        $appointment_data = array(); 
        $user_data = array(); 
        $hero_data = array(); 
        $current_hero_data = array();

            if($this->request->is('post')){
            $post_data = $this->request->data;
            $hero_id = $post_data['hero_id'];
            $order_id = $post_data['order_id'];           
            $appointment_data = $this->Orders->find('all')->where(['id =' => $order_id])->toArray();
            if(count($appointment_data)){
                $user_id = $appointment_data[0]->user_id;
                $user_data = $this->Users->find('all')->where(['id =' => $user_id])->toArray();
                $current_hero_data = $this->Users->find('all')->where(['id =' => $hero_id,'role'=>3])->toArray();
            }
        
        
             $data_cust = array (
            'appointment_data'=>$appointment_data[0],
            'user_data'=>$user_data[0]
                        );

              $data_hero = array (
            'appointment_data'=>$appointment_data[0],
            'current_hero_data'=>@$current_hero_data[0]
            );

            $hero_vars = array('data_cust'=>$data_cust,'hero_name'=>$current_hero_data[0]->first_name);
            $this->sendemailglobal(
                $current_hero_data[0]->email,
                'assign_hero_to_cust',
                $hero_vars,
                'html',
                'New Appointment Assigned to You');

             $user_vars = array('data_hero'=>$data_hero,'customer_name'=>$user_data[0]->first_name);
             $this->sendemailglobal(
                $user_data[0]->email,
                'assign_cust_to_hero',
                $user_vars,
                'html',
                'Hero Assigned to Your Appointment');

            $this->Orders->updateAll( array('hero_id'=>$hero_id ),  array('id' => $order_id ));
            echo "done";

        }

        die();
    }

    # Unsetting/Canceling the Hero from Current Appointment 
    public function cancelcurhero(){

        $this->loadModel('Orders');
	    $this->loadModel('Users');
        if($this->request->is('post')){
            $post_data = $this->request->data;
            $hero_id = 0;
            $order_id = $post_data['order_id'];
            
	    $appointment_data = $this->Orders->find('all')->where(['id =' => $order_id])->toArray();
	    $old_hero_id = $appointment_data[0]->hero_id;
	    $this->Orders->updateAll( array('hero_id'=>$hero_id,'add_hour'=>0 ),  array('id' => $order_id ));
	    if(count($appointment_data)){
                $user_id = $appointment_data[0]->user_id;
                $user_data = $this->Users->find('all')->where(['id =' => $user_id])->toArray();
                $current_hero_data = $this->Users->find('all')->where(['id =' => $old_hero_id])->toArray();
            }

	    $data_cust = array (
            'appointment_data'=>$appointment_data[0],
            'user_data'=>$user_data[0]
                        );

              $data_hero = array (
            'appointment_data'=>$appointment_data[0],
            'current_hero_data'=>@$current_hero_data[0]
            );

             $can_vars = array('data_hero'=>$data_hero);
             $this->sendemailglobal(
                $current_hero_data[0]->email,
                'cancel_cust_for_hero',
                $can_vars,
                'html',
                'Appointment Canceled');

             $her_vars = array('data_cust'=>$data_cust);
             $this->sendemailglobal(
                $user_data[0]->email,
                'cancel_hero_for_cust',
                $her_vars,
                'html',
                'Assigned Hero Canceled');

            echo "done";

        }
        die();
    }
 
    #for statics code
    public function statics()
    {
        
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $total_users=$this->Users->find('all')->where(['role'=>2])->toArray();
        $total_act_users=$this->Users->find('all')->where(['status'=>1,'role'=>2])->toArray();
        $total_hero=$this->Users->find('all')->where(['role'=>3])->toArray();
        $total_act_hero=$this->Users->find('all')->where(['status'=>1,'role'=>3])->toArray();

        $total_user_result=count($total_users);
        $total_act_result=count($total_act_users);
        $total_inact_result=$total_user_result-$total_act_result;
        $total_hero_result=count($total_hero);
        $total_hero_act_result=count($total_act_hero);
        $total_hero_inact_result=$total_hero_result-$total_hero_act_result;
        $total_appointment=$this->Orders->find('all')->toArray();
        $total_uppcoming_appo=$this->Orders->find('all')->where(['status'=>0])->toArray();
        $total_complete_appo=$this->Orders->find('all')->where(['status'=>1])->toArray();
        $total_cancel_appo=$this->Orders->find('all')->where(['status'=>2])->toArray();
        $total_appointment_res=count($total_appointment);
        $total_uppcoming_res=count($total_uppcoming_appo);
        $total_complete_res=count($total_complete_appo);
        $total_cancel_res=count($total_cancel_appo);
        $data=array(
            'total_user_result' => $total_user_result,
            'total_act_result' => $total_act_result,
            'total_inact_result' => $total_inact_result,
            'total_hero_result' => $total_hero_result,
            'total_hero_act_result' => $total_hero_act_result,
            'total_hero_inact_result' => $total_hero_inact_result,
            'total_appointment_res' => $total_appointment_res,
            'total_uppcoming_res' => $total_uppcoming_res,
            'total_complete_res' => $total_complete_res,
            'total_cancel_res'=>$total_cancel_res
            
                );
        $this->set('data', $data); 
    }

             #View pages
    public function get_pages()
    {

        try{

            $this->loadModel('Pages');
            if($this->request->is('post'))
            {
                $post_data = $this->request->data;
                $keyword=$post_data["keyword"];
                $this->paginate=array();
            }
            else
            {
                $keyword="";
            }
            $this->paginate=array(
                'condition'=>array('Pages.title LIKE' =>"%$keyword%"),
                'limit' =>20,
                'order' =>array('id' =>'desc')
            );               
            $page_data=$this->paginate('Pages')->toArray();
            $data=array("page_data" => $page_data);
            $this->set('data' ,$data);
        }
        catch (NotFoundException  $e) {

        }
    }


  #Create Pages
  # TODO - make url_title unique 
    public function createpages()
    {
        $this->loadModel('Pages');
            if($this->request->is('post'))
            {
            $post_data = $this->request->data;
             $post_data['url_title'] = preg_replace('#[ -]+#', '-', strtolower($post_data['title']));
             $title = $this->Pages->find('all')->where(['url_title =' => $post_data["url_title"]])->toArray(); 
             if(!count($title))
            {
           
            $page_count = count($this->Pages->find('all')->where(['status =' => 1])->toArray());
            
            $post_data['sorting']=$page_count+1;
            $pages = TableRegistry::get('Pages');
            $query = $pages->query();
            $query->insert(['title','url_title','description','sorting','status'])
                  ->values($post_data)
                  ->execute();
            $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"createpages",
              "val"=>"create")); 
           }
           else
           {
             $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"createpages",
              "val"=>"error")); 
           }
            }
  
    }

     #Edit Customer 
    public function editpages(){
        $this->loadModel('Pages');
        $pages = array();

        if($this->request->is('post')){
            $edit_id = $this->request->query['id'];
            $pages = $this->Pages->find('all')->where(['id =' => $edit_id])->toArray();
            
            $post_data = $this->request->data;
            $post_data = array_filter($post_data); // used if password is not set
            if(!isset($post_data["status"])) {$post_data["status"]=0;} // array_filter removes status field
            unset($post_data["id"]);
            $post_data['url_title'] = preg_replace('#[ -]+#', '-', strtolower($post_data['title']));
             $title_data = $this->Pages->find('all')->where(array('id !=' => $edit_id))->where(array('url_title =' => $post_data['url_title']))->toArray(); 
            if(count($title_data))
             {
               $this->redirect(array("controller" => "admin_dashboard",
             "action"=>"editpages",
              "id" => $edit_id,
              "val"=>"error")); 
            }
            else
           {
            
                $this->Pages->updateAll( $post_data,  array('id' => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard", 
                    "action" => "editpages",
                    "id" => $edit_id,
                    "val" => "update")); 
           }
                
            }

        if($this->request->is('get')){
            $edit_id = $this->request->query['id'];
            @$del = $this->request->query['del'];
            @$status = $this->request->query['status'];

            # Activate/Deactivate Customer
            if($status=="0" || $status=="1"){ 
                if($status) {$status_arr = array("status"=>0);} 
                else {$status_arr = array("status"=>1);}
                $this->Pages->updateAll( $status_arr,  array('id' => $edit_id ));
                $this->redirect(array("controller" => "admin_dashboard",'action'=>'get_pages')); 
            }

            # delete Customer
            if(@$del=="yes"){
                $this->Pages->updateAll( array('status ='=>0,'del_status ='=>1),  array('id' => $edit_id));  
                $this->redirect(array("controller" => "admin_dashboard",'action'=>'get_pages')); 
            }
            $sort = $this->Pages->find('all')->where(['status =' => 1])->toArray();
           $pages = $this->Pages->find('all')->where(['id =' => $edit_id])->toArray();
        }

        $data = array ("pages"=>$pages,"total_sort"=>@$sort);
        $this->set('data', $data);        
    }




    
}
