<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class IndexController extends AppController
{
    public function initialize()
    {
        $this->layout = 'default';
        session_start();
        $_SESSION["sesval"] = 0;
    }

    public function index(){
        $session = $this->request->session();
        $this->loadModel('Testimonials');
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            
            // if Admin login -> no access to home
            if ($user_info['role']==1) {
                $this->redirect(array("controller" => "AdminDashboard", "action" => "index"));
            }
            // if Hero login -> no access to home
            if ($user_info['role']==3) {
                $this->redirect(array("controller" => "HeroDashboard", "action" => "index"));
            }

        }else{
            $user_info = array();
        }
        $testimonials = $this->Testimonials->find('all');
        $main_arr = array("userinfo"=>$user_info,"testimonials"=>$testimonials);
        $this->set('data', $main_arr);
        
    }
    public function applicants(){
        
        if($this->request->is('post')){          
            $post_data = $this->request->data;
            $applicants = TableRegistry::get('Applicants');
            $query = $applicants->query();
            $query->insert(['name','email','phone_no','comments'])
                  ->values(['name'=>$post_data['name'],'email'=>$post_data['email'],'phone_no'=>$post_data['phone_no'],'comments'=>$post_data['comments']])
                  ->execute();
            echo "<h2> Thanks for your interest. We'll be in touch soon! </h2>";
        }
        die;
    }

    public function wizardStepOne(){
        $this->loadModel('Categories');
        $parent_categories = $this->Categories->find('all')->where(['parent_id =' => 0])->toArray();
        
        die;
    }
      #forgot Password
    public function forgot_pass()
    { 
        $this->loadModel('Users');
        $session = $this->request->session();
        if($this->request->is('get'))
        {
                $url_code= $this->request->query('code');
                if($url_code)
                    {
                        $link_code = $this->Users->find('all')->where(['p_r_code' => $url_code])->toArray();
                                if(!count($link_code))
                                    {
                                   
                                    echo '<h2>This Link has Expired</h2>';
                                    die();
                                    }
                                }
                                else{
                                    echo '<h2>Not a Valid Url</h2>';
                                    die();
                                    }
                    }
                    if($this->request->is('post'))
                    {
                            $post_data = $this->request->data;

                            $str2 = substr($post_data['code'], 3);
                            $url_id=substr_replace($str2 ,"",-3);

                            $new_pass = md5($post_data['new_password']);
                            $new_password=array('password'=>$new_pass);
                            $this->Users->updateAll( $new_password,  array('id' => $url_id )); 
                            $this->Users->updateAll( array('p_r_code'=>rand(100,999).'NA'.rand(100,999)),  array('id' => $url_id )); 
                                 $email = $this->Users->find('all')->where(['id' => $url_id])->toArray();
                                  if(!isset($_SESSION['login_popup'])){

                                    $_SESSION['login_popup'] = 'yes';
                                }
                        
                                // if user loges in from fb and Steps data is in sesstion
                                // if(isset($_SESSION['steps'])){

                                //     $_SESSION['api_login'] = 'yes';
                                // }
                                        die("Password change successfully");
                    }   

       
    }

    public function beahero(){
        $this->loadModel('Users');
        if($this->request->is('post'))
        {
            $post_data = $this->request->data;
            $email = $post_data['email'];
            $email_check = $this->Users->find('all')->where(['email =' => $email])->toArray();
            if(!count($email_check)){

                $usersTable = TableRegistry::get('Users');
                $User = $usersTable->newEntity();
                $User->status = 0;
                $User->first_name = $post_data['first_name'];
                $User->last_name = $post_data['last_name'];
                $User->email = $post_data['email'];
                $User->phone_no = $post_data['phone_no'];
                $User->a_postal_code = $post_data['a_postal_code'];
                $User->role = $post_data['role'];


                if ($usersTable->save($User)) {

                    print_r(json_encode(array("msg"=>"Your Application Has Been Submited.","status"=>"success")));
                }else{

                    print_r(json_encode(array("msg"=>"Something went Wrong try again later","status"=>"error")));
                }
                die();
            }else{
                print_r(json_encode(array("msg"=>"User Already Exists.","status"=>"error")));
            }
            die();
        }

    }

    // Static Footer page Help
    public function Page($id){
        
    }    

    // Static Footer page Help
    public function notfound(){
        
    }

 


}
