<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HeroDashboardController extends AppController
{

    public function initialize()
    {
        $this->layout = 'hero_account';
        $session = $this->request->session();
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            if($user_info['role']!=3){ $this->redirect(array("controller" => "Index", "action" => "index")); }

        }else{
             $this->redirect(array("controller" => "Index", "action" => "index")); 
        }
        
    }

    #Login the user 
    public function index(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $user_info = $session->read('UserInfo');
        $hero = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray();       
        $data = array ("hero"=>$hero);
        $this->set('data', $data);     
       
    }

      #Edit hero  Info
    public function edithero()
       { 
                $this->loadModel('Users');
                $session = $this->request->session();
                if($this->request->is('post'))
                {
                    $post_data = $this->request->data;
                    $user_info = $session->read('UserInfo');
                            $post_data = array_filter($post_data); // used if password is not set
                            if(!isset($post_data['phone_text']))
                            {
                            $post_data['phone_text']=0;
                            }

                    $this->Users->updateAll( $post_data,  array('email' => $user_info['email'] ));
                    die("Your information changed successfully.");
                }
        }

            #Edit Hero  Password
    public function editpassword(){ 
        $this->loadModel('Users');
        $session = $this->request->session();
      
          if($this->request->is('post')){
            $post_data = $this->request->data;
            $old_pass = md5($post_data['password']);
            $new_pass = md5($post_data['new_password']);
            $user_info = $session->read('UserInfo');
            $customer = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray();       
            $data_pass=$customer[0]->password;
            if($old_pass==$data_pass)
            {
                $new_password=array('password'=>$new_pass);
               $this->Users->updateAll( $new_password,  array('email' => $user_info['email'] )); 
             die("Your information changed successfully.");
            }
            else
            {
                $msg="Current Password is wrong";
                die('Current Password is wrong');
            }
          }

       
        }


    # Hero Appointment Info
    public function hero_info()
       { 
            $this->loadModel('Orders');
            $this->loadModel('Users');
            $session = $this->request->session();
            $user_info = $session->read('UserInfo');
            $hero = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray(); 
            $id=$hero[0]->id;
            $appointment_data = $this->Orders->find('all',['order' => ['Orders.id' => 'DESC']])->where(['hero_id ' =>$id,'status' =>0 ])->toArray();
            $data = array ("appointment_data"=>$appointment_data);
            $this->set('data', $data);
            $appointment_data1 = $this->Orders->find('all')->where(['hero_id ' =>$id,'status' =>1 ])->toArray();
            $data1 = array ("appointment_data1"=>$appointment_data1);
            $this->set('data1', $data1);

        }


           # Hero Appointment View
    public function hero_view()
       { 
           $this->loadModel('Orders');
            $this->loadModel('Users');
            $hero_data=array();
            $session = $this->request->session();
            $user_info = $session->read('UserInfo');
            $appointment_id=$this->request->query['id'];
            $appointment_data = $this->Orders->find('all')->where(['id ' =>$appointment_id])->where(['status !=' => 2])->toArray();
            if(count($appointment_data))
            {
                $user_id=@$appointment_data[0]->user_id;
                $hero_id=@$appointment_data[0]->hero_id;
                $customer_data = $this->Users->find('all')->where(['id ' =>$user_id])->toArray();
                $hero_data = $this->Users->find('all')->where(['id ' =>$hero_id])->toArray();
            }
            $data = array (
                "appointment_data"=>$appointment_data,
                "customer_data"=>$customer_data,
                "hero_data"=>$hero_data
                );
            $this->set('data', $data);
        }
     # Cancel Appointment   
    public function cancel_appointment()
        {
                $this->loadModel('Orders');
                $session = $this->request->session();        
                if($this->request->is('post'))
                {
                $post_data = $this->request->data;
                $id=$post_data['id'];
                $this->Orders->updateAll(array('status'=>2), array('id'=>$id));
                die("Your Cancel information changed successfully.");           
                }
                die();

      }
      # Complete Appointment 
    public function complete_appointment()
        {
                $this->loadModel('Orders');
                $session = $this->request->session();        
                if($this->request->is('post'))
                {
                $post_data = $this->request->data;
                $id=$post_data['id'];
                $this->Orders->updateAll(array('status'=>1), array('id'=>$id));
                }
              die("Your Complete information changed successfully.");  

       }
        #Update Hour
    public function add_hour(){ 
        $this->loadModel('Orders');
        $this->loadModel('Users');
        $session = $this->request->session();
        if($this->request->is('post'))
        {
            $post_data = $this->request->data;
                    
            $user_info = $session->read('UserInfo');

            $hero_email= $user_info['email'];
            $hero_data = $this->Users->find('all')->where(['email' =>$hero_email])->toArray(); 

            $order_data = $this->Orders->find('all')->where(['id' =>$post_data['id']])->toArray(); 

            if($hero_data[0]->id==$order_data[0]->hero_id){
                $this->Orders->UpdateAll($post_data,array('id' =>$post_data['id']));
                die('Update Information');
            }else{
                die('Hero Not Assigned');
            }
           
        }
       
    }


    #Register New User 
    public function register(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            
            if(!count($email)){
                $post_data['password'] = md5($post_data['password']);
                $post_data['role'] = 1;
                $connection = ConnectionManager::get('default');
                $connection->insert('users',$post_data);
                $msg="User Created Successfully";

            }else{
                $msg="Email already Exists";
            }
        }
        $this->set('msg', $msg);
    }

    public function logout(){
        $session = $this->request->session();
        $session->delete('UserInfo');
        $this->redirect(array("controller" => "Index", "action" => "index")); 

    }
}
