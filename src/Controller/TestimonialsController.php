<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Testimonials Controller
 *
 * @property \App\Model\Table\TestimonialsTable $Testimonials
 */
class TestimonialsController extends AppController
{

    public function initialize()
    {
        $this->layout = 'admin';
        $session = $this->request->session();
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            if($user_info['role']!=1){ $this->redirect(array("controller" => "Index", "action" => "index")); }

        }else{
             $this->redirect(array("controller" => "Index", "action" => "index")); 
        }
        
    }
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('testimonials', $this->paginate($this->Testimonials));
        $this->set('_serialize', ['testimonials']);
    }

    /**
     * View method
     *
     * @param string|null $id Testimonial id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $testimonial = $this->Testimonials->get($id, [
            'contain' => []
        ]);
        $this->set('testimonial', $testimonial);
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $testimonial = $this->Testimonials->newEntity();
        if ($this->request->is('post')) {
            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->data);
            if ($this->Testimonials->save($testimonial)) {
                //$this->Flash->success(__('The testimonial has been saved.'));
                return $this->redirect(['controller'=>'testimonials','action' => 'add','val'=>'create']);
            } else {
                //$this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('testimonial'));
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Testimonial id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $testimonial = $this->Testimonials->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->data);

            if ($this->Testimonials->save($testimonial)) {
                
                //$this->Flash->success(__('The testimonial has been saved.'));
                return $this->redirect(['controller'=>'testimonials','action' => 'edit','val'=>'update',$id]);
            } else {
                //$this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('testimonial'));
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Testimonial id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $testimonial = $this->Testimonials->get($id);
        if ($this->Testimonials->delete($testimonial)) {
            //$this->Flash->success(__('The testimonial has been deleted.'));
        } else {
            //$this->Flash->error(__('The testimonial could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
