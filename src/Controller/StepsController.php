<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class StepsController extends AppController
{
    public function initialize()
    {
        $this->layout = '';
        session_start();

        // unset($_SESSION["steps"]);
        // unset($_SESSION["sesval"]);
    }

    // Step One 
    public function index(){

        $this->loadModel('Categories');
        $parent_categories = $this->Categories->find('all')->where(['parent_id =' => 0])->toArray();
        $this->set('cat_one', $parent_categories);

    }

    // Step Two
    public function two(){
        
        if($this->request->is('post')){  
            $post_data = $this->request->data;
            $parms = json_decode($post_data['vals'],2);

            //if( isset($_SESSION["steps"]) && count($_SESSION["steps"][0])==3 ){
            if(isset($_SESSION["sesval"])){

                $_SESSION["steps"][ $_SESSION["sesval"] ]['stepOne'] = $parms['catName'];
            }else{
                $_SESSION["steps"][ 0 ]['stepOne'] = $parms['catName'];
            }
        }

        $this->loadModel('Categories');
        $parent_categories = $this->Categories->find('all')->where(['parent_id =' => $parms['catId']])->toArray();
        $this->set('cat_two', $parent_categories);

    }

    //Step Three
    public function three(){

        if($this->request->is('post')){  
            $post_data = $this->request->data;
            $parms = json_decode($post_data['vals'],2);

            //if( isset($_SESSION["steps"]) && count($_SESSION["steps"][0])==3 ){
            if(isset($_SESSION["sesval"])){

                $_SESSION["steps"][ $_SESSION["sesval"] ]['stepTwo'] = $parms['catName'];
            }else{
                $_SESSION["steps"][0]['stepTwo'] = $parms['catName'];
            }
        }
        
    }

    // Step Four
    public function four(){

        if($this->request->is('post')){  
            $post_data = $this->request->data;
            if(isset($_SESSION["sesval"])){
            //if( isset($_SESSION["steps"]) && count($_SESSION["steps"][0])==3 ){
                
                $_SESSION["steps"][ $_SESSION["sesval"] ]['stepThree'] = $post_data['vals'];
                $_SESSION["sesval"] = $_SESSION["sesval"] +1;
            }else{
                $_SESSION["steps"][ 0 ]['stepThree'] = $post_data['vals'];  
            }
        }

        $this->set('services', $_SESSION["steps"]);

    }

    //Delete a Service from Cart Popup
    public function deleteservice(){

        if($this->request->is('post')){  
            $post_data = $this->request->data;
            $val = $post_data['vals'];
            unset($_SESSION['steps'][$val]);
        }
        die;
    }
     #Register New User 
    public function register(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            
            if(!count($email)){
                $post_data['password'] = md5($post_data['password']);
                $post_data['role'] = 2;
                $post_data['status'] = 1;
                $connection = ConnectionManager::get('default');
                $connection->insert('users',$post_data);
                $msg="User Created Successfully";
                die($msg);
            }else{
                $msg="Email already Exists";
                die($msg);
            }
        }
        $this->set('msg', $msg);
    }


    // Check For Coverage Area
    public function checkcoverage(){
        $this->loadModel('PostalCode');
        $this->loadModel('Users');
        $session = $this->request->session();
        $user_info = $session->read('UserInfo');
        $coverage_data = array();


        if($this->request->is('post')){  
            $post_data = $this->request->data;

            if(isset($post_data['vals'])){
                $val = $post_data['vals'];
                $zip_code = $this->PostalCode->find('all')->where(['postal_code =' => $val])->toArray();

                if(count($zip_code)){

                    $coverage_data['zip'] = 'yes';
                    $coverage_data['zip_code'] = $zip_code[0]->postal_code;
                    

                    if(count($user_info) && $user_info['role']==2 ){
                        $coverage_data['user_login'] = 'yes';

                    }else{
                        $coverage_data['user_login'] = 'no';
                    }

                }else{
                    $coverage_data['zip_code'] = $val; 
                    $coverage_data['zip'] = 'no';
                }
            }
        }
        if(count($user_info)){
            $user_email = $user_info['email'];
            $user_data = $this->Users->find('all')->where(['email =' => $user_email])->toArray();
            $credit_data = array(
                'a_card_no'=>$user_data[0]->a_card_no,
                'a_exp_date'=>$user_data[0]->a_exp_date,
                'a_cvv'=>$user_data[0]->a_cvv,
                'a_postal_code'=>$user_data[0]->a_postal_code
            );
            $session->write('UserCardInfo', $credit_data);
        }
        $this->set('coverage_data', $coverage_data);
    }

    // Saving the Data For Payment and Steps ie (Services / Orders)
    public function savedata(){
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $session = $this->request->session();
        
        if($this->request->is('post')){

            // Save data for Payment 
            $post_data = $this->request->data;
            $appointment_date = $post_data['appointment_date'];
	if($appointment_date==""){ return false;}
		$form_type = $post_data['form_type'];
            unset($post_data['appointment_date']);
		unset($post_data['form_type']);
            $user_info = $session->read('UserInfo');
            $post_data = array_filter($post_data); // used if password is not set
            if(count($post_data)){
                $this->Users->updateAll( $post_data,  array('email' => $user_info['email'] ));
            }

		if($form_type=="old"){
                $card_info =  $session->read('UserCardInfo');
                $this->Users->updateAll( $card_info,  array('email' => $user_info['email'] ));
            }
            //Save Steps Data
            $get_id = $this->Users->find('all')->where(['email' => $user_info['email']])->toArray();
            foreach ($_SESSION['steps'] as $steps){
                
                $orders_data = array('user_id'=>$get_id[0]->id, 'order_data'=>json_encode($steps), 'appointment_date'=>$appointment_date);
                $Orders = TableRegistry::get('Orders');
                $query = $Orders->query();

                $query->insert(['user_id','order_data','appointment_date'])
                      ->values($orders_data)->execute();
            }
                $step_vars = array('steps' => $_SESSION['steps'],'appointment_date'=>$appointment_date);
                $this->sendemailglobal(
                $user_info['email'],
                'welcome',
                $step_vars,
                'html',
                'Your appointment details');  

        }
    }

    // If Zip Code not Exist , Save Email and Zip Code in DB
    public function zipemail(){
        if($this->request->is('post')){  
            $post_data = $this->request->data;
            $NonArea_data = array('email'=>$post_data['vals']['zip_email'], 'zip_code'=>$post_data['vals']['zip_code']);
            $NonArea = TableRegistry::get('NonArea');
            $query = $NonArea->query();
            $query->insert(['email','zip_code'])
                  ->values($NonArea_data)->execute();
        }    

    }

    //Clear everything after closing the Modal
    public function clearall(){

        $session = $this->request->session();
        unset($_SESSION["steps"]);
        unset($_SESSION['popup_login']);
        unset($_SESSION["login_popup"]);
        unset($_SESSION['api_login']);
    	unset($_SESSION['UserCardInfo']);
        $user_info = $session->read('UserInfo');
        if($user_info['role']==2){
            echo "login_customer";
        }

        die();
    }

    public function login(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['vals']['user_email']])->toArray();

            if(strlen(@$email[0]->email)>3){

                if(md5($post_data["vals"]["user_password"])==$email[0]->password){

                    $UserInfo = array(
                        "role"=>$email[0]->role,
                        "email"=>$email[0]->email,
                        "first_name"=>$email[0]->first_name,
                        "last_name"=>$email[0]->last_name,
                        "status"=>$email[0]->status,
                        "phone_no"=>$email[0]->phone_no,

                        );

                    if(!$UserInfo['status']){
                        $msg = "Your Account is Not Activated.";
                        die($msg);       
                    }else{
                        unset($UserInfo["password"]);
                        //print_r($UserInfo);
                        if($UserInfo['role']==2 || $UserInfo['role']=='2'){

                            $session->write('UserInfo', $UserInfo); 
                            return $this->redirect(['controller' => 'Steps', 'action' => 'checkcoverage']);
                        }else{
                            
                            $msg = "Eather Email or Password Does Not Match.";           
                        }
                             //$this->redirect(array("controller" => "Steps", "action" => "checkcoverage")); 
                            /*elseif($UserInfo['role']==1){
                            $session->write('UserInfo', $UserInfo);
                            $this->redirect(array("controller" => "AdminDashboard", "action" => "index")); 
                        }elseif($UserInfo['role']==3){
                            $session->write('UserInfo', $UserInfo);
                            $this->redirect(array("controller" => "HeroDashboard", "action" => "index")); 
                        }*/
                    }                
                }
                else{ $msg = "Eather Email or Password Does Not Match."; }

            }else{
                $msg = "Eather Email or Password Does Not Match.";
            }
        }
        die($msg);
    }


    public function applicants(){
        
        if($this->request->is('post')){          
            $post_data = $this->request->data;
            $applicants = TableRegistry::get('Applicants');
            $query = $applicants->query();
            $query->insert(['name','email','phone_no','comments'])
                  ->values(['name'=>$post_data['name'],'email'=>$post_data['email'],'phone_no'=>$post_data['phone_no'],'comments'=>$post_data['comments']])
                  ->execute();
            echo "<h2> Thanks for your interest. We'll be in touch soon! </h2>";
        }
        die;
    }

    public function wizardStepOne(){
        $this->loadModel('Categories');
        $parent_categories = $this->Categories->find('all')->where(['parent_id =' => 0])->toArray();
        $html ='        
        <fieldset>
            <div class="modal-header">
            <ol class="breadcrumb">
                <li><a href="#">Setup & Install </a></li>
                <li><a href="#">Smart Home</a></li>
                <li>Smart Home Device</li>
                <li><a href="#">Comments</a></li>
                <li  class="active"><a href="#">Summary</a></li>
            </ol>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <h3 class="fs-title">Select from the options below to get started.</h3>
            <h2 class="fs-subtitle">What Can We Help You With?</h2>
            <div class="row inner-grid">
                <div class="col-md-3">
                    <div class="speaker-inner">
                        <div class="speaker-icons setup-install"><img src="'.$base_url_temp.'img/template_images/set-up-img.png" alt="set-up &amp; install"></div>
                        <div class="speaker-icons setup-install-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/set-up-hover.png" alt="set-up &amp; install"></div>
                        <div class="speaker_heading"><h3>Setup &amp; Install</h3></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="speaker-inner">
                        <div class="speaker-icons troubleshooting"><img src="'.$base_url_temp.'img/template_images/troubleshooting.png" alt="set-up &amp; install"></div>
                        <div class="speaker-icons troubleshooting-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/troubleshooting-hover.png" alt="set-up &amp; install"></div>
                        <div class="speaker_heading"><h3>Troubleshooting</h3></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="speaker-inner">
                        <div class="speaker-icons product-advice"><img src="'.$base_url_temp.'img/template_images/product-advice.png" alt="set-up &amp; install"></div>
                        <div class="speaker-icons product-advice-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/product-advice-hover.png" alt="set-up &amp; install"></div>
                        <div class="speaker_heading"><h3>Product Advice</h3></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="speaker-inner">
                        <div class="speaker-icons training"><img src="'.$base_url_temp.'img/template_images/training.png" alt="set-up &amp; install"></div>
                        <div class="speaker-icons training-hover"><img src="<?php echo $base_url_temp; ?>img/template_images/training-hover.png" alt="set-up &amp; install"></div>
                        <div class="speaker_heading"><h3>Training</h3></div>
                    </div>
                </div>
            </div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>';
        echo $html;
        die;
    }

}
