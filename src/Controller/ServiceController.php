<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ServiceController extends AppController
{

    public function initialize()
    {
        $this->layout = 'default';
    }

    public function index(){
        $this->loadModel('Categories'); 
        $parent_categories = $this->Categories->find('all')->where(['parent_id =' => 0])->toArray();
        $this->set('parent_categories', $parent_categories);
    }

    public function nextcat(){
        $this->loadModel('Categories'); 
        if($this->request->is('post')){
            $post_data = $this->request->data;
            $parent_categories = $this->Categories->find('all')->where(['parent_id =' => $post_data['parent_id']])->toArray();
            $arr = array();
            foreach($parent_categories as $key => $parent_categorie){
                $arr[$key]["id"] = $parent_categorie->id;
                $arr[$key]["category_name"] = $parent_categorie->category_name;
            }
            print_r(json_encode($arr));
        }
        exit;        
    }
    #Login in the user 
    public function login(){
       
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();

            if(strlen(@$email[0]->email)>3){

                if(md5($post_data["password"])==$email[0]->password){
                    $UserInfo = @$email[0];

                    if(!$UserInfo->status){
                        $msg = "Your Account is Not Activated. ";       
                    }else{
                        unset($UserInfo["password"]);
                        $session->write('UserInfo', $UserInfo);
                        $this->redirect(array("controller" => "Index", "action" => "index")); 
                    }                
                }
                else{ $msg = "Eather Email or Password Does Not Match."; }

            }else{
                $msg = "Eather Email or Password Does Not Match.";
            }
        }
        $this->set('msg', $msg);
    }

    #Register New User 
    public function register()
    {
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            
            if(!count($email)){
                $post_data['password'] = md5($post_data['password']);
                $post_data['role'] = 1;
                $connection = ConnectionManager::get('default');
                $connection->insert('users',$post_data);
                $msg="User Created Successfully";

            }else{
                $msg="Email already Exists";
            }
        }
        $this->set('msg', $msg);
    }

    public function logout(){
        $session = $this->request->session();
        $session->delete('UserInfo');
        $this->redirect(array("controller" => "Index", "action" => "index")); 

    }
}
