<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class LoginController extends AppController
{

    public function initialize()
    {
        $this->layout = 'default';
    }

    #Login the user 
    public function index(){
                  
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();

            if(strlen(@$email[0]->email)>3){

                if(md5($post_data["password"])==$email[0]->password){

                    $UserInfo = array(
                        "role"=>$email[0]->role,
                        "email"=>$email[0]->email,
                        "first_name"=>$email[0]->first_name,
                        "last_name"=>$email[0]->last_name,
                        "status"=>$email[0]->status,
                        "phone_no"=>$email[0]->phone_no,

                        );

                    if(!$UserInfo['status']){
                        $msg = "Your Account is Not Activated.";   
                        die($msg);    
                    }else{
                        unset($UserInfo["password"]);
                        if($UserInfo['role']==2){
                              
                            $session->write('UserInfo', $UserInfo);
                            $msg = "User Logged";
                            die($msg);
                            //$this->redirect(array("controller" => "UserDashboard", "action" => "index")); 
                            // echo '<pre>';
                            //     print_r($UserInfo);
                                //die('test11');
                        }elseif($UserInfo['role']==1){
                            $session->write('UserInfo', $UserInfo);
                            $msg = "Admin Logged";
                            die($msg);
                            //$this->redirect(array("controller" => "AdminDashboard", "action" => "index")); 
                        }elseif($UserInfo['role']==3){
                            $session->write('UserInfo', $UserInfo);
                            $msg = "Hero Logged";
                            die($msg);
                            //$this->redirect(array("controller" => "HeroDashboard", "action" => "index")); 
                        }
                    }                
                }
                else{ $msg = "Eather Email or Password Does Not Match."; }

            }else{
                $msg = "Eather Email or Password Does Not Match.";
            }
        }
        die($msg);
        $this->set('msg', $msg);
    }

    #Register New User 
    public function register(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            
            if(!count($email)){
                $post_data['password'] = md5($post_data['password']);
                $post_data['role'] = 2;
                $post_data['status'] = 1;
                $connection = ConnectionManager::get('default');
                $connection->insert('users',$post_data);
                $msg="User Created Successfully";
                die($msg);
            }else{
                $msg="Email already Exists";
                die($msg);
            }
        }
        $this->set('msg', $msg);
    }
    #Forgot password 
    public function forgot_send_mail(){
        $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';

        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            
            if(count($email)){
                $id=$email[0]->id;
                $name=$email[0]->first_name;
                $rand_url=rand(100,900);
                $rand_url2=rand(100,900);
                $code=$rand_url.$id.$rand_url2;
                $p_r_code=array('p_r_code'=>$code);
                $this->Users->updateAll( $p_r_code,  array('id' => $id)); 
                $pass_vars = array('code' => $code,'name'=>$name);
                $this->sendemailglobal(
                $post_data['email'],
                'forgot_pass',
                $pass_vars,
                'html',
                'Password Reset');

                    $msg="Email Successfully Send";
                die($msg);
            }else{
                $msg="Email Does Not Exists";
                die($msg);
            }
        }
        $this->set('msg', $msg);
    }

    

    public function logout(){
        $session = $this->request->session();
        $session->delete('UserInfo');
        $this->redirect(array("controller" => "Index", "action" => "index")); 

    }
}
