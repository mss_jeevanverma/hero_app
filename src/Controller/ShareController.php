<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ShareController extends AppController
{

    public function initialize()
    {
        //$this->layout = 'default';
        $this->layout = 'user_account';
        $session = $this->request->session();
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            if($user_info['role']==3 || $user_info['role']==2  ){  }
            else { $this->redirect(array("controller" => "Index", "action" => "index")); }    

        }else{
             $this->redirect(array("controller" => "Index", "action" => "index")); 
        }
        
    }

    #Links for sharing screen and Video Chat 
    public function index()
    {

        // $this->loadModel('Users');
        // $session = $this->request->session();
        // $user_info = $session->read('UserInfo');

        // $customer = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray();       
        // $data = array ("customer"=>$customer);
        // $this->set('data', $data);       
    }

    public function videoConference(){

    }

    public function screenSharing(){

    }

    public function textChat(){

    }

    public function sharelink()
    {
      if($this->request->is('post'))
       {
        $post_data=$this->request->data;
         $pass_vars = array('share_link' => $post_data['share_url'],'link_type' =>$post_data['link_type']);
                $this->sendemailglobal(
                $post_data['email'],
                'share_link',
                 $pass_vars,
                'html',
                'Share Link');
                die('success');
        
       }

    }


       

}
