<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
require_once('braintree/braintree_php/lib/autoload.php');
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Braintree\Configuration;
use Braintree\TransparentRedirect;
use Braintree\Transaction;
use Braintree\Exception;
use Cake\Network\Email\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class UserDashboardController extends AppController
{

    public function initialize()
    {
        //$this->layout = 'default';
        $this->layout = 'user_account';
        $session = $this->request->session();
        if($session->read('UserInfo')){
            $user_info = $session->read('UserInfo');
            if($user_info['role']!=2){ $this->redirect(array("controller" => "Index", "action" => "index")); }

        }else{
             $this->redirect(array("controller" => "Index", "action" => "index")); 
        }
        
    }

    #Send Current User Data to Home page  
    public function index()
    {

        $this->loadModel('Users');
        $session = $this->request->session();
        $user_info = $session->read('UserInfo');

        $customer = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray();       
        $data = array ("customer"=>$customer);
        $this->set('data', $data);       
    }

    #Edit Customer  Info
    public function editcustomer()
       { 
                $this->loadModel('Users');
                $session = $this->request->session();
                if($this->request->is('post'))
                {
                    $post_data = $this->request->data;
                    $user_info = $session->read('UserInfo');
                        if($post_data["password"])
                            { 
                                $post_data["password"] = md5($post_data["password"]); 
                            }
                            $post_data = array_filter($post_data); // used if password is not set
                            if(!isset($post_data['phone_text']))
                            {
                            $post_data['phone_text']=0;
                            }

                    $this->Users->updateAll( $post_data,  array('email' => $user_info['email'] ));
                    die("Your information changed successfully.");
                }
        }

        #Edit Customer Edit  Password
    public function editpassword(){ 
        $this->loadModel('Users');
        $session = $this->request->session();
      
          if($this->request->is('post')){
            $post_data = $this->request->data;
            $old_pass = md5($post_data['password']);
            $new_pass = md5($post_data['new_password']);
            $user_info = $session->read('UserInfo');
            $customer = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray();       
            $data_pass=$customer[0]->password;
            if($old_pass==$data_pass)
            {
                $new_password=array('password'=>$new_pass);
               $this->Users->updateAll( $new_password,  array('email' => $user_info['email'] )); 
             die("Your information changed successfully.");
            }
            else
            {
                $msg="Current Password is wrong";
                die('Current Password is wrong');
            }
          }

       
    }

    #Edit Customer Payment Edit
    public function editpayment(){ 
        $this->loadModel('Users');
        $session = $this->request->session();
        
        if($this->request->is('post'))
        {

            $post_data = $this->request->data;
            $user_info = $session->read('UserInfo');
            $post_data = array_filter($post_data); // used if password is not set
            $this->Users->updateAll( $post_data,  array('email' => $user_info['email'] ));
            die("Your Payment information changed successfully.");
        }
       
    }

    #Update Hour
    public function add_hour(){ 
        $this->loadModel('Orders');
        $session = $this->request->session();
        if($this->request->is('post'))
        {
            $post_data = $this->request->data;
            $this->Orders->UpdateAll($post_data,array('id' =>$post_data['id']));
           die('Update Information');
        }
       
    }


    # Customer Appointment Info
    public function appointment_info()
       { 
            $this->loadModel('Orders');
            $this->loadModel('Users');
            $session = $this->request->session();
            $user_info = $session->read('UserInfo');
            $customer = $this->Users->find('all')->where(['email =' => $user_info['email']])->toArray(); 
            $id=$customer[0]->id;
            $appointment_data = $this->Orders->find('all',['order' => ['Orders.id' => 'DESC']])->where(['user_id ' =>$id,'status' =>0 ])->toArray();
            $data = array ("appointment_data"=>$appointment_data);
            $this->set('data', $data);
            $appointment_data1 = $this->Orders->find('all')->where(['user_id ' =>$id,'status' =>1 ])->toArray();
            $data1 = array ("appointment_data1"=>$appointment_data1);
            $this->set('data1', $data1);

        }
        # Customer Appointment View
    public function appointment_view()
       { 
           $this->loadModel('Orders');
            $this->loadModel('Users');
            $hero_data=array();
            $session = $this->request->session();
            $user_info = $session->read('UserInfo');
            $appointment_id=$this->request->query['order_id'];
            $appointment_data = $this->Orders->find('all')->where(['id ' =>$appointment_id])->where(['status !=' => 2])->toArray();
            if(count($appointment_data))
            {
            $hero_id=$appointment_data[0]->hero_id;
            $user_id=$appointment_data[0]->user_id;
            $hero_data = $this->Users->find('all')->where(['id ' =>$hero_id])->toArray();
            $user_data = $this->Users->find('all')->where(['id ' =>$user_id])->toArray();
              }
            // For Payment
              $home_url=$this->request->here; 
                 $base_url = "http://" . $_SERVER['SERVER_NAME'].$home_url."?order_id=".$appointment_id; 
                 $settings = [
                'environment' => 'sandbox', // possible values are sandbox or production
                'merchantId' =>  '3zhkwb29wsxz3rnw',
                'publicKey' => 'vbbcp8xn8qmxqnmj',
                'privateKey' => 'abf05e79c47b7275bd8b6c210687341c',
                'redirectUrl' => $base_url  // no trailing slash
                ];


                    Configuration::environment($settings['environment']);
                    Configuration::merchantId($settings['merchantId']);
                    Configuration::publicKey($settings['publicKey']);
                    Configuration::privateKey($settings['privateKey']);
                    $total_hour=$appointment_data[0]->add_hour;
                    $total_pay=$total_hour*79;
                    $tr_data = TransparentRedirect::transactionData([
                                                     'transaction' => [
                                                        'type' => Transaction::SALE,
                                                        'amount' => $total_pay,
                                                        'orderId' => $appointment_id,
                                                        'billing'=>[
                                                        'postalCode'=>$user_data[0]->a_postal_code
                                                        ],
                                                        'options' => [
                                                            'submitForSettlement' => true
                                                        ]
                                                    ],
                                                    'redirectUrl' => $settings['redirectUrl']
                                                ]);
                    
                $status = '';
                if(isset($this->request->query['http_status']))
                            {
                    
                     $http_status=$this->request->query['http_status'];

                        if(isset($http_status) && $http_status == '200') {
                        try {
                            $result = TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
                            $transaction_id = $result->transaction->id;
                             if ($result->success) {
                                $status = 'Your transaction was processed successfully.';
                                $email = new Email('default');
                                             
                  
                                $this->Orders->updateAll(array('status'=>1,'transaction_id'=>$transaction_id), array('id'=>$appointment_id));
                                         
                                $app_vars = array("appointment_data"=>$appointment_data[0],"hero_data"=>$hero_data[0],"user_data"=>$user_data[0],'transaction_id'=>$transaction_id);
                                $this->sendemailglobal(
                                $user_data[0]->email,
                                'payment_detail',
                                $app_vars,
                                'html',
                                'Our payment details'); 

                               $this->redirect(array('controller'=>'user_dashboard','action'=>'appointment_view','order_id'=>$appointment_id));
                            } else {
                                $status = $result->message;
                            }
                        } catch (Exception_NotFound $e) {
                             $status = 'Due to security reasons, the reload button has been disabled on this page.';
                        }

                    }
                   
                  
                }
                    
                

              $data = array ("appointment_data"=>$appointment_data,"hero_data"=>$hero_data,"user_data"=>$user_data,"tr_data"=>$tr_data,'status'=>$status);
                          
            $this->set('data', $data);
        }
     # Cancel Appointment   
    public function cancel_appointment()
        {
                $this->loadModel('Orders');
                $session = $this->request->session();        
                if($this->request->is('post'))
                {
                $post_data = $this->request->data;
                $id=$post_data['id'];
                $this->Orders->updateAll(array('status'=>2), array('id'=>$id));
                die("Your Cancel information changed successfully.");           
                }
                die();

      }
      # Complete Appointment 
    public function complete_appointment()
        {
                $this->loadModel('Orders');
                $session = $this->request->session();        
                if($this->request->is('post'))
                {
                $post_data = $this->request->data;
                $id=$post_data['id'];
                $this->Orders->updateAll(array('status'=>1), array('id'=>$id));
                }
              die("Your Complete information changed successfully.");  

       }
       

}
