<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Network\Email\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $base_url_temp = "http://" . $_SERVER['SERVER_NAME']."/grabahero/";
        $base_url_temp = Router::url('/', true);


        $this->loadModel('Pages');
        $pages = $this->Pages->find('all')->toArray();
        $sorted_pages = array();
        foreach ($pages as $key => $row)
        {
            $sorted_pages[$key] = $row['sorting'];
        }
        array_multisort($sorted_pages, SORT_ASC, $pages); 
        $this->set ( 'layout_pages_data', $pages );

        $this->set('base_url_temp', $base_url_temp);
        if ($this->response->statusCode() == '404') {
            die("gooolaa");
        }
        
    }
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
    }

    public function sendemailglobal($to,$template,$vars,$format,$subject){

        $email = new Email('default');
        $email->to($to)
            ->template($template)
            ->viewVars($vars)
            ->emailFormat($format)
            ->subject($subject)
            ->from('hero@app.com')
            ->send();

    }

}
