<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;

require_once 'autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

require_once ('libraries/Google/autoload.php');
require_once (ROOT. DS .'/src/Controller/twitter/inc/twitteroauth.php');

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class LoginApiController extends AppController
{

    public function initialize()
    {
        $this->layout = 'default';
        session_start();
    }

    #Login the user Using Facebook
    public function index(){
	
	// if user loges in from fb and Steps data is in sesstion
	if(isset($_SESSION['steps'])){
		$_SESSION['api_login'] = 'yes';
	}	
		
	
	$this->loadModel('Users');
	$cake_session = $this->request->session();
        // init app with app id and secret
        FacebookSession::setDefaultApplication( '512265015594764','7ea9b3280cab0be141937b20abd0e436' );
        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper('http://mastersoftwaretechnologies.com/grabahero/login_api' );
        try {
          $session = $helper->getSessionFromRedirect();

        } catch( FacebookRequestException $ex ) {
          // When Facebook returns an error
        } catch( Exception $ex ) {
          // When validation fails or other local issues
        }
        

        if ( isset( $session ) ) {
  
            $request = new FacebookRequest( $session, 'GET', '/me' );
            $response = $request->execute();

            $graphObject = $response->getGraphObject();
		$graphObject->getProperty("first_name");
                $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
                $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
                $femail = $graphObject->getProperty('email');    // To Get Facebook email ID
		$name_arr = explode(" ",$fbfullname);
		if(!$femail){ $femail=strtolower( str_replace(" ","",$fbfullname)."@facebook.com"); } 
		$fb_arr = array("role"=>2,"email"=>$femail,"first_name"=>$name_arr[0],"last_name"=>$name_arr[1],"status"=>1,"fb_id"=>$fbid);
					
		
		$db_email = $this->Users->find('all')->where(['email =' => $femail])->toArray();

		if(strlen(@$db_email[0]->email)>3){
			$cake_session->write('UserInfo', $fb_arr);
			
			//if Steps data present go to index and open Steps popup
			if(@$_SESSION['api_login'] == 'yes'){
				$this->redirect(array("controller" => "index", "action" => "index"));
			}else{
				$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
			}
		}else{
			$cake_session->write('UserInfo', $fb_arr);
			$connection = ConnectionManager::get('default');
			$connection->insert('users',$fb_arr);
			
			//if Steps data present go to index and open Steps popup
			if(@$_SESSION['api_login'] == 'yes'){ 
				$this->redirect(array("controller" => "index", "action" => "index"));
			}else{
				$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
			}
		}
                 

        } else {
            
            $loginUrl = $helper->getLoginUrl();
	    //$loginUrl = $loginUrl."email";	
            $this->redirect( $loginUrl );
           //print_r($loginUrl); die();
            //header("Location: ".$loginUrl);
        }             
    }


	 #Login the user Using Facebook
	public function gmail(){

		// if user loges in from Gmail and Steps data is in sesstion
		if(isset($_SESSION['steps'])){
			$_SESSION['api_login'] = 'yes';
		}	
	
		$this->loadModel('Users');
		$cake_session = $this->request->session();
		$client_id = '648015861504-2s0dokhn5er0inhk2hdv351p32v42eto.apps.googleusercontent.com'; 
		$client_secret = 'pdEBg_C2UPtWKwpHPqrMGovZ';
		$redirect_uri = 'http://mastersoftwaretechnologies.com/grabahero/login_api/gmail';

		
		$client = new \Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->addScope("email");
		$client->addScope("profile");

		$service = new \Google_Service_Oauth2($client);

		if (isset($_GET['code'])) {
		  $client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $client->getAccessToken();
		  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
		  exit;
		}

		if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
		  $client->setAccessToken($_SESSION['access_token']);
		} else {
		  $authUrl = $client->createAuthUrl();
		}

		if (isset($authUrl) && !isset($_GET['error']) ){ 
			//show login url
			$this->redirect( $authUrl );	
	
		} else {
	if(!isset($_GET['error'])){
			$user = $service->userinfo->get(); //get user info 
			$name_arr = explode(" ",$user->name);
			$fb_arr = array("role"=>2,"email"=>$user->email,"first_name"=>$name_arr[0],"last_name"=>$name_arr[1],"status"=>1,"gmail_id"=>$user->id);
			$db_email = $this->Users->find('all')->where(['email =' => $user->email])->toArray();

			if(strlen(@$db_email[0]->email)>3){

				$cake_session->write('UserInfo', $fb_arr);

				//if Steps data present go to index and open Steps popup
				if(@$_SESSION['api_login'] == 'yes'){ 
					$this->redirect(array("controller" => "index", "action" => "index"));
				}else{
					$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
				}
			}else{

				$cake_session->write('UserInfo', $fb_arr);
				$connection = ConnectionManager::get('default');
				$connection->insert('users',$fb_arr);

				//if Steps data present go to index and open Steps popup
				if(@$_SESSION['api_login'] == 'yes'){ 
					$this->redirect(array("controller" => "index", "action" => "index"));
				}else{
					$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
				}
			}

			//show user picture
			//echo '<img src="'.$user->picture.'" style="float: right;margin-top: 33px;" />';
			echo 'Hi '.$user->name.', Thanks for Registering!';

			//$statement->bind_param('issss', $user->id,  $user->name, $user->email, $user->link, $user->picture);

			//print user details
			//echo '<pre>';
			//print_r($user);
			//echo '</pre>';
	} // if error exits
	else{
		$this->redirect( 'http://mastersoftwaretechnologies.com/grabahero/' );	// home url if error exits
	}
		}

 
	}

	public function twitterlogin(){
		$base_url_temp = Router::url('/', true);


        $this->loadModel('Users');
        $cake_session = $this->request->session();
		if(isset($_REQUEST['oauth_token']) && $_SESSION['token']  !== $_REQUEST['oauth_token']) {
			

			//If token is old, distroy session and redirect user to index.php
			session_destroy();
			$this->redirect(array("controller" => "index", "action" => "index"));
			
		}
		elseif(isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {
			
			//Successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
			$connection = new \TwitterOAuth('gOa6ZFmp74iGpexVrCewxExVs', 'PYuWbAKwVY4KDITTvN9rRAYQ1ZHmj7yEC0fh2QghRad2eZ0Lhh', $_SESSION['token'] , $_SESSION['token_secret']);
			$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
			if($connection->http_code == '200')
			{
				//Redirect user to twitter
				$_SESSION['status'] = 'verified';
				$_SESSION['request_vars'] = $access_token;
				
				//Insert user into the database
				$user_info = $connection->get('account/verify_credentials'); 
				$twitteremail=strtolower( str_replace(" ","",$user_info->screen_name)."@twitter.com");
                $name = explode(" ",$user_info->name);
				$fname = isset($name[0])?$name[0]:'';
				$lname = isset($name[1])?$name[1]:'';
			 $fb_arr = array("role"=>2,"email"=>$twitteremail,"first_name"=>$fname,"last_name"=>$lname,"status"=>1,"twitter_id"=>$user_info->id_str);
		     $db_email = $this->Users->find('all')->where(['email =' => $twitteremail])->toArray();

		    if(strlen(@$db_email[0]->email)>3){
			$cake_session->write('UserInfo', $fb_arr);
			
			//if Steps data present go to index and open Steps popup
			if(@$_SESSION['api_login'] == 'yes'){
				$this->redirect(array("controller" => "index", "action" => "index"));
			}else{
				$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
			}
		}
		else{
			$cake_session->write('UserInfo', $fb_arr);
			$connection = ConnectionManager::get('default');
			$connection->insert('users',$fb_arr);	
					
			//if Steps data present go to index and open Steps popup
			if(@$_SESSION['api_login'] == 'yes'){ 
				$this->redirect(array("controller" => "index", "action" => "index"));
			}else{
				$this->redirect(array("controller" => "UserDashboard", "action" => "index"));
			}

		}

			}
			else{
				die("error, try again later!");
			}
				
		}else{

			if(isset($_GET["denied"]))
			{
				$this->redirect(array("controller" => "index", "action" => "index"));
				die();
			}

			//Fresh authentication

			$connection = new \TwitterOAuth('gOa6ZFmp74iGpexVrCewxExVs', 'PYuWbAKwVY4KDITTvN9rRAYQ1ZHmj7yEC0fh2QghRad2eZ0Lhh');
			$request_token = $connection->getRequestToken($base_url_temp.'/login_api/twitterlogin');
			
			//Received token info from twitter
			$_SESSION['token'] 			= $request_token['oauth_token'];
			$_SESSION['token_secret'] 	= $request_token['oauth_token_secret'];
			

			//Any value other than 200 is failure, so continue only if http code is 200
			if($connection->http_code)
			{
				//redirect user to twitter
				$twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
				$this->redirect( $twitter_url );
				
			}
			else{
				die("error connecting to twitter! try again later!");
			}
		}
	}
}
